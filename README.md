# AgriBIoT RFID Application


### Note about this project

AgriBIoT was a startup project lead by Oscar Roberto Bastos at the time of his untimely passing in 2022. To honor his legacy and in line with his values, we have decided to release AgriBIoT android application and the [backend and frontend](https://gitlab.inria.fr/agribiot_devel/agribiot_solution) into the public domain.  

Additional information related to the start-up project can be found [here](https://web.archive.org/web/20210703182926/https://agribiot.fr/en/index.html).  

Support for either projects can be obtained by contacting Simon Tropée (simon.tropee@sirocha.fr) or E4SE team projet leader Jean-Marie Bonnin (Jean-Marie.Bonnin@irisa.fr)  

### Compile

- Download and install Android studio
- Clone this repository
- In android studio :File > Open : select your  agribiot-android folder
- Then select : Build > Make Project
- To execute application select : Run > Run 'app' 

Since no local database migration has been implemented yet if any modification is done on bzh.agribiot.database package 
the application data should be clear on the Android device (Or the app should be uninstall to be reinstall properly) 


### Dependencies and licenses



AgriBIoT Backend
| Name  | Licenses  | Project link |
|-------|-----------| -------------|
| Android  | Apache  | https://source.android.com/setup/start/licenses?hl=fr |
| Android room  | Apache License | https://android.googlesource.com/platform/frameworks/support/+/android-room-release/LICENSE.txt |
| Android volley | Apache | https://github.com/google/volley/blob/master/LICENSE |
| Axem MagSled/Chafon library  | |  |
| osmdroid | Apache | https://github.com/osmdroid/osmdroid/blob/master/LICENSE |
| zxing | Apache license | https://github.com/zxing/zxing/blob/master/LICENSE |
| 
