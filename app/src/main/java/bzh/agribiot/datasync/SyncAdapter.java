/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.datasync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.AnnotationTemplateDao;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionDao;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.DimensionValueDao;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationDao;
import bzh.agribiot.database.OperationImage;
import bzh.agribiot.database.SensorObject;
import bzh.agribiot.database.SensorObjectDao;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.volley.SynchronizationCallback;
import bzh.agribiot.volley.VolleyHelper;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter implements SynchronizationCallback {
    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver contentResolver;
    static final String LOG_TAG = "SyncAdapter";

    AgribiotDatabase agDb;
    AgribiotApplication agribiotApplication ;

    SynchroStep syncStep ;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        contentResolver = context.getContentResolver();
        agribiotApplication = (AgribiotApplication) context;
        agDb = AgribiotDatabase.getDatabase(getContext());
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        contentResolver = context.getContentResolver();
    }

    /*
        How many Annotation and operation has to be synced
     */
    int nbrOperationToSync = 0;
    int cntInsertedAnnotation = 0;
    int cntInsertedDimension = 0;
    int cntInsertedSensorObject = 0;
    int synchronizedImage = 0;
    int synchronizedOperation = 0;
    int synchronizedDimension = 0;
    int synchronizedDimensionValue = 0;

    ArrayList<AnnotationTemplate> pulledAnnotationTemplates;
    ArrayList<AnnotationTemplate> insertedAnnotationTemplates;
    ArrayList<AnnotationTemplate> annotationTemplatesToSync;
    ArrayList<DimensionValue> dimensionValuesToSync;
    ArrayList<Dimension> dimensionToSync;
    ArrayList<SensorObject> sensorObjectsToSync;

    ArrayList<Dimension> pulledDimensions;
    ArrayList<Dimension> insertedDimensions;

    ArrayList<DimensionValue> pulledDimensionValue;
    ArrayList<Operation> insertedOperation = null;
    ArrayList<Operation> pulledOperation = null;
    ArrayList<SensorObject> pulledSensorObjects = null;
    ArrayList<SensorObject> insertedSensorObjects = null;
    ArrayList<Operation> operationsToSync = null;
    ArrayList<AnnotationTemplate> allAnnotationTemplate = null;
    ArrayList<Dimension> allDimension = null;
    ArrayList<SensorObject> allSensorObject = null;
    ArrayList<OperationImage> imageToSync = null;

    public enum SynchroStep {
        PULL_ANNOTATION_TEMPLATE,
        PUSH_ANNOTATION_TEMPLATE,
        PULL_SENSOR_OBJECT,
        PUSH_SENSOR_OBJECT,
        PULL_ANNOTATION_TEMPLATE_IMAGES,
        PULL_DIMENSION,
        PUSH_DIMENSION,
        PUSH_OPERATION_IMAGES,
        PUSH_OPERATION,
        PULL_OPERATION,
        PUSH_DIMENSION_VALUE,
        PULL_DIMENSION_VALUE,
        COMPLETE,
        ERROR;
        public SynchroStep next() {
            if (ordinal() == values().length - 1)
                throw new NoSuchElementException();
            return values()[ordinal() + 1];
        }

    };

    int getSensorObjectPostGreIdFromLocalId(ArrayList<SensorObject> list,int localId){
        for(int i = 0;i<list.size();i++){
            if(list.get(i).getSensorObjectId() == localId){
                return list.get(i).getPostgreId();
            }
        }
        return -1;
    }

    int getAnnotationTemplatePostGreIdFromLocalId(ArrayList<AnnotationTemplate> list,int localId){
        for(int i = 0;i<list.size();i++){
            if(list.get(i).getAnnotationTemplateId() == localId){
                return list.get(i).getPostgreId();
            }
        }
        return -1;
    }
    int getAnnotationTemplateLocalIdFromPostGreId(ArrayList<AnnotationTemplate> list,int postGreId){
        for(int i = 0;i<list.size();i++){
            if(list.get(i).getPostgreId() == postGreId){
                return list.get(i).getAnnotationTemplateId();
            }
        }
        return -1;
    }
    int getSensorObjectLocalIdFromPostGreId(ArrayList<SensorObject> list,int postGreId){
        for(int i = 0;i<list.size();i++){
            if(list.get(i).getPostgreId() == postGreId){
                return list.get(i).getSensorObjectId();
            }
        }
        return -1;
    }
    int getOperationLocalIdFromPostGreId(ArrayList<Operation> list,int postGreId){
        for(int i = 0;i<list.size();i++){
            if(list.get(i).getPostgreId() == postGreId){
                return list.get(i).getOperationId();
            }
        }
        return -1;
    }

    int getDimensionLocalIdFromPostGreId(ArrayList<Dimension> list,int postGreId){
        for(int i = 0;i<list.size();i++){
            if(list.get(i).getPostgreId() == postGreId){
                return list.get(i).getDimensionId();
            }
        }
        return -1;
    }

    int getDimensionPostGreIdFromLocalId(ArrayList<Dimension> list, int localId){
        for(int i = 0;i<list.size();i++){
            if(list.get(i).getDimensionId() == localId){
                return list.get(i).getPostgreId();
            }
        }
        return -1;
    }
    /*
        Synchronization :
        1) Preparation :
            - Count and prepare Entities to sync
        2) Push/Pull entities
        3) Merge Pulled and Pushed entities
        4) Commit to local room database

     */
    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {
        LoggerUtil.debug(LOG_TAG,"onPerformSync");


        agDb.databaseWriteExecutor.execute(() -> {
            LoggerUtil.debug(LOG_TAG,"Synchronize");
            LoggerUtil.debug(LOG_TAG,"initialization");

            AnnotationTemplateDao annotationTemplateDao = agDb.annotationTemplateDao();
            OperationDao operationDao = agDb.operationDao();
            SensorObjectDao sensorObjectDao = agDb.sensorObjectDao();
            DimensionDao dimensionDao = agDb.dimensionDao();
            DimensionValueDao dimensionValueDao = agDb.dimensionValueDao();

            allAnnotationTemplate = (ArrayList<AnnotationTemplate>) annotationTemplateDao.getAllAsList();
            allDimension = (ArrayList<Dimension>)dimensionDao.getAllAsList();
            allSensorObject = (ArrayList<SensorObject>)sensorObjectDao.getAllAsList();
            annotationTemplatesToSync = (ArrayList<AnnotationTemplate>)  annotationTemplateDao.loadAllBySynchronizedFlag(0);
            operationsToSync = (ArrayList<Operation>) operationDao.loadAllBySynchronizedFlag(0);
            imageToSync = new ArrayList<OperationImage>();
            dimensionToSync = (ArrayList<Dimension>) dimensionDao.loadAllBySynchronizedFlag(0);
            dimensionValuesToSync = (ArrayList<DimensionValue>) dimensionValueDao.loadAllBySynchronizedFlag(0);
            sensorObjectsToSync = (ArrayList<SensorObject>) sensorObjectDao.loadAllBySynchronizedFlag(0);

            insertedOperation = new ArrayList<Operation>();
            cntInsertedAnnotation = 0;
            cntInsertedSensorObject = 0;
            cntInsertedDimension = 0;

            synchronizedImage = 0;
            synchronizedOperation = 0;
            synchronizedDimension = 0;
            synchronizedDimensionValue = 0;
            nbrOperationToSync = operationsToSync.size();
            pulledAnnotationTemplates = new ArrayList<AnnotationTemplate>();
            insertedAnnotationTemplates = new ArrayList<AnnotationTemplate>();
            insertedDimensions = new ArrayList<Dimension>();
            insertedSensorObjects = new ArrayList<SensorObject>();


            LoggerUtil.debug(LOG_TAG,"transaction with server");
            syncStep = SynchroStep.PULL_ANNOTATION_TEMPLATE;
            while(syncStep != SynchroStep.COMPLETE){
                LoggerUtil.debug(LOG_TAG,"Step : " + syncStep);
                sendProgress();
                SynchroStep next = syncStep.next();
                if(syncStep == SynchroStep.PULL_ANNOTATION_TEMPLATE){
                    VolleyHelper.GetAnnotationTemplate(this.getContext(), this);
                }else if(syncStep == SynchroStep.PUSH_ANNOTATION_TEMPLATE){
                    if(annotationTemplatesToSync.size()  == 0){
                        syncStep = syncStep.next();
                        continue;
                    }
                    for (AnnotationTemplate curAnnotationTemplate : annotationTemplatesToSync) {
                        VolleyHelper.PostAnnotationTemplate(this.getContext(), curAnnotationTemplate, this);
                    }
                }else if(syncStep == SynchroStep.PULL_SENSOR_OBJECT){
                    VolleyHelper.GetSensorObject(this.getContext(),this);
                }else if(syncStep == SynchroStep.PUSH_SENSOR_OBJECT){
                    if(sensorObjectsToSync.size()  == 0){
                        syncStep = syncStep.next();
                        continue;
                    }
                    for (SensorObject curSensorObject : sensorObjectsToSync) {
                        VolleyHelper.PostSensorObject(this.getContext(), curSensorObject, this);
                    }
                }else if(syncStep == SynchroStep.PULL_ANNOTATION_TEMPLATE_IMAGES){
                    for(int i = 0;i<pulledAnnotationTemplates.size();i++){
                        if(pulledAnnotationTemplates.get(i).getPhoto() != null && pulledAnnotationTemplates.get(i).getPhoto().startsWith("/media/")){
                            String filename= pulledAnnotationTemplates.get(i).getPhoto().substring(pulledAnnotationTemplates.get(i).getPhoto().lastIndexOf('/')+1);
                            AnnotationTemplate curAnnotationTemplate = pulledAnnotationTemplates.get(i);
                            curAnnotationTemplate.setPhoto(filename);
                            pulledAnnotationTemplates.set(i,curAnnotationTemplate);
                            /*
                             * Download the image if not already present on terminal
                             */
                            if(!new File(this.getContext().getFilesDir().getPath() + "/" +filename).exists()){
                                VolleyHelper.GetAnnotationTemplatePhoto(this.getContext(), pulledAnnotationTemplates.get(i),this);
                            }
                        }
                    }
                    syncStep =  syncStep.next();
                }else if(syncStep == SynchroStep.PULL_DIMENSION){
                    VolleyHelper.PullDimensions(this.getContext(),this);
                }else if(syncStep == SynchroStep.PUSH_DIMENSION){
                    if(dimensionToSync.size() == 0){
                        syncStep = syncStep.next();
                        continue;
                    }
                    for(Dimension dimension:dimensionToSync){
                        VolleyHelper.PushDimension(this.getContext(),dimension,this);
                    }
                }else if(syncStep == SynchroStep.PUSH_OPERATION_IMAGES){
                    for(Operation operationToSync :operationsToSync){
                        if(operationToSync.getPhoto() == null){
                            continue;
                        }else{
                            imageToSync.add(new OperationImage(operationToSync.getOperationId(),operationToSync.getPhoto()));
                        }
                    }
                    if(imageToSync.size() == 0){
                        syncStep = syncStep.next();
                        continue;
                    }
                    VolleyHelper.PostOperationImage(this.getContext(),imageToSync,this);
                }else if(syncStep == SynchroStep.PUSH_OPERATION){
                    if(operationsToSync.size() == 0){
                        syncStep = syncStep.next();
                        continue;
                    }
                    for(int i = 0;i<operationsToSync.size();i++){
                        int pgId = getAnnotationTemplatePostGreIdFromLocalId(insertedAnnotationTemplates,operationsToSync.get(i).getAnnotationTemplateId());
                        if(pgId == -1){
                            pgId = getAnnotationTemplatePostGreIdFromLocalId(allAnnotationTemplate,operationsToSync.get(i).getAnnotationTemplateId());
                            if(pgId == -1){
                                LoggerUtil.debug(LOG_TAG,"Error during synchronization at step : " + syncStep);
                                syncStep = SynchroStep.ERROR;
                                return;
                            }
                        }
                        operationsToSync.get(i).setAnnotationTemplateId(pgId);
                        pgId = getSensorObjectPostGreIdFromLocalId(insertedSensorObjects,operationsToSync.get(i).getSensorObjectId());
                        if(pgId == -1){
                            pgId = getSensorObjectPostGreIdFromLocalId(allSensorObject,operationsToSync.get(i).getSensorObjectId());
                            if(pgId == -1){
                                LoggerUtil.debug(LOG_TAG,"Error during synchronization at step : " + syncStep);
                                syncStep = SynchroStep.ERROR;
                                return;
                            }
                        }
                        operationsToSync.get(i).setSensorObjectId(pgId);
                    }
                    VolleyHelper.PostOperation(this.getContext(), operationsToSync, this);
                }else if(syncStep == SynchroStep.PULL_OPERATION){
                    VolleyHelper.PullOperations(this.getContext(),this);
                }else if(syncStep == SynchroStep.PUSH_DIMENSION_VALUE){
                    if(dimensionValuesToSync.size() == 0){
                        syncStep = syncStep.next();
                        continue;
                    }
                    for(int i = 0;i<dimensionValuesToSync.size();i++){
                        int pgId = getDimensionPostGreIdFromLocalId(insertedDimensions,dimensionValuesToSync.get(i).getDimensionId());
                        if(pgId == -1){
                            pgId = getDimensionPostGreIdFromLocalId(allDimension,dimensionValuesToSync.get(i).getDimensionId());
                            if(pgId == -1){
                                LoggerUtil.debug(LOG_TAG,"Error during synchronization at step : " + syncStep);
                                syncStep = SynchroStep.ERROR;
                                return;
                            }
                        }
                        dimensionValuesToSync.get(i).setDimensionId(pgId);
                    }
                    VolleyHelper.PushDimensionValues(this.getContext(),dimensionValuesToSync,this);
                }else if(syncStep == SynchroStep.PULL_DIMENSION_VALUE){
                    VolleyHelper.PullDimensionValues(this.getContext(),this);
                }else if(syncStep == SynchroStep.ERROR){
                    Bundle msg = new Bundle();
                    String message = new String("Synchronization error");
                    Intent i = new Intent(agribiotApplication.SYNC_FINISHED);
                    msg.putString("message",message);
                    i.putExtra("bundle",msg);
                    agribiotApplication.sendBroadcast(i);
                    return;
                }
                while (syncStep != next) {
                    try {
                        if(syncStep == SynchroStep.ERROR){
                            Bundle msg = new Bundle();
                            String message = new String("Synchronization error");
                            Intent i = new Intent(agribiotApplication.SYNC_FINISHED);
                            msg.putString("message",message);
                            i.putExtra("bundle",msg);
                            agribiotApplication.sendBroadcast(i);
                            return;
                        }
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        showError(e);
                    }
                }
            }

            LoggerUtil.debug(LOG_TAG,"commit to local database");

            /*
             * Merge both pulled and inserted annotation template in pulled list
             */
            for(int i = 0;i<insertedAnnotationTemplates.size();i++){
                for(int j  = 0;j< pulledAnnotationTemplates.size();j++){
                    if(pulledAnnotationTemplates.get(j).getPostgreId() == insertedAnnotationTemplates.get(i).getPostgreId()){
                        pulledAnnotationTemplates.remove(j);
                    }
                }
                pulledAnnotationTemplates.add(insertedAnnotationTemplates.get(i));
            }
            /*
             * Merge both pulled and inserted dimension in pulled list
             */
            for(int i = 0;i<insertedDimensions.size();i++){
                for(int j  = 0;j< pulledDimensions.size();j++){
                    if(pulledDimensions.get(j).getPostgreId() == insertedDimensions.get(i).getPostgreId()){
                        pulledDimensions.remove(j);
                    }
                }
                pulledDimensions.add(insertedDimensions.get(i));
            }
            /*
             * Merge both pulled and inserted sensor object in pulled list
             */
            for(int i = 0;i<insertedSensorObjects.size();i++){
                for(int j  = 0;j< pulledSensorObjects.size();j++){
                    if(pulledSensorObjects.get(j).getPostgreId() == insertedSensorObjects.get(i).getPostgreId()){
                        pulledSensorObjects.remove(j);
                    }
                }
                pulledSensorObjects.add(insertedSensorObjects.get(i));
            }

            annotationTemplateDao.deleteAll();
            annotationTemplateDao.insertAllAnnotationTemplates(pulledAnnotationTemplates);
            pulledAnnotationTemplates = (ArrayList<AnnotationTemplate>) annotationTemplateDao.getAllAsList();

            if(pulledSensorObjects != null){
                sensorObjectDao.deleteAndCreate(pulledSensorObjects);
            }
            pulledSensorObjects = (ArrayList<SensorObject>) sensorObjectDao.getAllAsList();

            operationDao.deleteAll();
            if(pulledOperation!= null){
                for(int i = 0;i<pulledOperation.size();i++){

                    /*
                        set AnnotationTemplateId to local annotation id
                    */
                    int localId = getAnnotationTemplateLocalIdFromPostGreId(pulledAnnotationTemplates,pulledOperation.get(i).getAnnotationTemplateId());
                    if(localId == -1){
                        LoggerUtil.debug(LOG_TAG,"unknow foreign key for annotation on operation : " + pulledOperation.get(i).toString());
                    }else{
                        pulledOperation.get(i).setAnnotationTemplateId(localId);
                    }

                    localId = getSensorObjectLocalIdFromPostGreId(pulledSensorObjects,pulledOperation.get(i).getSensorObjectId());
                    if(localId == -1){
                        LoggerUtil.debug(LOG_TAG,"unknow foreign key for sensor object on operation : " + pulledOperation.get(i).toString());
                    }else{
                        pulledOperation.get(i).setSensorObjectId(localId);
                    }
/*
                    LoggerUtil.debug(LOG_TAG,"insert operation : " + pulledOperation.get(i).toString());
                    operationDao.insert(pulledOperation.get(i));*/
                }
                if(pulledOperation.size()>0){
                    operationDao.insertAll(pulledOperation);
                }
            }

            if(pulledDimensions != null){
                dimensionDao.deleteAndCreate(pulledDimensions);
            }

            /**
             * We will now request local operations and dimensions to update foreign key id in DimensionsValues array to local one
             */
            pulledDimensions = (ArrayList<Dimension>) dimensionDao.getAllAsList();
            pulledOperation = (ArrayList<Operation>) operationDao.getAllAsList();


            dimensionValueDao.deleteAll();
            if(pulledDimensionValue!= null) {

                for (int i = 0; i < pulledDimensionValue.size(); i++) {
                    int localId = getOperationLocalIdFromPostGreId(pulledOperation, pulledDimensionValue.get(i).getOperationId());
                    if (localId == -1) {
                        LoggerUtil.debug(LOG_TAG, "unknow foreign key for operation on dimension value : " + pulledDimensionValue.get(i).toString());
                    } else {
                        pulledDimensionValue.get(i).setOperationId(localId);
                    }
                    localId = getDimensionLocalIdFromPostGreId(pulledDimensions, pulledDimensionValue.get(i).getDimensionId());
                    if (localId == -1) {
                        LoggerUtil.debug(LOG_TAG, "unknow foreign key for dimension on dimension value : " + pulledDimensionValue.get(i).toString());
                    } else {
                        pulledDimensionValue.get(i).setDimensionId(localId);
                    }
                }
                if(pulledDimensionValue.size()>0){
//                    LoggerUtil.debug(LOG_TAG,"try to insert " + pulledDimensions);
                    dimensionValueDao.insertAll(pulledDimensionValue);
                }
            }



            Bundle msg = new Bundle();
            String message = new String(cntInsertedAnnotation + " new annotation templates\r\n");
            message += imageToSync.size() + " new operation images\r\n";
            message += operationsToSync.size() + " new operations\r\n";
            msg.putString("message",message);
            Intent i = new Intent(agribiotApplication.SYNC_FINISHED);
            i.putExtra("bundle",msg);
            agribiotApplication.sendBroadcast(i);
        });
    }
    public void showError(Exception e) {
        LoggerUtil.debug(LOG_TAG,"showError: " + e.getMessage());
        e.printStackTrace();
        Intent i = new Intent(agribiotApplication.SYNC_ERROR);
        Bundle msg = new Bundle();
        String message =  e.getMessage();
        msg.putString("message",message);
        i.putExtra("bundle",msg);
        agribiotApplication.sendBroadcast(i);
    }
    public void showErrorString(String e) {
        LoggerUtil.debug(LOG_TAG,"showErrorString: " + e);
        Intent i = new Intent(agribiotApplication.SYNC_ERROR);
        Bundle msg = new Bundle();
        String message =  e;
        msg.putString("message",message);
        i.putExtra("bundle",msg);
        agribiotApplication.sendBroadcast(i);
    }

    @Override
    public void onAnnotationTemplateFetch(ArrayList<AnnotationTemplate> annotationTemplates) {
        pulledAnnotationTemplates = annotationTemplates;
        syncStep = syncStep.next();
    }

    @Override
    public void onOperationFetch(ArrayList<Operation> operations) {
        pulledOperation = operations;
        syncStep = syncStep.next();
    }

    @Override
    public void onDimensionsFetch(ArrayList<Dimension> dimensions) {
        pulledDimensions = dimensions;
        syncStep = syncStep.next();
    }

    @Override
    public void onDimensionUploaded(Dimension dimensionToInsert, Dimension dimensionSaved) {

        /**/
        cntInsertedDimension++;
        /*
         * If the dimension is null an error occur
         * It may be another client that insert the same Dimension
         * We try to get it from previously fetched dimension
         */
        LoggerUtil.debug(LOG_TAG,dimensionSaved + " " + pulledDimensions.size() );
        if(dimensionSaved == null) {
            for (Dimension curDimension : pulledDimensions) {
                if(curDimension.equals(dimensionToInsert)) {
                    dimensionSaved = curDimension;
                    break;
                }
            }
        }
        /*
         * If still null it's mean the dimension has not be inserted by another client, and unknown error occur
         */
        if(dimensionSaved == null){
            LoggerUtil.debug(LOG_TAG,"Error during synchronization at step : " + syncStep + " dimension not found");
            syncStep = SynchroStep.ERROR;
            return;
        }
        /*
         * We kept back the old Room entry PK and set it to the inserted one
         * This is important to keep track for the insertion of newly created operation into server
         */
        dimensionSaved.setDimensionId(dimensionToInsert.getDimensionId());
        dimensionSaved.setSynchronizedFlag(true);
        synchronized (insertedDimensions){
            insertedDimensions.add(dimensionSaved);
            if(cntInsertedDimension == dimensionToSync.size() && syncStep != SynchroStep.ERROR){
                syncStep = syncStep.next();
            }
        }
    }

    @Override
    public void onDimensionsValueFetch(ArrayList<DimensionValue> dimensionsValues) {
        pulledDimensionValue = dimensionsValues;
        syncStep = syncStep.next();
        LoggerUtil.debug(LOG_TAG,"dimension value fetched");
    }

    @Override
    public void onDimensionValueUploaded(DimensionValue dimensionValueToInsert, DimensionValue insertedDimensionValue) {
        synchronizedDimensionValue++;
        if(synchronizedDimensionValue == dimensionValuesToSync.size()){
            syncStep = syncStep.next();
        }
    }

    @Override
    public void onSensorObjectInserted(SensorObject sensorObjectToInsert, SensorObject sensorObjectInserted) {
        cntInsertedSensorObject++;
        /*
         * If the insertedSensorObject is null an error occur
         * It may be another client that insert the same SensorObject
         * We try to get it from previously fetched sensor object
         */
        if(sensorObjectInserted == null) {
            for (SensorObject curSensorObject : pulledSensorObjects) {
                if(curSensorObject.equals(sensorObjectToInsert)) {
                    sensorObjectInserted = curSensorObject;
                    break;
                }
            }
        }
        /*
         * If still null it's mean the sensor object has not be inserted by another client, and unknown error occur
         */
        if(sensorObjectInserted == null){
            LoggerUtil.debug(LOG_TAG,"Error during synchronization at step : " + syncStep + " sensor object not found");
            //showErrorString("Error during synchronization at step : " + syncStep);
            syncStep = SynchroStep.ERROR;
            return;
        }
        /*
         * We kept back the old Room entry PK and set it to the inserted one
         * This is important to keep track for the insertion of newly created operation into server
         */
        sensorObjectInserted.setSensorObjectId(sensorObjectToInsert.getSensorObjectId());
        sensorObjectInserted.setSynchronizedFlag(true);
        synchronized (insertedSensorObjects){
            insertedSensorObjects.add(sensorObjectInserted);
            if(cntInsertedSensorObject == sensorObjectsToSync.size() && syncStep != SynchroStep.ERROR){
                syncStep = syncStep.next();
            }
        }
    }

    @Override
    public void onAnnotationTemplateInserted(AnnotationTemplate annotationTemplateToInsert,AnnotationTemplate annotationTemplateInserted){
        cntInsertedAnnotation++;
        /*
         * If the insertedAnnotationTemplate is null an error occur
         * It may be another client that insert the same AnnotationTemplate description
         * We try to get it from previously fetched annotation template
         */
        if(annotationTemplateInserted == null) {
            for (AnnotationTemplate curAnnotationTemplate : pulledAnnotationTemplates) {
                if(curAnnotationTemplate.equals(annotationTemplateToInsert)) {
                    annotationTemplateInserted = curAnnotationTemplate;
                    break;
                }
            }
        }
        /*
         * If still null it's mean the annotation template has not be inserted by another client, and unknown error occur
         */
        if(annotationTemplateInserted == null){
            LoggerUtil.debug(LOG_TAG,"Error during synchronization at step : " + syncStep + " annotation template not found");
            //showErrorString("Error during synchronization at step : " + syncStep);
            syncStep = SynchroStep.ERROR;
            return;
        }
        /*
         * We kept back the old Room entry PK and set it to the inserted one
         * This is important to keep track for the insertion of newly created operation into server
         */
        annotationTemplateInserted.setAnnotationTemplateId(annotationTemplateToInsert.getAnnotationTemplateId());
        annotationTemplateInserted.setSynchronizedFlag(true);
        synchronized (insertedAnnotationTemplates){
            insertedAnnotationTemplates.add(annotationTemplateInserted);
            if(cntInsertedAnnotation == annotationTemplatesToSync.size() && syncStep != SynchroStep.ERROR){
                syncStep = syncStep.next();
            }
        }
    }

    @Override
    public void onSensorObjectFetch(ArrayList<SensorObject> sensorObjects) {
        pulledSensorObjects = sensorObjects;
        syncStep = syncStep.next();
    }

    @Override
    public void onOperationImageUploaded(ArrayList<OperationImage> imageToInsert, OperationImage insertedImage) {
        if(imageToInsert.size() == 0 && insertedImage == null){
            synchronizedImage = 0;
            syncStep = syncStep.next();
            return;
        }else{
            LoggerUtil.debug(LOG_TAG,"Have inserted photo for operation" + insertedImage.toString());
            for(int i =  0;i<operationsToSync.size();i++){
                if(operationsToSync.get(i).getOperationId() == insertedImage.getOperationId()){
                    operationsToSync.get(i).setPhotoPostGreId(insertedImage.getPostgreId());
                }
            }
        }
        synchronizedImage ++;
        if(synchronizedImage == imageToInsert.size()){
            synchronizedImage = 0;
            syncStep = syncStep.next();
        }
    }

    @Override
    public void onOperationInserted(Operation operationToInsert, Operation operationSaved){
        insertedOperation.add(operationSaved);
        for(int i = 0;i<dimensionValuesToSync.size();i++){
            if(dimensionValuesToSync.get(i).getOperationId() == operationToInsert.getOperationId() ){
                dimensionValuesToSync.get(i).setOperationId(operationSaved.getPostgreId());
            }
        }

        synchronizedOperation ++;
        if(synchronizedOperation == operationsToSync.size()) {
            synchronizedOperation = 0;
            syncStep = syncStep.next();

        }
    }

    @Override
    public void onAnnotationTemplatePhotoFetch(AnnotationTemplate annotationTemplate, Bitmap response) {
        LoggerUtil.debug(LOG_TAG,"trying to save bitmap to " + this.getContext().getFilesDir().getPath() + "/" + annotationTemplate.getPhoto().substring(annotationTemplate.getPhoto().lastIndexOf('/')+1));
        try (FileOutputStream out = new FileOutputStream(new File(this.getContext().getFilesDir(), annotationTemplate.getPhoto().substring(annotationTemplate.getPhoto().lastIndexOf('/')+1)))){
            response.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (IOException e) {
            this.showError(e);
        }
    }

    @Override
    public void onError(Exception e) {
        this.showError(e);
        syncStep = SynchroStep.ERROR;
    }

    public void sendProgress(){
        Intent i = new Intent(agribiotApplication.SYNC_PROGRESS);
        i.putExtra("step",syncStep.ordinal());
        agribiotApplication.sendBroadcast(i);
    }

    @Override
    public void onVolleyError(VolleyError e) {

        LoggerUtil.debug(LOG_TAG," e.getMessage : " + e.getMessage());

        if (e.networkResponse != null) {
            NetworkResponse networkResponse = e.networkResponse;
            if (networkResponse != null && networkResponse.data != null) {
                /**
                 *
                 */
                if(e.networkResponse.statusCode == 500){ //  Internal Server Error
                    String jsonError = new String("Internal server error");
                    this.showErrorString("step : " + syncStep  + " " + jsonError  );

                }else{
                    String jsonError = new String(networkResponse.data);
                    LoggerUtil.debug(LOG_TAG,e.networkResponse.statusCode + " error : " + jsonError);
                    this.showErrorString("Error : " + jsonError + " step : " + syncStep );
                }
            }else{
                this.showErrorString("Error http  : " + e.networkResponse.statusCode + " step : " + syncStep );
            }
        } else if (e.getClass() == NoConnectionError.class) {
            this.showErrorString("Server unreachable" + " step : " + syncStep);
        } else if(e.getCause() != null) {
            if(e.getCause().toString().contains("java.lang.RuntimeException: Bad URL")){
                this.showErrorString("Unknown HTTP error : " + e.getCause() + " step : " + syncStep);
            }
        }else{
            this.showErrorString( "Unknown HTTP error : " + e.getCause() + " step : " + syncStep);
        }
        syncStep = SynchroStep.ERROR;
    }
}
