/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;

import androidx.multidex.MultiDexApplication;

import java.util.Iterator;
import java.util.List;

import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.rfid.RFIDHandler;

public class AgribiotApplication extends MultiDexApplication {
    static final String LOG_TAG = "AgribiotApplication";

    private RFIDHandler rfidHandler;
    // Constants
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "agribiot.bzh";
    // The account name
    public static final String ACCOUNT = "placeholderaccount";

    public static final String SYNC_FINISHED = "bzh.agribiot.datasync.finished";
    public static final String SYNC_PROGRESS = "bzh.agribiot.datasync.progress";
    public static final String SYNC_ERROR = "bzh.agribiot.datasync.error";




    private Account mAccount;

    public RFIDHandler getRfidHandler() {
        return rfidHandler;
    }
    public Account getAccount() {
        return mAccount;
    }

    public void setRfidHandler(RFIDHandler rfidHandler) {
        this.rfidHandler = rfidHandler;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!

        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) super.getApplicationContext().getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
        } else {
            LoggerUtil.debug(LOG_TAG,"Unable to add account");
        }
        mAccount = newAccount;

    }
    public boolean isServiceRunning(String serviceName){
        boolean serviceRunning = false;
        ActivityManager am = (ActivityManager) this.getSystemService(this.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> l = am.getRunningServices(50);
        Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
        while (i.hasNext()) {
            ActivityManager.RunningServiceInfo runningServiceInfo = i
                    .next();
            //LoggerUtil.debug(LOG_TAG,"service " + runningServiceInfo.service.getClassName());
            if(runningServiceInfo.service.getClassName().equals(serviceName)){
                serviceRunning = true;

                if(runningServiceInfo.foreground)
                {
                    //service run in foreground
                }
            }
        }
        return serviceRunning;
    }

}
