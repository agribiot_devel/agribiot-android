/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

@Dao
public abstract class DimensionValueDao {
    @Query("SELECT * FROM dimensionValue")
    public abstract LiveData<List<DimensionValue>> getAll();

    @Query("SELECT * FROM dimensionValue")
    public abstract List<DimensionValue> getAllAsList();

    @Query("SELECT * FROM dimensionValue WHERE dv_operation_id LIKE :operationId")
    public abstract LiveData<List<DimensionValue>> findByOperation(int operationId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<DimensionValue> dimensions);

    @Query("DELETE FROM dimensionValue")
    public abstract void deleteAll();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insert(DimensionValue dimensionValue);

    @Update
    public abstract int update(DimensionValue dimensionValue);

    /*@Query("SELECT * FROM dimensionvalue WHERE operation_id IN (SELECT id FROM operation WHERE annotation_template_id LIKE :annotationTemplateId AND operation_synchronizedFlag LIKE 1 ORDER BY created DESC LIMIT 1)")
    public abstract LiveData<List<DimensionValue>> lastUsedDimensionValue(int annotationTemplateId);
*/
    @Query("SELECT * FROM dimensionvalue WHERE dv_operation_id IN (SELECT op_id FROM operation WHERE op_annotation_template_id LIKE :annotationTemplateId ORDER BY op_created DESC LIMIT 1 OFFSET 1)")
    public abstract LiveData<List<DimensionValue>> lastUsedDimensionValue(int annotationTemplateId);
    @Delete
    public abstract void delete(DimensionValue dimensionValue);

    @Query("SELECT * FROM dimensionvalue WHERE dv_synchronizedFlag LIKE :synchronizedFlag")
    public abstract List<DimensionValue> loadAllBySynchronizedFlag(int synchronizedFlag);
}
