/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

@Dao
public abstract class DimensionDao {
    @Query("SELECT * FROM dimension")
    public abstract LiveData<List<Dimension>> getAll();

    @Query("SELECT * FROM dimension")
    public abstract List<Dimension> getAllAsList();

    @Transaction
    public void deleteAndCreate(List<Dimension> dimensions) {
        deleteAll();
        insertAll(dimensions);
    }
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<Dimension> dimensions);

    @Query("DELETE FROM dimension")
    public abstract void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insert(Dimension dimension);
    @Update
    public abstract int update(Dimension dimension);

    @Query("SELECT * FROM dimension WHERE d_synchronizedFlag LIKE :synchronizedFlag")
    public abstract List<Dimension> loadAllBySynchronizedFlag(int synchronizedFlag);

    @Query("SELECT * FROM dimension WHERE d_id IN (SELECT dv_dimension_id FROM dimensionvalue WHERE dv_operation_id IN (SELECT op_id FROM operation WHERE op_annotation_template_id LIKE :annotationTemplateId))")
    public abstract LiveData<List<Dimension>> getUsedDimensionForAnnotationId(int annotationTemplateId);
}
