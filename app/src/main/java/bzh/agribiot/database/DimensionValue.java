/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import bzh.agribiot.logger.LoggerUtil;

@Entity(foreignKeys = {
        @ForeignKey(entity = Dimension.class,
                parentColumns = "d_id",
                childColumns = "dv_dimension_id",
                onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = Operation.class,
                parentColumns = "op_id",
                childColumns = "dv_operation_id",
                onDelete = ForeignKey.CASCADE)
        })
public class DimensionValue {
    static final String LOG_TAG = "DimensionValue";

    @ColumnInfo(name = "dv_id")
    @PrimaryKey(autoGenerate = true)
    private int dimensionValueId;
    @ColumnInfo(name = "dv_postgre_id")
    private int postgreId;

    @ColumnInfo(name = "dv_value")
    private Double value = 0.0;

    @ColumnInfo(name = "dv_dimension_id", index = true)
    private int dimensionId;
    @ColumnInfo(name = "dv_operation_id", index = true)
    private int operationId;

    @ColumnInfo(name = "dv_synchronizedFlag")
    private boolean synchronizedFlag = false;

    public int getDimensionValueId() {
        return dimensionValueId;
    }

    public void setDimensionValueId(int dimensionValueId) {
        this.dimensionValueId = dimensionValueId;
    }

    public int getPostgreId() {
        return postgreId;
    }

    public void setPostgreId(int postgreId) {
        this.postgreId = postgreId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public int getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(int dimensionId) {
        this.dimensionId = dimensionId;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }
    public boolean isSynchronizedFlag() {
        return synchronizedFlag;
    }

    public void setSynchronizedFlag(boolean synchronizedFlag) {
        this.synchronizedFlag = synchronizedFlag;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DimensionValue that = (DimensionValue) o;
        return dimensionId == that.dimensionId &&
                operationId == that.operationId &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, dimensionId, operationId);
    }

    @Override
    public String toString() {
        return "DimensionValue{" +
                "dimensionValueId=" + dimensionValueId +
                ", postgreId=" + postgreId +
                ", value=" + value +
                ", dimensionId=" + dimensionId +
                ", operationId=" + operationId +
                ", synchronizedFlag=" + synchronizedFlag +
                '}';
    }

    public DimensionValue(){

    }
    public DimensionValue(JSONObject dimensionValueJson) throws JSONException {
        this.setValue(dimensionValueJson.getDouble("value"));
        this.setDimensionId(dimensionValueJson.getInt("dimension_id"));
        this.setOperationId(dimensionValueJson.getInt("operation_id"));
        this.setPostgreId(dimensionValueJson.getInt("id"));
        this.setSynchronizedFlag(true);
    }
    public static List<DimensionValue> ParseJsonArray(String dimensionValueList) throws JSONException, ParseException {
        JSONArray dimensionValueListJson = new JSONArray(dimensionValueList);
        ArrayList<DimensionValue> dimensionValueArray = new ArrayList<DimensionValue>();

        for (int i = 0; i < dimensionValueListJson.length(); i++) {
            dimensionValueArray.add(new DimensionValue(dimensionValueListJson.getJSONObject(i)));
        }
        return dimensionValueArray;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject dimJson = new JSONObject();
        dimJson.put("dimension_id",this.dimensionId);
        dimJson.put("operation_id",this.operationId);
        dimJson.put("value",this.value);
        return dimJson;
    }
}
