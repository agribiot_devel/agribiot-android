/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import bzh.agribiot.logger.LoggerUtil;

@Entity(indices = {@Index(value = {"at_description","at_annotation_type"},unique = true)})
public class AnnotationTemplate implements Serializable {
    @ColumnInfo(name = "at_id")
    @PrimaryKey(autoGenerate = true)
    private int annotationTemplateId;
    @ColumnInfo(name = "at_postgre_id")
    private int postgreId;
    @ColumnInfo(name = "at_description")
    private String description;

    @ColumnInfo(name = "at_annotation_type")
    private String annotationType;


    @ColumnInfo(name = "at_photo")
    private String photo;

    @ColumnInfo(name = "at_synchronizedFlag")
    private boolean synchronizedFlag = false;
    static String LOG_TAG = "AnnotationTemplate";

    public static AnnotationTemplate ParseJson(JSONObject annotationTemplate)throws JSONException, UnsupportedEncodingException{
        AnnotationTemplate nAnnotationTemplate = new AnnotationTemplate();
        nAnnotationTemplate.setPostgreId(annotationTemplate.getInt("id"));
        nAnnotationTemplate.setDescription(new String(annotationTemplate.getString("description").getBytes("ISO-8859-1"), "UTF-8"));
        nAnnotationTemplate.setPhoto(new String(annotationTemplate.optString("photo","").getBytes("ISO-8859-1"), "UTF-8"));
        nAnnotationTemplate.setAnnotationType(annotationTemplate.getString("annotation_type"));
        nAnnotationTemplate.setSynchronizedFlag(true);
        return nAnnotationTemplate;
    }
    public static List<AnnotationTemplate> ParseJsonArray(String annotationTemplateList) throws JSONException, UnsupportedEncodingException {
        JSONArray annotationTemplateListJson = new JSONArray(annotationTemplateList);
        ArrayList<AnnotationTemplate> annotationTemplateArray = new ArrayList<AnnotationTemplate>();

        for (int i = 0; i < annotationTemplateListJson.length(); i++) {
            annotationTemplateArray.add(ParseJson(annotationTemplateListJson.getJSONObject(i)));
        }
        return annotationTemplateArray;
    }
    public JSONObject toJson() throws JSONException {
        JSONObject annotationTemplateJson = new JSONObject();
        annotationTemplateJson.put("description",this.getDescription());
        annotationTemplateJson.put("annotation_type",this.getAnnotationType());
        return annotationTemplateJson;
    }

    public int getAnnotationTemplateId() {
        return annotationTemplateId;
    }

    public void setAnnotationTemplateId(int id) {
        this.annotationTemplateId = id;
    }

    public int getPostgreId() {
        return postgreId;
    }

    public void setPostgreId(int postgreId) {
        this.postgreId = postgreId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnnotationType() {
        return annotationType;
    }

    public void setAnnotationType(String annotationType) {
        this.annotationType = annotationType;
    }

    public boolean isSynchronizedFlag() {
        return synchronizedFlag;
    }

    public void setSynchronizedFlag(boolean synchronizedFlag) {
        this.synchronizedFlag = synchronizedFlag;
    }

    public String toString(){
        return new String("AnnotationTemplate id : " + this.annotationTemplateId + " PostGreId " + this.postgreId + " description : " + this.description + " annotatonType : " + this.annotationType + " synchronized : " + this.synchronizedFlag + " photo " + photo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnnotationTemplate that = (AnnotationTemplate) o;
        return  description.equals(that.description) &&
                annotationType.equals(that.annotationType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(annotationTemplateId, postgreId, description, annotationType, photo,synchronizedFlag);
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
