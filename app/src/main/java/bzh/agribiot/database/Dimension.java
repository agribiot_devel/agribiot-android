/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Dimension {
    static final String LOG_TAG = "Dimension";

    @ColumnInfo(name = "d_id")
    @PrimaryKey(autoGenerate = true)
    private int dimensionId;
    @ColumnInfo(name = "d_postgre_id")
    private int postgreId;
    @ColumnInfo(name = "d_name")
    private String name;
    @ColumnInfo(name = "d_unit")
    private String unit;

    @ColumnInfo(name = "d_synchronizedFlag")
    private boolean synchronizedFlag = false;

    public int getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(int dimensionId) {
        this.dimensionId = dimensionId;
    }

    public int getPostgreId() {
        return postgreId;
    }

    public void setPostgreId(int postgreId) {
        this.postgreId = postgreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dimension dimension = (Dimension) o;
        return Objects.equals(name, dimension.name) &&
                Objects.equals(unit, dimension.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, unit);
    }
    public boolean isSynchronizedFlag() {
        return synchronizedFlag;
    }

    public void setSynchronizedFlag(boolean synchronizedFlag) {
        this.synchronizedFlag = synchronizedFlag;
    }
    @Override
    public String toString() {
        return "Dimension{" +
                "dimensionId=" + dimensionId +
                ", postgreId=" + postgreId +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", synchronizedFlag='" + synchronizedFlag + '\'' +

                '}';
    }

    public Dimension(){

    }
    public Dimension(JSONObject dimensionJson) throws JSONException, UnsupportedEncodingException {
        this.setName(new String(dimensionJson.optString("name","").getBytes("ISO-8859-1"),"UTF-8"));
        this.setUnit(new String(dimensionJson.optString("unit","").getBytes("ISO-8859-1"),"UTF-8"));
        this.setPostgreId(dimensionJson.getInt("id"));
        this.setSynchronizedFlag(true);
    }
    public static List<Dimension> ParseJsonArray(String dimensionList) throws JSONException, ParseException, UnsupportedEncodingException {
        JSONArray dimensionListJson = new JSONArray(dimensionList);
        ArrayList<Dimension> dimensionArray = new ArrayList<Dimension>();

        for (int i = 0; i < dimensionListJson.length(); i++) {
            dimensionArray.add(new Dimension(dimensionListJson.getJSONObject(i)));
        }
        return dimensionArray;
    }
    public JSONObject toJson() throws JSONException {
        JSONObject dimJson = new JSONObject();
        dimJson.put("name",this.getName());
        dimJson.put("unit",this.getUnit());
        return dimJson;
    }
}
