package bzh.agribiot.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;
@Dao
public abstract class OperationWithAllForeignDao {
    @Transaction
    @Query("SELECT Operation.*, AnnotationTemplate.*, COUNT(DimensionValue.dv_operation_id) as nbr_dimension_value FROM Operation INNER JOIN AnnotationTemplate ON Operation.op_annotation_template_id = AnnotationTemplate.at_id LEFT JOIN DimensionValue ON Operation.op_id = DimensionValue.dv_operation_id GROUP BY Operation.op_id")
    public abstract LiveData<List<OperationWithAllForeign>> findAll();

    @Transaction
    @Query("SELECT Operation.*, AnnotationTemplate.*, SensorObject.*, COUNT(DimensionValue.dv_operation_id) as nbr_dimension_value FROM Operation INNER JOIN AnnotationTemplate ON Operation.op_annotation_template_id = AnnotationTemplate.at_id INNER JOIN SensorObject ON Operation.op_sensor_object_id = SensorObject.so_id LEFT JOIN DimensionValue ON Operation.op_id = DimensionValue.dv_operation_id  WHERE Operation.op_annotation_template_id LIKE :annotationTemplateId  GROUP BY Operation.op_id ")
    public abstract LiveData<List<OperationWithAllForeign>> findAllByAnnotationTemplateId(int annotationTemplateId);

    @Transaction
    @Query("SELECT Operation.*, AnnotationTemplate.*, SensorObject.*, COUNT(DimensionValue.dv_operation_id) as nbr_dimension_value FROM Operation INNER JOIN AnnotationTemplate ON Operation.op_annotation_template_id = AnnotationTemplate.at_id INNER JOIN SensorObject ON Operation.op_sensor_object_id = SensorObject.so_id LEFT JOIN DimensionValue ON Operation.op_id = DimensionValue.dv_operation_id  WHERE Operation.op_id LIKE :operationId")
    public abstract LiveData<OperationWithAllForeign> get(int operationId);
}
