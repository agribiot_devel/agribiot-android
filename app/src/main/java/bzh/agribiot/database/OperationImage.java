/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

public class OperationImage {
    @ColumnInfo(name = "image_id")
    @PrimaryKey(autoGenerate = true)
    private int imageId;

    @ColumnInfo(name = "image_postgre_id")
    private int postgreId;

    @ColumnInfo(name = "photo")
    private String photo;


    public long operationId;

    public OperationImage(int operationId,String photoPath){
        this.photo = photoPath;
        this.operationId = operationId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getPostgreId() {
        return postgreId;
    }

    public void setPostgreId(int postgreId) {
        this.postgreId = postgreId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public long getOperationId() {
        return operationId;
    }

    public void setOperationId(long operationId) {
        this.operationId = operationId;
    }

    @Override
    public String toString() {
        return "OperationImage{" +
                "imageId=" + imageId +
                ", postgreId=" + postgreId +
                ", photo='" + photo + '\'' +
                '}';
    }
}
