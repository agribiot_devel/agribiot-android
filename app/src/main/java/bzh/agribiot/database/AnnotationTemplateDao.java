/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import android.database.sqlite.SQLiteConstraintException;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public abstract class AnnotationTemplateDao {
    @Query("SELECT * FROM annotationTemplate")
    public abstract LiveData<List<AnnotationTemplate>> getAll();

    @Query("SELECT * FROM annotationTemplate")
    public abstract List<AnnotationTemplate> getAllAsList();

    @Query("SELECT * FROM annotationTemplate WHERE at_id IN (:annotationTemplateIds)")
    public abstract List<AnnotationTemplate> loadAllByIds(int[] annotationTemplateIds);

    @Query("SELECT * FROM annotationTemplate WHERE at_postgre_id IN (:postGreId)")
    public abstract List<AnnotationTemplate> loadAllByPostGreIds(int[] postGreId);

    @Query("DELETE FROM annotationTemplate WHERE at_postgre_id NOT IN (:postGreId)")
    public abstract void deleteAllByNotPostGreIds(int[] postGreId);

    @Query("SELECT * FROM annotationTemplate WHERE at_annotation_type LIKE :annotationType")
    public abstract LiveData<List<AnnotationTemplate>> loadAllByType(String annotationType);

    @Query("SELECT * FROM annotationTemplate WHERE at_synchronizedFlag LIKE :synchronizedFlag")
    public abstract List<AnnotationTemplate> loadAllBySynchronizedFlag(int synchronizedFlag);

    @Query("SELECT * FROM annotationTemplate WHERE at_id LIKE :id")
    public abstract AnnotationTemplate get(int id);

    @Query("SELECT * FROM annotationTemplate WHERE at_id LIKE :id")
    public abstract LiveData<AnnotationTemplate> getLive(int id);

    @Query("SELECT * FROM annotationTemplate WHERE at_description LIKE :description LIMIT 1")
    public abstract AnnotationTemplate findByDescription(String description);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public abstract long insertAnnotationTemplate(AnnotationTemplate annotationTemplate) throws SQLiteConstraintException;

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long[] insertAllAnnotationTemplates(List<AnnotationTemplate> annotationTemplates);

    @Update
    public abstract int updateAnnotationTemplate(AnnotationTemplate annotationTemplate);

    @Delete
    public abstract void delete(AnnotationTemplate annotationTemplate);

    @Query("DELETE FROM annotationTemplate")
    public abstract void deleteAll();


    // https://stackoverflow.com/questions/45059942/return-type-for-android-room-joins
    @Transaction
    @Query("SELECT * FROM annotationTemplate")
    public abstract LiveData<List<AnnotationWithOperations>> getAllLiveAnnotationWithOperations();
    @Transaction
    @Query("SELECT * FROM annotationTemplate")
    public abstract List<AnnotationWithOperations> getAllAnnotationWithOperations();


}
