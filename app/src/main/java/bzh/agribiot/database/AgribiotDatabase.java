/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {AnnotationTemplate.class,Operation.class,SensorObject.class,Dimension.class,DimensionValue.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AgribiotDatabase extends RoomDatabase {
    public abstract AnnotationTemplateDao annotationTemplateDao();
    public abstract OperationDao operationDao();
    public abstract OperationWithAllForeignDao operationWithAllForeignDao();

    public abstract SensorObjectDao sensorObjectDao();
    public abstract DimensionDao dimensionDao();
    public abstract DimensionValueDao dimensionValueDao();

    private static volatile AgribiotDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    /**
     * enableMultiInstanceInvalidation : https://developer.android.com/reference/androidx/room/RoomDatabase.Builder#enableMultiInstanceInvalidation()
     * If not set livedata will not be updated in main application thread by Sync Adapter thread
     */
    public static AgribiotDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AgribiotDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AgribiotDatabase.class, "agribiot_database").enableMultiInstanceInvalidation()
                            .build();

                }
            }
        }
        if(INSTANCE.isOpen() == false){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AgribiotDatabase.class, "agribiot_database").enableMultiInstanceInvalidation()
                    .build();
        }
        return INSTANCE;
    }

}
