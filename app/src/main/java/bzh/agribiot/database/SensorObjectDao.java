/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

@Dao
public abstract class SensorObjectDao {

    @Query("SELECT * FROM sensorObject")
    public abstract LiveData<List<SensorObject>> getAll();

    @Query("SELECT * FROM sensorObject")
    public abstract List<SensorObject> getAllAsList();

    @Transaction
    public void deleteAndCreate(List<SensorObject> sensorObjects) {
        deleteAll();
        insertAll(sensorObjects);
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<SensorObject> sensorObjects);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insert(SensorObject sensorObject);
    @Query("DELETE FROM sensorObject")
    public abstract void deleteAll();

    @Query("SELECT * FROM sensorObject WHERE so_uuid LIKE :tid LIMIT 1")
    public abstract SensorObject findSensorObject(String tid);
    @Update
    public abstract int updateSensorObject(SensorObject sensorObject);

    @Query("SELECT * FROM sensorObject WHERE so_synchronizedFlag LIKE :synchronizedFlag")
    public abstract List<SensorObject> loadAllBySynchronizedFlag(int synchronizedFlag);
}
