/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import bzh.agribiot.logger.LoggerUtil;

@Entity(foreignKeys = {@ForeignKey(entity = AnnotationTemplate.class,
        parentColumns = "at_id",
        childColumns = "op_annotation_template_id",
        onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = SensorObject.class,
                parentColumns = "so_id",
                childColumns = "op_sensor_object_id",
                onDelete = ForeignKey.CASCADE)
        })
public class Operation implements Serializable {

    static final String LOG_TAG = "Operation";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "op_id")
    private int operationId;

    @ColumnInfo(name = "op_postgre_id")
    private int postgreId;

    @ColumnInfo(name = "op_imported")
    private Date imported;

    @ColumnInfo(name = "op_created")
    private Date created;

    @ColumnInfo(name = "op_reader_uuid")
    private String readerUuid;

    @ColumnInfo(name = "op_tag_embedded_task_index")
    private int tagEmbeddedTaskIndex;

    @ColumnInfo(name = "op_annotation_template_id", index = true)
    private int annotationTemplateId;

    @ColumnInfo(name = "op_sensor_object_id", index = true)
    private int sensorObjectId;

    @ColumnInfo(name = "op_lat")
    private Double opLat = 0.0;
    @ColumnInfo(name = "op_lon")
    private Double opLon = 0.0;

    @ColumnInfo(name = "op_accuracy")
    private float opAccuracy;

    @ColumnInfo(name = "op_synchronizedFlag")
    private boolean synchronizedFlag = false;

    @ColumnInfo(name = "op_photo")
    private String photo;

    @ColumnInfo(name = "op_photo_postgre_id")
    private int photoPostGreId;

    public Operation(){

    }
    public Operation(JSONObject operationJson) throws JSONException, ParseException {
        this.postgreId = operationJson.getInt("id");
        DateFormat dateFormat;
        try{
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'");
            this.imported = dateFormat.parse(operationJson.getString("imported"));
        }catch(ParseException e){
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            this.imported = dateFormat.parse(operationJson.getString("imported"));
        }
        try{
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'");
            this.created = dateFormat.parse(operationJson.getString("created"));
        }catch(ParseException e){
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            this.created = dateFormat.parse(operationJson.getString("created"));
        }
        this.readerUuid = operationJson.optString("reader_uui","");
        this.tagEmbeddedTaskIndex = operationJson.getInt("tag_embedded_task_index");
        this.annotationTemplateId = operationJson.getInt("annotation_template_id");
        this.sensorObjectId = operationJson.getInt("sensor_object_id");
        this.opLat = 0.0;
        this.opLon = 0.0;
        this.opAccuracy =  0.0f;
        this.setSynchronizedFlag(true);
    }
    public static List<Operation> ParseJsonArray(String operationList) throws JSONException, ParseException {
        JSONArray operationListJson = new JSONArray(operationList);
        ArrayList<Operation> operationArray = new ArrayList<Operation>();

        for (int i = 0; i < operationListJson.length(); i++) {
            operationArray.add(new Operation(operationListJson.getJSONObject(i)));
        }
        return operationArray;
    }
    public JSONObject toJson() throws JSONException {
        JSONObject operationJson = new JSONObject();
        operationJson.put("tag_embedded_task_index",this.getTagEmbeddedTaskIndex());
        // Todo timezone ?
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String strDate = dateFormat.format(this.getCreated());
        operationJson.put("created",strDate);
        operationJson.put("annotation_template_id",this.getAnnotationTemplateId());
        operationJson.put("lat",this.getOpLat());
        operationJson.put("lon",this.getOpLon());
        operationJson.put("accuracy",this.getOpAccuracy());
        operationJson.put("image_id",this.getPhotoPostGreId());
        operationJson.put("sensor_object_id",this.getSensorObjectId());

        return operationJson;
    }

    public int getOperationId() {
        return operationId;
    }

    public int getPostgreId() {
        return postgreId;
    }

    public Date getImported() {
        return imported;
    }

    public Date getCreated() {
        return created;
    }

    public String getReaderUuid() {
        return readerUuid;
    }

    public int getTagEmbeddedTaskIndex() {
        return tagEmbeddedTaskIndex;
    }

    public int getAnnotationTemplateId() {
        return annotationTemplateId;
    }

    public Double getOpLat() {
        return opLat;
    }

    public Double getOpLon() {
        return opLon;
    }

    public float getOpAccuracy() {
        return opAccuracy;
    }

    public boolean isSynchronizedFlag() {
        return synchronizedFlag;
    }

    public void setOperationId(int id)
    {
        this.operationId = id;
    }

    public void setPostgreId(int postgreId) {
        this.postgreId = postgreId;
    }

    public void setImported(Date imported) {
        this.imported = imported;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setReaderUuid(String readerUuid) {
        this.readerUuid = readerUuid;
    }

    public void setTagEmbeddedTaskIndex(int tagEmbeddedTaskIndex) {
        this.tagEmbeddedTaskIndex = tagEmbeddedTaskIndex;
    }

    public void setAnnotationTemplateId(int annotationTemplateId) {
        this.annotationTemplateId = annotationTemplateId;
    }

    public void setOpLat(Double lat) {
        this.opLat = lat;
    }

    public void setOpLon(Double opLon) {
        this.opLon = opLon;
    }

    public void setOpAccuracy(float opAccuracy) {
        this.opAccuracy = opAccuracy;
    }
    public void setSynchronizedFlag(boolean synchronizedFlag) {
        this.synchronizedFlag = synchronizedFlag;
    }
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public int getPhotoPostGreId() {
        return photoPostGreId;
    }

    public void setPhotoPostGreId(int photoPostGreId) {
        this.photoPostGreId = photoPostGreId;
    }


    @Override
    public String toString() {
        return "Operation{" +
                "id=" + this.getOperationId() +
                ", postgreId=" + postgreId +
                ", imported=" + imported +
                ", created=" + created.getTime() +
                ", readerUuid='" + readerUuid + '\'' +
                ", tagEmbeddedTaskIndex=" + tagEmbeddedTaskIndex +
                ", annotationTemplateId=" + annotationTemplateId +
                ", sensorObjectId=" + sensorObjectId +
                ", lat=" + opLat +
                ", lon=" + opLon +
                ", accuracy=" + opAccuracy +
                ", synchronizedFlag=" + synchronizedFlag +
                ", photo=" + photo +
                ", photoPostGreId=" + photoPostGreId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return tagEmbeddedTaskIndex == operation.tagEmbeddedTaskIndex &&
                created.equals(operation.created) &&
                annotationTemplateId == operation.annotationTemplateId &&
                sensorObjectId == operation.sensorObjectId ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(created, tagEmbeddedTaskIndex,annotationTemplateId,sensorObjectId, opLat, opLon, opAccuracy);
    }

    public int getSensorObjectId() {
        return sensorObjectId;
    }

    public void setSensorObjectId(int sensorObjectId) {
        this.sensorObjectId = sensorObjectId;
    }
}
