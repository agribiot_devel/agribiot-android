/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class AnnotationWithOperations {
    @Embedded
    public AnnotationTemplate annotationTemplate;
    @Relation(
            parentColumn = "at_id",
            entityColumn = "op_annotation_template_id"
    )
    public List<Operation> operations;
/*
    public JSONObject OperationtoJson() throws JSONException {
        JSONObject operationJson = new JSONObject();
        operationJson.put("tid",this.operation.getTid());
        operationJson.put("tag_embedded_task_index",this.operation.getTagEmbeddedTaskIndex());
        // Todo timezone ?
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String strDate = dateFormat.format(this.operation.getCreated());
        operationJson.put("created",strDate);
        operationJson.put("value",this.operation.getValue());
        operationJson.put("annotation_template_id",this.annotationTemplate.getPostgreId());
        operationJson.put("lat",this.operation.getLat());
        operationJson.put("lon",this.operation.getLon());
        operationJson.put("accuracy",this.operation.getAccuracy());

        return operationJson;
    }*/
}
