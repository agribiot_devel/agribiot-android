/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.Date;
import java.util.List;

@Dao
public interface OperationDao {

    @Query("SELECT * FROM operation")
    public abstract LiveData<List<Operation>> getAll();

    @Query("SELECT * FROM operation")
    public abstract List<Operation> getAllAsList();
    @Query("SELECT * FROM operation WHERE op_id LIKE :id")
    public abstract Operation get(int id);

    @Query("SELECT * FROM operation WHERE op_id LIKE :id")
    public abstract LiveData<Operation> getLive(int id);


    @Query("SELECT * FROM operation WHERE op_synchronizedFlag LIKE :synchronizedFlag")
    public abstract List<Operation> loadAllBySynchronizedFlag(int synchronizedFlag);


    @Query("SELECT * FROM operation WHERE op_annotation_template_id LIKE :annotationTemplateId")
    public abstract LiveData<List<Operation>> findByTask(int annotationTemplateId);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insert(Operation operation);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<Operation> operation);

    @Update
    public abstract int updateOperation(Operation operation);

    @Query("DELETE FROM operation WHERE op_id = :operationId")
    public abstract void delete(int operationId);

    @Delete
    public abstract void delete(Operation operation);

    @Query("DELETE FROM operation")
    public abstract void deleteAll();


}
