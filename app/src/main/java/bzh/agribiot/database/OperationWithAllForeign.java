package bzh.agribiot.database;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class OperationWithAllForeign {
    @Embedded
    Operation operation;
    @Relation(parentColumn =  "op_annotation_template_id", entityColumn = "at_id")
    AnnotationTemplate annotationTemplate;
    @Relation(parentColumn =  "op_sensor_object_id", entityColumn = "so_id")
    SensorObject sensorObject;

    @ColumnInfo(name = "nbr_dimension_value", defaultValue = "0")
    int nbrDimensionValues;
    //@Relation(parentColumn =  "operation_id", entityColumn = "operation_id")
   // List<OperationImage> operationImages;


    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public AnnotationTemplate getAnnotationTemplate() {
        return annotationTemplate;
    }

    public void setAnnotationTemplate(AnnotationTemplate annotationTemplate) {
        this.annotationTemplate = annotationTemplate;
    }

    public int getNbrDimensionValues() {
        return nbrDimensionValues;
    }

    public void setNbrDimensionValues(int nbrDimensionValues) {
        this.nbrDimensionValues = nbrDimensionValues;
    }

    public SensorObject getSensorObject() {
        return sensorObject;
    }

    public void setSensorObject(SensorObject sensorObject) {
        this.sensorObject = sensorObject;
    }
}
