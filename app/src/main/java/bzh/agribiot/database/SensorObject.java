/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import bzh.agribiot.logger.LoggerUtil;

@Entity
public class SensorObject {


    @ColumnInfo(name = "so_id")
    @PrimaryKey(autoGenerate = true)
    private int sensorObjectId;

    @ColumnInfo(name = "so_postgre_id")
    private int postgreId;


    @ColumnInfo(name = "so_object_id")
    private int objectId;

    @ColumnInfo(name = "so_lat")
    private Double lat = 0.0;

    @ColumnInfo(name = "so_lon")
    private Double lon = 0.0;

    @ColumnInfo(name = "so_description")
    private String description;

    @ColumnInfo(name = "so_uuid")
    private String uuid;

    @ColumnInfo(name = "so_sensor_id")
    private int sensorId;

    @ColumnInfo(name = "so_type")
    private String type;

    @ColumnInfo(name = "so_unit")
    private String unit;

    @ColumnInfo(name = "so_synchronizedFlag")
    private boolean synchronizedFlag = false;

    public int getSensorObjectId() {
        return sensorObjectId;
    }

    public void setSensorObjectId(int sensorObjectId) {
        this.sensorObjectId = sensorObjectId;
    }

    public static List<SensorObject> ParseJsonArray(String sensorObjectsList) throws JSONException, UnsupportedEncodingException {
        JSONArray sensorObjectsJson = new JSONArray(sensorObjectsList);
        ArrayList<SensorObject> sensorObjectList = new ArrayList<SensorObject>();

        for (int i = 0; i < sensorObjectsJson.length(); i++) {
            sensorObjectList.add(ParseJson(sensorObjectsJson.getJSONObject(i)));
        }
        return sensorObjectList;
    }
    public static SensorObject ParseJson(JSONObject sensorObjectJson)throws JSONException, UnsupportedEncodingException{
        SensorObject sensorObject = new SensorObject();
        sensorObject.setPostgreId(sensorObjectJson.getInt("id"));
        sensorObject.setUuid(new String(sensorObjectJson.getJSONObject("object_id").getString("uuid").getBytes("ISO-8859-1"), "UTF-8"));
        sensorObject.setDescription(new String(sensorObjectJson.getJSONObject("object_id").getString("description").getBytes("ISO-8859-1"), "UTF-8"));
        String point = new String(sensorObjectJson.getJSONObject("object_id").getString("position").getBytes("ISO-8859-1"), "UTF-8");
        point = point.substring(point.indexOf('(')+1);
        point = point.replace(")","");
        sensorObject.setLat(Double.valueOf(point.split(" ")[1]));
        sensorObject.setLon(Double.valueOf(point.split(" ")[0]));
        sensorObject.setType(new String(sensorObjectJson.getJSONObject("sensor_id").getString("name").getBytes("ISO-8859-1"), "UTF-8"));
        sensorObject.setUnit(new String(sensorObjectJson.getJSONObject("sensor_id").getString("unit").getBytes("ISO-8859-1"), "UTF-8"));
        sensorObject.setSynchronizedFlag(true);

        return sensorObject;
    }
    public JSONObject toJson() throws JSONException {
        JSONObject sensorObjectJson = new JSONObject();
        JSONObject sensorJson = new JSONObject();
        JSONObject objectJson = new JSONObject();
        sensorJson.put("id",this.getSensorId());
        sensorJson.put("type",this.getType());
        sensorJson.put("unit",this.getUnit());

        objectJson.put("lat",this.getLat());
        objectJson.put("lon",this.getLon());
        objectJson.put("uuid",this.getUuid());
        objectJson.put("description",this.getDescription());

        sensorObjectJson.put("sensor_id",sensorJson);
        sensorObjectJson.put("object_id",objectJson);
        return sensorObjectJson;
    }

    public int getPostgreId() {
        return postgreId;
    }

    public void setPostgreId(int postgreId) {
        this.postgreId = postgreId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public boolean isSynchronizedFlag() {
        return synchronizedFlag;
    }

    public void setSynchronizedFlag(boolean synchronizedFlag) {
        this.synchronizedFlag = synchronizedFlag;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    @Override
    public String toString() {
        return "SensorObject{" +
                "sensorObjectLocalId=" + sensorObjectId +
                ", postgreId=" + postgreId +
                ", lat=" + lat +
                ", lon=" + lon +
                ", description='" + description + '\'' +
                ", uuid='" + uuid + '\'' +
                ", type='" + type + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}
