/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot;

import android.Manifest;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.android.material.navigation.NavigationView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import bzh.agribiot.datasync.SyncAdapter;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.AgribiotFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private AppBarConfiguration mAppBarConfiguration;
    static final String LOG_TAG = "MainActivity";
    NavHostFragment navHostFragment = null;
    Button syncButton ;
    AgribiotApplication agribiotApplication ;

    AlertDialog.Builder builder;
    AlertDialog progressDialog;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        boolean rfidManagement = getResources().getBoolean(R.bool.rfidmanagement);
        agribiotApplication = (AgribiotApplication) this.getApplication();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        syncButton = (Button)findViewById(R.id.sync_button);

        progressDialog = getDialogProgressBar().create();

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        LoggerUtil.debug(LOG_TAG,"Rfid management : " + Boolean.toString(rfidManagement));
        if(rfidManagement){
            mAppBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.nav_task, R.id.nav_observation, R.id.nav_object,R.id.nav_settings,R.id.nav_rfid_management)
                    .setDrawerLayout(drawer)
                    .build();
        }else{
            mAppBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.nav_task, R.id.nav_observation, R.id.nav_object,R.id.nav_settings)
                    .setDrawerLayout(drawer)
                    .build();
        }


        navHostFragment = (NavHostFragment)this.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        NavController navController = navHostFragment.getNavController();
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        runtime_permissions();
        IntentFilter iFilter = new IntentFilter(AgribiotApplication.SYNC_FINISHED);
        iFilter.addAction(AgribiotApplication.SYNC_ERROR);
        iFilter.addAction(AgribiotApplication.SYNC_PROGRESS);

        registerReceiver(syncFinishedReceiver, iFilter);
        syncButton.setOnClickListener(this);
    }

    /**
     * https://stackoverflow.com/questions/61477929/problem-in-scanning-bluetooth-le-devices-no-callback
     * @return
     */
    private boolean runtime_permissions()
    {
        /*
        LoggerUtil.debug(LOG_TAG,"Permission : ");
        LoggerUtil.debug(LOG_TAG, "1  " + ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION));
        LoggerUtil.debug(LOG_TAG, "2  " + ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION));
        LoggerUtil.debug(LOG_TAG, "3  " + ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION));
        LoggerUtil.debug(LOG_TAG, "4  " + ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH));
        */
        if (Build.VERSION.SDK_INT >= 23 && (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED))
        {
            new AlertDialog.Builder(this)
                    .setTitle("Access à la position de l'appareil")
                    .setMessage("Cette application néccésite un accès constant à la position \"Toujours autoriser\" ")

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.ACCESS_BACKGROUND_LOCATION}, 100);
                        }
                    })

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();



            return true;
        }

        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LoggerUtil.debug(LOG_TAG, "onActivityResult() " + requestCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AgribiotFragment.REQUEST_IMAGE_CAPTURE) {

            for (Fragment fragment :    navHostFragment.getChildFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
                LoggerUtil.debug(LOG_TAG, "onActivityResult()" + fragment.getClass());
            }
        }
    }

    private BroadcastReceiver syncFinishedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra("bundle")){
                Bundle bdl = intent.getBundleExtra("bundle");
                int duration = Toast.LENGTH_LONG;

                CharSequence text = bdl.getString("message");
                LoggerUtil.debug(LOG_TAG,bdl.getString("message"));
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                //layoutSyncPb.setVisibility(View.GONE);
                progressDialog.dismiss();

            }else if(intent.hasExtra("step")){
                int step = intent.getIntExtra("step",0);
                LoggerUtil.debug(LOG_TAG,"step : " + intent.getIntExtra("step",0));
                if(step == 0){
                    //progressBar.setMax(SyncAdapter.SynchroStep.COMPLETE.ordinal());
                    progressDialog.show();


                }
                //progressBar.setProgress(step);
                if(step == SyncAdapter.SynchroStep.COMPLETE.ordinal()){
                    progressDialog.dismiss();
                }

            }


        }
    };
    public AlertDialog.Builder getDialogProgressBar() {

        if (builder == null) {
            builder = new AlertDialog.Builder(this);

            builder.setTitle("Synchronisation...");

            progressBar = new ProgressBar(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            progressBar.setLayoutParams(lp);
            builder.setView(progressBar);
            builder.setCancelable(false);
        }
        return builder;
    }

    @Override
    public void onClick(View v) {
        // Pass the settings flags by inserting them in a bundle
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        /*
         * Request the sync for the default account, authority, and
         * manual sync settings
         */
        LoggerUtil.debug(LOG_TAG,"Request sync for provider " + v.getContext().getString(R.string.content_provider));
        ContentResolver.requestSync(agribiotApplication.getAccount(), v.getContext().getString(R.string.content_provider), settingsBundle);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


}