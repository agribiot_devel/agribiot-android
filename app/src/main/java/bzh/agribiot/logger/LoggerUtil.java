/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.logger;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Locale;

public class LoggerUtil {

	public static final String APP_ID = "AgribiotRFIDApp";
    private static String logDir = "/Android";
    private static String logFileName = "/agribiot-Debug.txt";
    private static boolean writeLogsToFile = false;
    private static final int LOG_LEVEL_VERBOSE = 4;
    private static final int LOG_LEVEL_DEBUG = 3;
    private static final int LOG_LEVEL_INFO = 2;
    private static final int LOG_LEVEL_ERROR = 1;
    private static final int LOG_LEVEL_OFF = 0;
    private static final int CURRENT_LOG_LEVEL = LOG_LEVEL_DEBUG;

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String BytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
    public static String ByteToHex(byte myByte) {
        char[] hexChars = new char[2] ;
        int v = myByte & 0xFF;
        hexChars[0] = HEX_ARRAY[v >>> 4];
        hexChars[1] = HEX_ARRAY[v & 0x0F];
        return new String(hexChars);
    }
    public static void log(String tag, String message, int logLevel) {
        if (logLevel <= CURRENT_LOG_LEVEL) 
        	if(logLevel != LOG_LEVEL_ERROR){
        		 Log.d(APP_ID, tag + " "  + message);
        	}
            if (writeLogsToFile) {
                writeToFile(message);
            }
    }

    public static void writeToFile(String message) {
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            File dir = new File(sdCard.getAbsolutePath() + logDir);
            dir.mkdirs();
            File file = new File(dir, logFileName);
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file, true), 8 * 1024));
            writer.println(APP_ID + " " + new Date().toString() + " : " + message);
            writer.flush();
            writer.close();
        } catch (Exception e) {
        	Log.e(APP_ID, "Exception in logging  :", e);
        }
    }

    public static void verbose(String tag, String message) {
        log(tag, message, LOG_LEVEL_VERBOSE);
    }

    public static void debug(String tag, String message) {
        log(tag, message, LOG_LEVEL_DEBUG);
    }
    public static void debug(String tag, String format, Object... args) {
        log(tag, String.format(Locale.US, format, args), LOG_LEVEL_DEBUG);
    }

    public static void error(String tag, String message, Exception ex) {
    	Log.e(tag, message, ex);
        log(tag,message, LOG_LEVEL_ERROR);
    }
    
    public static void error(String tag, String message) {
        log(tag,message, LOG_LEVEL_ERROR);
        Log.e(tag, message);
    }

    public static void info(String tag, String message) {
        log(tag, message, LOG_LEVEL_INFO);
    }
}