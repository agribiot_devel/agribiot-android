/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.volley;

import android.graphics.Bitmap;

import com.android.volley.VolleyError;

import java.io.File;
import java.util.ArrayList;

import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationImage;
import bzh.agribiot.database.SensorObject;

public interface SynchronizationCallback {
    public void onAnnotationTemplateInserted(AnnotationTemplate annotationTemplateToInsert, AnnotationTemplate insertedAnnotationTemplate);
    public void onAnnotationTemplateFetch(ArrayList<AnnotationTemplate> annotationTemplates);
    public void onOperationFetch(ArrayList<Operation> operations);
    public void onDimensionsFetch(ArrayList<Dimension> dimensions);
    public void onDimensionUploaded(Dimension dimensionToInsert, Dimension insertedDimension);
    public void onDimensionsValueFetch(ArrayList<DimensionValue> dimensionsValues);
    public void onDimensionValueUploaded(DimensionValue dimensionValueToInsert, DimensionValue insertedDimensionValue);
    public void onSensorObjectInserted(SensorObject sensorObjectToInsert, SensorObject insertedSensorObject);
    public void onSensorObjectFetch(ArrayList<SensorObject> sensorObjects);
    public void onOperationImageUploaded(ArrayList<OperationImage> imageToInsert, OperationImage insertedImage);
    public void onOperationInserted(Operation operationToInsert, Operation insertedOperation);
    public void onAnnotationTemplatePhotoFetch(AnnotationTemplate annotationTemplate, Bitmap response);
    public void onError(Exception e);
    public void onVolleyError(VolleyError e);

}
