/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.volley;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.AnnotationWithOperations;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationImage;
import bzh.agribiot.database.SensorObject;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.AgribiotFragment;
import bzh.agribiot.ui.settings.ServerFragment;
import bzh.agribiot.ui.settings.SettingsFragment;

public class VolleyHelper {
    static String LOG_TAG = "VolleyHelper";
    static String defaultApiUrl = "http://agribiot-box.simon-tropee.com:8091";
    static String defaultApiToken = "4552a488c6be7bf280b894baeec8f5d56884b644";

    public static void PostOperationImage(Context context, ArrayList<OperationImage> imagesToSync, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        Map<String, String> params = new HashMap<String, String>();
        //params.put("Content-Type", "application/json");
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/operation_image/";

        params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
        if(imagesToSync.size() == 0){
            callback.onOperationImageUploaded(imagesToSync,null);
        }else{

            for(OperationImage imageToSync :imagesToSync){
                LoggerUtil.debug(LOG_TAG," send : " + imageToSync.getPhoto() );
                if(imageToSync.getPhoto() == null){
                    callback.onOperationImageUploaded(imagesToSync,null);
                    continue;
                }
                VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                        new Response.Listener<NetworkResponse>() {
                            @Override
                            public void onResponse(NetworkResponse response) {
                                try {
                                    JSONObject obj = new JSONObject(new String(response.data));
                                    imageToSync.setPostgreId(obj.getInt("id"));
                                    callback.onOperationImageUploaded(imagesToSync,imageToSync);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                LoggerUtil.debug(LOG_TAG,"PostOperationImage error : ");
                                LoggerUtil.debug(LOG_TAG,error.getMessage());
                                LoggerUtil.debug(LOG_TAG,error.toString());
                                error.getStackTrace();

                            }
                        },params) {


                    @Override
                    protected Map<String, DataPart> getByteData() {
                        Map<String, DataPart> params = new HashMap<>();
                        Bitmap myBitmap = BitmapFactory.decodeFile(imageToSync.getPhoto());
                        LoggerUtil.debug(LOG_TAG," send : " + imageToSync.getPhoto() + " " + myBitmap.getByteCount());
                        params.put("photo", new DataPart(imageToSync.getPhoto().substring(imageToSync.getPhoto().lastIndexOf('/')+1), getFileDataFromDrawable(myBitmap)));
                        return params;
                    }
                };

                //adding the request to volley
                Volley.newRequestQueue(context).add(volleyMultipartRequest);
            }
        }

    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    public static void PostOperation(Context context, ArrayList<Operation> operations, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/operation/";
        // Request a string response from the provided URL.
        for(Operation curOpToSync:operations){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject operationSavedJson = new JSONObject(response);
                                callback.onOperationInserted(curOpToSync,new Operation(operationSavedJson));
                            } catch (JSONException | ParseException e) {
                                callback.onError(e);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onVolleyError(error);
                }


            }){
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                            JSONObject operationJson = curOpToSync.toJson();
                            LoggerUtil.debug(LOG_TAG, " operation to send : " + curOpToSync.toString());
                            return operationJson.toString().getBytes();

                    } catch (JSONException e) {
                            callback.onError(e);
                    }
                    return null;

                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                    return params;
                }
            };
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
            queue.start();
        }

    }
    public static void PullOperations(Context context, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/operation/";

        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<Operation> operations = (ArrayList<Operation>)Operation.ParseJsonArray(response);
                            callback.onOperationFetch(operations);

                        } catch (JSONException | ParseException e) {
                            callback.onError(e);
                        }
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoggerUtil.debug(LOG_TAG,error.getMessage());
                        callback.onVolleyError(error);
                    }
                }){
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        jsonObjectRequest.setShouldCache(false);
        queue.add(jsonObjectRequest);

    }
    public static void PullDimensions(Context context, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/dimension/";

        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<Dimension> dimensions = (ArrayList<Dimension>)Dimension.ParseJsonArray(response);
                            callback.onDimensionsFetch(dimensions);
                        } catch (JSONException | ParseException | UnsupportedEncodingException e) {
                            callback.onError(e);
                        }
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoggerUtil.debug(LOG_TAG,error.getMessage());
                        callback.onVolleyError(error);
                    }
                }){
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        jsonObjectRequest.setShouldCache(false);
        queue.add(jsonObjectRequest);
    }
    public static void PushDimension(Context context, Dimension dimension, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/dimension/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject dimensionSavedJson = new JSONObject(response);
                            callback.onDimensionUploaded(dimension,new Dimension(dimensionSavedJson));
                        } catch (JSONException | UnsupportedEncodingException e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error.networkResponse == null){
                    callback.onVolleyError(error);
                    return;
                }
                NetworkResponse networkResponse = error.networkResponse;
                if(networkResponse.data == null){
                    callback.onVolleyError(error);
                    return;
                }
                try {
                    JSONObject myJsonError = new JSONObject(new String(networkResponse.data));
                    JSONArray errorDesc = myJsonError.optJSONArray("name");
                    if(errorDesc == null){
                        callback.onVolleyError(error);
                        return;
                    }
                    LoggerUtil.debug(LOG_TAG,"errorDesc : " + (String)errorDesc.get(0));
                    if(((String)errorDesc.get(0)).equals("dimension with this name already exists.")){
                        callback.onDimensionUploaded(dimension,null);
                    }
                } catch (JSONException e) {
                    callback.onError(e);
                    return;
                }
            }


        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    JSONObject dimensionJson = dimension.toJson();
                    return dimensionJson.toString().getBytes();
                } catch (JSONException e) {
                    callback.onError(e);
                }
                return null;

            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        queue.start();

    }

    public static void PullDimensionValues(Context context, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/dimension_value/";

        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<DimensionValue> dimensionValues = (ArrayList<DimensionValue>)DimensionValue.ParseJsonArray(response);
                            callback.onDimensionsValueFetch(dimensionValues);

                        } catch (JSONException | ParseException e) {
                            callback.onError(e);
                        }
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoggerUtil.debug(LOG_TAG,error.getMessage());
                        callback.onVolleyError(error);
                    }
                }){
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        jsonObjectRequest.setShouldCache(false);
        queue.add(jsonObjectRequest);

    }

    public static void PushDimensionValues(Context context, ArrayList<DimensionValue> dimensionValues, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/dimension_value/";
        // Request a string response from the provided URL.
        for(DimensionValue dimValueToSync:dimensionValues){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject dimensionValueSavedJson = new JSONObject(response);
                                callback.onDimensionValueUploaded(dimValueToSync,new DimensionValue(dimensionValueSavedJson));
                            } catch (JSONException e) {
                                callback.onError(e);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onVolleyError(error);
                }


            }){
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        JSONObject dimensionJson = dimValueToSync.toJson();
                        return dimensionJson.toString().getBytes();

                    } catch (JSONException e) {
                        callback.onError(e);
                    }
                    return null;

                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                    return params;
                }
            };
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
            queue.start();
        }
    }
    public static void PostAnnotationTemplate(Context context, AnnotationTemplate atToSync, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/annotation_template/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject annotationSaved = new JSONObject(response);
                            AnnotationTemplate atSynced = AnnotationTemplate.ParseJson(annotationSaved);
                            callback.onAnnotationTemplateInserted(atToSync,atSynced);
                        } catch (JSONException | UnsupportedEncodingException e) {
                            callback.onError(e);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error.networkResponse == null){
                    callback.onVolleyError(error);
                    return;
                }
                NetworkResponse networkResponse = error.networkResponse;
                if(networkResponse.data == null){
                    callback.onVolleyError(error);
                    return;
                }
                try {
                    JSONObject myJsonError = new JSONObject(new String(networkResponse.data));
                    JSONArray errorDesc = myJsonError.optJSONArray("description");
                    if(errorDesc == null){
                        callback.onVolleyError(error);
                        return;
                    }
                    LoggerUtil.debug(LOG_TAG,"errorDesc : " + (String)errorDesc.get(0));
                    if(((String)errorDesc.get(0)).equals("annotation template with this description already exists.")){
                        callback.onAnnotationTemplateInserted(atToSync,null);
                    }
                } catch (JSONException e) {
                    callback.onError(e);
                    return;
                }
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    //LoggerUtil.debug(LOG_TAG,atToSync.toJson().toString());
                    return atToSync.toJson().toString().getBytes();
                } catch (JSONException e) {
                    callback.onError(e);
                }
                return null;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        queue.start();
    }


    public static void GetAnnotationTemplate(Context context, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/annotation_template/";

        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<AnnotationTemplate> annotTemplates = (ArrayList<AnnotationTemplate>)AnnotationTemplate.ParseJsonArray(response);
                            callback.onAnnotationTemplateFetch(annotTemplates);

                        } catch (JSONException | UnsupportedEncodingException e) {
                            callback.onError(e);
                        }
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoggerUtil.debug(LOG_TAG,error.getMessage());
                        callback.onVolleyError(error);
                    }
                }){
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        jsonObjectRequest.setShouldCache(false);
        queue.add(jsonObjectRequest);

    }
    public static void GetAnnotationTemplatePhoto(Context context, AnnotationTemplate annotationTemplate,SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/media/annotationtemplateimg/"+annotationTemplate.getPhoto();


        ImageRequest ir = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                // callback
                callback.onAnnotationTemplatePhotoFetch(annotationTemplate,response);
            }
        }, 200, 200, null, null);
        // 100 is your custom Size before Downloading the Image.
        queue.add(ir);
    }

    public static void TestConnection(ServerFragment serverFragment, String testUrl, String testToken, SharedPreferences.Editor editor){
        RequestQueue queue = Volley.newRequestQueue(serverFragment.getContext());
        String url = testUrl +"/api/annotation_template/";
        LoggerUtil.debug(LOG_TAG,"Test connection with " + url + "Authorization : Token " + testToken);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        LoggerUtil.debug(LOG_TAG,"reponse test : " + response);
                        editor.putString("apiUrl", testUrl);
                        editor.putString("token", testToken);
                        editor.apply();
                        serverFragment.onTestOk();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(error.networkResponse != null) {
                    LoggerUtil.debug(LOG_TAG,"reponse error : " + error.networkResponse.statusCode+ " " + error.getMessage());

                    if(error.networkResponse.statusCode == 403){
                        try {
                            JSONObject response = new JSONObject(new String(error.networkResponse.data));

                            if(response.getString("detail").equals(new String("Authentication credentials were not provided."))){
                                LoggerUtil.debug(LOG_TAG,"Connection test NOK credentials");


                            }else{
                                serverFragment.showErrorString(response.getString("detail"));
                            }
                        } catch (JSONException e) {
                            serverFragment.showError(e);
                        }
                    }else{
                        serverFragment.showErrorString("Error http  : " + error.networkResponse.statusCode);
                    }

                }else if(error.getClass() == NoConnectionError.class){
                    serverFragment.showErrorString("Server unreachable");
                }else{
                    LoggerUtil.debug(LOG_TAG,"Unknown HTTP error : "+error.getClass());
                    LoggerUtil.debug(LOG_TAG,"Unknown HTTP error : "+error.getCause());
                }
            }


        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +testToken);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        queue.start();
    }
    public static void GetSensorObject(Context context, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/sensor_object_tag/";

        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            //LoggerUtil.debug(LOG_TAG,response);
                            ArrayList<SensorObject> sensorObjects = (ArrayList<SensorObject>)SensorObject.ParseJsonArray(response);
                            callback.onSensorObjectFetch(sensorObjects);

                        } catch (JSONException | UnsupportedEncodingException e) {
                            LoggerUtil.debug(LOG_TAG,e.getMessage());
                            callback.onError(e);
                        }
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoggerUtil.debug(LOG_TAG,error.getMessage());
                        callback.onVolleyError(error);
                    }
                }){
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        jsonObjectRequest.setShouldCache(false);
        queue.add(jsonObjectRequest);

    }

    public static void PostSensorObject(Context context, SensorObject soToSync, SynchronizationCallback callback){
        SharedPreferences sharedPref = context.getSharedPreferences("Agribiot", context.MODE_PRIVATE);
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = sharedPref.getString("apiUrl",defaultApiUrl) +"/api/sensor_object_tag/";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            LoggerUtil.debug(LOG_TAG,"response : "+ response);
                            JSONObject soSaved = new JSONObject(response);
                            SensorObject soSynced = SensorObject.ParseJson(soSaved);
                            callback.onSensorObjectInserted(soToSync,soSynced);
                        } catch (JSONException | UnsupportedEncodingException e) {
                            callback.onError(e);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //LoggerUtil.debug(LOG_TAG,"error : " + error.toString());
                if(error.networkResponse == null){
                    callback.onVolleyError(error);
                    return;
                }
                NetworkResponse networkResponse = error.networkResponse;
                if(networkResponse.data == null){
                    callback.onVolleyError(error);
                    return;
                }
                try {
                    JSONObject myJsonError = new JSONObject(new String(networkResponse.data));
                    JSONArray errorDesc = myJsonError.optJSONArray("description");
                    if(errorDesc == null){
                        callback.onVolleyError(error);
                        return;
                    }
                    LoggerUtil.debug(LOG_TAG,"errorDesc : " + (String)errorDesc.get(0));
                    if(((String)errorDesc.get(0)).equals("sensor object with this description already exists.")){
                        callback.onSensorObjectInserted(soToSync,null);
                    }
                } catch (JSONException e) {
                    callback.onError(e);
                    return;
                }
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    //LoggerUtil.debug(LOG_TAG,soToSync.toJson().toString());
                    return soToSync.toJson().toString().getBytes();
                } catch (JSONException e) {
                    callback.onError(e);
                }
                return null;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Token " +sharedPref.getString("token",defaultApiToken));
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        queue.start();
    }
}
