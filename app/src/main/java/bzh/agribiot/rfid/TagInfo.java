/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.rfid;

import com.rscja.deviceapi.entity.UHFTAGInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import bzh.agribiot.logger.LoggerUtil;
/*
 *
 RFID User Memory mapping :

protocol "magic_number:24,version:8,used_bytes:8,reserved:24,cur_index:16"
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                  magic_number                 |    version    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   used_bytes  |                    reserved                   |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|           cur_index           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


Where :
- magic_number is the hex value of 210492 : 0x03363C, a well know value to assert this RFID user memory has already
been initialized
- version is the current version of memory management used in memory
- used bytes : the number of bytes currently used in memory (including header) (current version should be 10 bytes)
- reserved : 24 reserved bits because to align cur_index on memory (write is done with word-length offset)
- cur_index : Number of task that has been write to the tag (the first one is for init)

EPC Memory : Unchanged
TID Memory : Unchanged

Current processing when trying to write a TAG :
1) Read EPC, TID and whole USER Memory of the first TAG to respond (Inventory mode is not used)
2) Validate User memory
3) Increment cur_index if User Memory comply with our Memory mapping
*
Task with index 0 is always init
 *
 */
public class TagInfo implements Serializable {
    String mEpcString = null;
    byte[] mEpc = null;
    byte[] mTid = null;
    byte[] mData = null;
    float mRssi = 0.0f;
    float mPhase = 0.0f;
    static String LOG_TAG = "TagInfo";
    byte XTIDBit;
    byte securityIndicatorBit;
    byte fileBit;
    short MDID;
    short brand;
    String manufacturer = null;
    String modelName =null;
    int userMemorySize;
    int epcMemorysize;
    public static byte MEMORY_MAPPING_VERSION = 0x1;
    public static byte MEMORY_USED_BYTE= 0xa;
    public static byte MAGIC_NUMBER[] =  {0x03,0x36,0x3C};


    public TagInfo(UHFTAGInfo uhfTagInfo){
        //LoggerUtil.debug(LOG_TAG,"Epc : " + uhfTagInfo.getEPC());

        mEpcString = uhfTagInfo.getEPC();
        mEpc = hexStringToByteArray(mEpcString);

        if(uhfTagInfo.getRssi().contains(",")){
            uhfTagInfo.setRssi(uhfTagInfo.getRssi().replace(",","."));
        }

        if(uhfTagInfo.getRssi().equals("N/A")){
            mRssi = 0.0f;
        }else{
            mRssi = Float.parseFloat(uhfTagInfo.getRssi());
        }
        if(uhfTagInfo.getTid() != null) {
            //LoggerUtil.debug(LOG_TAG,"TID : " + uhfTagInfo.getTid());
            mTid = hexStringToByteArray(uhfTagInfo.getTid().substring(0, 24));
        }
        if(uhfTagInfo.getUser() != null){
            //LoggerUtil.debug(LOG_TAG,"User : " + uhfTagInfo.getUser());
            mData = hexStringToByteArray(uhfTagInfo.getUser().substring(0,MEMORY_USED_BYTE*2));
        }
    }

    public TagInfo(String epcString, float rssi, float phase){
        mEpcString = epcString;
        mEpc = hexStringToByteArray(epcString);
        mRssi = rssi;
        mPhase = phase;
    }

    public String epcString() {
        return mEpcString;
    }

    public int getRssi() {
        return Math.round(mRssi);
    }

    public int getFrequency() {
        return 0;
    }

    public int getReadCount() {
        return 0;
    }

    public String getBrandIdentifier() {
            return manufacturer;
    }
    public String getModel() {
            return modelName;
    }
    public long getTime() {
        return 0;

    }

    public byte[] getData() {
        return mData;

    }

    public byte[] getEPCMemData() {
        return mEpc;
    }

    public byte[] getReservedMemData() {
        return null;
    }

    public void setUserMemData(String userMemData){
            this.mData = hexStringToByteArray(userMemData);
    }
    public byte[] getUserMemData() {
        return mData;
    }
    public void setTIDMemData(String TID) {
        this.mTid = hexStringToByteArray(TID);
    }
    public byte[] getTIDMemData() {
        return mTid;
    }

    public String getTidString() {
        return BytesToHex(mTid);
    }

    public int getPhase() {
        return Math.round(mPhase);
    }

    /*

        public String toString() {
            if(tagReadData != null){
                return this.tagReadData.toString();
            }else{
                return null;
            }
        }
        */
    private final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    private String BytesToHex(byte[] bytes) {
        if(bytes == null){
            return new String("");
        }
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
    @Override
    public String toString() {
        return "TagInfo{" +
                ", mEpcString='" + mEpcString + '\'' +
                ", mEpc=" + BytesToHex(mEpc) +
                ", mTid=" + BytesToHex(mTid) +
                ", mData=" + BytesToHex(mData) +
                ", mRssi=" + mRssi +
                ", mPhase=" + mPhase +
                '}';
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public short validateUserMemory() {
        byte memoryHeader[] = getUserMemData();
        if(memoryHeader[0] != MAGIC_NUMBER[0] || memoryHeader[1] != MAGIC_NUMBER[1] || memoryHeader[2] != MAGIC_NUMBER[2] ) {
            return -1;
        }
        if(memoryHeader[3]!= MEMORY_MAPPING_VERSION || memoryHeader[4]!= MEMORY_USED_BYTE) {
            return -2;
        }

        return 0;
    }

    public short getCurIndex() {
        byte memory[] = getUserMemData();
        short rc = (short) (memory[8]<<8 | memory[9]);
        return rc;
    }
    public byte getPayloadVersion() {
        byte memory[] = getUserMemData();
        byte rc = memory[3];
        return rc;
    }
    public byte getPayloadLength() {
        byte memory[] = getUserMemData();
        byte rc = memory[4];
        return rc;
    }
    public int getEpcMemorysize(){
        return epcMemorysize;

    }
    public int getUserMemorySize(){
        return userMemorySize;
    }


    public int validateTid(String jsonDictionary) throws UnknownRFIDTagException{
        byte[] myTid = getTIDMemData();
        if((myTid[0] & 0xff) != 0xe2){
            LoggerUtil.debug(LOG_TAG,"Error with TID memory bank (bank 10) identifier != 0x02  : " + (myTid[0] & 0xff));
        }
        XTIDBit = (byte) (myTid[1]>>7 &0x01);
        securityIndicatorBit = (byte) (myTid[1]>>6 &0x01);
        fileBit = (byte) (myTid[1]>>5 &0x01);
        MDID = (short) ((short) (myTid[1] &0x1f)<<8);
        MDID = (short) (MDID | (myTid[2] & 0xf0));
        MDID = (short) (MDID >>4);
        brand = (short) ((myTid[2] << 8 | myTid[3]) & 0xfff);


        JSONObject json ;
        JSONArray registeredMaskDesignersArray ;
        JSONArray chipsArray = null;
        try {
            json = new JSONObject(jsonDictionary);
            registeredMaskDesignersArray = json.getJSONArray("registeredMaskDesigners");
            for(int i= 0;i<registeredMaskDesignersArray.length();i++){
                JSONObject brand = registeredMaskDesignersArray.getJSONObject(i);
                int curMdid = Integer.parseInt(brand.getString("mdid"), 2);
                if(curMdid == MDID){
                    chipsArray = brand.getJSONArray("chips");
                    manufacturer = brand.getString("manufacturer");
                    break;

                }
            }
            if(chipsArray == null){
                throw new UnknownRFIDTagException("Unknown MDID "+ Integer.toHexString(MDID));
            }

            for(int i = 0;i<chipsArray.length();i++){
                JSONObject chipObject = chipsArray.getJSONObject(i);
                int currentTmnHex = Integer.parseInt(chipObject.getString("tmnHex"),16);
                if(currentTmnHex == brand){
                    modelName = chipObject.getString("modelName");
                    epcMemorysize = chipObject.optInt("epcMemorysize",-1);
                    userMemorySize = chipObject.optInt("userMemorySize",-1);
                }
            }
            if(modelName == null){
                throw new UnknownRFIDTagException("Unknown modelName "+ Integer.toHexString(brand));
            }

        } catch (JSONException e) {
            throw new UnknownRFIDTagException("Json exception" + e.getMessage());
        }

        return 0;
    }
}
