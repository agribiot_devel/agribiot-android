/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.rfid;


import bzh.agribiot.database.Operation;

public interface RFIDEventListener {
    void onConnect();
    void onDisconnect();
    void onError(RfidException e);
    void onReadMemoryComplete(TagInfo tag);
    void onWriteComplete(Operation operation,TagInfo tag);

}
