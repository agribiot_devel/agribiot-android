/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.rfid;

import android.bluetooth.BluetoothDevice;
import android.content.Context;

import com.rscja.deviceapi.RFIDWithUHFBLE;
import com.rscja.deviceapi.RFIDWithUHFUART;
import com.rscja.deviceapi.entity.UHFTAGInfo;
import com.rscja.deviceapi.interfaces.ConnectionStatus;
import com.rscja.deviceapi.interfaces.ConnectionStatusCallback;
import com.rscja.deviceapi.interfaces.KeyEventCallback;

import java.util.List;
import bzh.agribiot.logger.LoggerUtil;

public class RFIDChafonHandler implements RFIDHandler, KeyEventCallback  {
    private static final RFIDChafonHandler INSTANCE = new RFIDChafonHandler();
    public RFIDWithUHFBLE uhf = null;
    static String LOG_TAG = "RFIDChafonHandler";
    RFIDEventListener mRfEventListener = null;
    TagInfoReadListener mTagInfoReadListener = null;

    BTStatus btStatus = new BTStatus();
    String mDeviceAddress;
    String mDeviceName;
    boolean connected = false;
    boolean loopFlag = false;

    AgribiotKeyEventCallback mKeyEventCallback;
    public enum CurrentAction {
        NONE,INVENTORY, READ_TAG, WRITE_TAG
    }
    public enum ChafonConnectionStatus {
        DISCONNECTED,CONNECTING,CONNECTED
    }
    CurrentAction mCurrentAction;
    ChafonConnectionStatus mChafonConnectionStatus;
    private RFIDChafonHandler() {
        uhf = RFIDWithUHFBLE.getInstance();
        mChafonConnectionStatus = ChafonConnectionStatus.DISCONNECTED;
    }
    public void open(RFIDEventListener eventListener, Context context) {
        LoggerUtil.debug(LOG_TAG,"Opening Chafon UHF");
        mRfEventListener = eventListener;
        uhf.init(context);
        mCurrentAction = CurrentAction.NONE;
    }
    public static RFIDChafonHandler getInstance() {
        return INSTANCE;
    }

    public String getRfidDevName(){
        return this.mDeviceName;
    }
    public void setRfidDevName(String devName){
        uhf.setRemoteBluetoothName(devName);
        this.mDeviceName = devName;
    }
    public void printVersion(){
        LoggerUtil.debug(LOG_TAG,"Connected to UHF reader");
        LoggerUtil.debug(LOG_TAG, "Using power : " + uhf.getPower());
        LoggerUtil.debug(LOG_TAG, "Version : " + uhf.getVersion());
        LoggerUtil.debug(LOG_TAG, "STM32 Version : " + uhf.getSTM32Version());
        LoggerUtil.debug(LOG_TAG, "Bluetooth Version : " + uhf.getBluetoothVersion());
        LoggerUtil.debug(LOG_TAG, "Temperature : " + uhf.getTemperature());
        LoggerUtil.debug(LOG_TAG, "Battery : " + uhf.getBattery());
    }
    public void setup(){
       //uhf.setBeep(true);
       uhf.setKeyEventCallback(this);
    }
    @Override
    public void setWritePower(int powerDbm) {
        LoggerUtil.debug(LOG_TAG,"Trying to set power to  : " + powerDbm);
        uhf.setPower(powerDbm);
        //uhf.setPower()
    }

    public int getPower(){
        return uhf.getPower();
    }

    @Override
    public void setReadPower(int powerDbm) {
        LoggerUtil.debug(LOG_TAG,"Trying to set read power to  : " + powerDbm);
        uhf.setPower(powerDbm);
    }

    @Override
    public void startInventory(long timeout, TagInfoReadListener tagInfoReadListener) {
        String dataStr = "00";
        uhf.setEPCMode();
        mTagInfoReadListener = tagInfoReadListener;
        mCurrentAction = CurrentAction.INVENTORY;
        loopFlag = true;
        if (!uhf.setFilter(RFIDWithUHFUART.Bank_EPC, 0, 0, dataStr)
                ||  !uhf.setFilter(RFIDWithUHFUART.Bank_TID, 0, 0, dataStr)
                ||  !uhf.setFilter(RFIDWithUHFUART.Bank_USER, 0, 0, dataStr))
        {
            mRfEventListener.onError(new RfidException("Filter disabling issue"));
            return;
        }
        (new Thread() {
            public void run() {
                uhf.startInventoryTag();
                while (loopFlag) {
                    List<UHFTAGInfo> list = getUHFInfo();
                    if (list != null && list.size() > 0) {
                        for(UHFTAGInfo currentUHFTAGInfo : list){
                            mTagInfoReadListener.TagReadCallback(new TagInfo(currentUHFTAGInfo));
                        }
                    }
                }
                uhf.stopInventory();
                mCurrentAction = CurrentAction.NONE;
            }
        }).start();
    }

    @Override
    public TagInfo readClosestTag() {
        if(!uhf.setEPCAndTIDUserMode(0, TagInfo.MEMORY_USED_BYTE)) {
            LoggerUtil.debug(LOG_TAG,"Failing setEPCAndTIDUserMode");
            return null;
        }
        UHFTAGInfo current =  uhf.inventorySingleTag();
        if(current != null){
            return new TagInfo(current);
        }else{
            return null;
        }
    }

    private synchronized   List<UHFTAGInfo> getUHFInfo() {
        List<UHFTAGInfo> list = uhf.readTagFromBufferList_EpcTidUser();
        return list;
    }
    @Override
    public int writeUserData(TagInfo tag, int offset, byte[] data) {
        String dataStr = "00";

        //LoggerUtil.debug(LOG_TAG,"write data : "+ tag.mEpcString.length() + " epc " + tag.mEpcString + " data : " + LoggerUtil.BytesToHex(data) + " offset " + offset+ " " + data.length+ " " + data[0] + " " +data[1]);
        uhf.setFilter(RFIDWithUHFBLE.Bank_EPC,32,/*96*/ tag.mEpcString.length()*4,tag.mEpcString);
        boolean rc = uhf.writeData("00000000", RFIDWithUHFUART.Bank_USER,offset,data.length/2,LoggerUtil.BytesToHex(data));
        if (!uhf.setFilter(RFIDWithUHFUART.Bank_EPC, 0, 0, dataStr)
                ||  !uhf.setFilter(RFIDWithUHFUART.Bank_TID, 0, 0, dataStr)
                ||  !uhf.setFilter(RFIDWithUHFUART.Bank_USER, 0, 0, dataStr))
        {
            mRfEventListener.onError(new RfidException("Filter disabling issue"));
        }
        if(rc){
            return 1;
        }else{
            return 0;
        }
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

    @Override
    public void close() {
        uhf.disconnect();
        uhf.free();
    }

    @Override
    public void stopOperation() {
        if(mCurrentAction == CurrentAction.INVENTORY){
            loopFlag = false;
            while (mCurrentAction != CurrentAction.NONE){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void setEventListener(RFIDEventListener eventListener) {
        this.mRfEventListener = eventListener;
    }

    @Override
    public void setKeyEventCallback(AgribiotKeyEventCallback keyEventCallBack) {
        this.mKeyEventCallback = keyEventCallBack;
    }

    @Override
    public int getBattery() {
        return uhf.getBattery();
    }


    @Override
    public void onKeyDown(int i) {
        LoggerUtil.debug(LOG_TAG,"On key down : " + i);
        if(mKeyEventCallback != null){
            this.mKeyEventCallback.KeyEvent();
        }
    }

    public void setBeep(boolean beep){
        uhf.setBeep(beep);
    }
    public void connect(String devAddress){
        mDeviceAddress = devAddress;
        if (mChafonConnectionStatus == ChafonConnectionStatus.CONNECTING) {
            LoggerUtil.debug(LOG_TAG,"Connecting to " + devAddress);
        } else {
            uhf.connect(mDeviceAddress, btStatus);
            mChafonConnectionStatus = ChafonConnectionStatus.CONNECTING;
        }
    }
    public ChafonConnectionStatus getConnectStatus() {
        return mChafonConnectionStatus;
    }


    class BTStatus implements ConnectionStatusCallback<Object> {
        @Override
        public void getStatus(final ConnectionStatus connectionStatus, final Object device1) {

            BluetoothDevice device = (BluetoothDevice) device1;

            LoggerUtil.debug(LOG_TAG,"Device " + device.getName() +" ("+device.getAddress() +") " + connectionStatus);

            if (connectionStatus == ConnectionStatus.CONNECTED) {

                /**
                 * Gatt register is not readable at this time, we need to ask for a callback on complete Gatt services discovered
                 */
                mDeviceName = device.getName();
                connected = true;
                mChafonConnectionStatus = ChafonConnectionStatus.CONNECTED;
                mRfEventListener.onConnect();


            } else if (connectionStatus == ConnectionStatus.DISCONNECTED) {
                mRfEventListener.onDisconnect();
                mChafonConnectionStatus = ChafonConnectionStatus.DISCONNECTED;
                connected = false;
            }
        }
    }
}