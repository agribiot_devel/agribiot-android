/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.rfid;


public interface RFIDHandler {
    public void setWritePower(int powerDbm);
    public void setReadPower(int powerDbm);
    public void startInventory(long timeout, TagInfoReadListener tagInfoReadListener);
    public TagInfo readClosestTag();
    public int writeUserData(TagInfo tag,int offset,byte[] data);
    public boolean isConnected();
    public void close();
    public void stopOperation();
    public void setEventListener(RFIDEventListener eventListener);
    public void setKeyEventCallback(AgribiotKeyEventCallback keyEventCallBack);
    public int getBattery();
}
