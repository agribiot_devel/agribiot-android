/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.rfid;

import android.os.AsyncTask;

import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;

import bzh.agribiot.database.Operation;
import bzh.agribiot.logger.LoggerUtil;



public class RfidWriteCounter extends
        AsyncTask<Void, Void, Integer> {


    int mTaskId = 0;
    static String LOG_TAG = "RfidWriteCounter";
    RFIDHandler rfidHandler;
    public static ConcurrentHashMap<String, TagInfo> tagRead;
    RFIDEventListener mEventListener;
    Operation resultOperation = null;
    public boolean errorOccur = false;
    TagInfo mTagInfo;
    public RfidWriteCounter(RFIDEventListener eventListener,int taskId, RFIDHandler rfidHandler,TagInfo closest) throws RfidException {
        this.rfidHandler =  rfidHandler;
        mEventListener = eventListener;
        if(rfidHandler.isConnected() == false){
            throw new RfidException("Device not connected");
        }
        mTaskId = taskId;
        mTagInfo = closest;
    }
    @Override
    protected Integer doInBackground(Void... voids) {
        LoggerUtil.debug(LOG_TAG,"doInBackground...");
        tagRead = new ConcurrentHashMap<String,TagInfo>();
        MyTagInfoListener tagInfoListener = new MyTagInfoListener(mEventListener,this);
        int rc = 0;
        short counter = 0;
        errorOccur = false;


        rc = mTagInfo.validateUserMemory();
        if (rc != 0) {
            // should init memory
            //initAndWriteMemory(mTagInfo);
            //counter = 2;
            return 1;
        } else {
            counter = getCurIndex(mTagInfo.getUserMemData());
            counter+=1;
            rc = writeIndex(mTagInfo,counter);
            LoggerUtil.debug(LOG_TAG,"Cur index : "+ counter + " write result : " +  rc);
            if(rc == 0){
                return 0;
            }

        }
        resultOperation = new Operation();
        resultOperation.setAnnotationTemplateId(mTaskId);
        //resultOperation.setTid(LoggerUtil.BytesToHex(Arrays.copyOfRange(mTagInfo.getTIDMemData(),0,12)));
        resultOperation.setCreated(Calendar.getInstance().getTime());
        resultOperation.setPostgreId(-1);
        resultOperation.setTagEmbeddedTaskIndex(counter);
        resultOperation.setSynchronizedFlag(false);



        return 0;
    }

    @Override
    protected void onPostExecute(Integer result) {
        LoggerUtil.debug(LOG_TAG,"onPostExecute result : " + result + " resultOperation " + resultOperation);
        if(result == 0 && resultOperation != null){
            mEventListener.onWriteComplete(resultOperation,mTagInfo);
        }else if(result == 1 && resultOperation == null){
            mEventListener.onError(new RfidException("Tag inconnu pour l'exploitation"));
        }else{
            mEventListener.onError(new RfidException("Tag non détecté"));
        }
    }

    static class MyTagInfoListener implements TagInfoReadListener
    {
        RfidWriteCounter mRfidWriteCounter;
        RFIDEventListener mRfidEventListener;
        public MyTagInfoListener(RFIDEventListener rfidEventListener, RfidWriteCounter rfidWriteCounter){
            mRfidEventListener = rfidEventListener;
            mRfidWriteCounter= rfidWriteCounter;
        }
        @Override
        public void TagReadCallback(TagInfo tag) {
            tagRead.put(tag.epcString(),tag);
        }

        @Override
        public void TagReadException(Exception e) {
            if(e.getMessage() != null){
                LoggerUtil.debug(LOG_TAG,"TagReadException : " + e.getMessage());
            }

            if(e.getMessage().equals("Device was reset externally.  Response opcode (2f) did not match command (22)")){
            }else if(e.getMessage().startsWith("Timeout") == true){
                mRfidWriteCounter.errorOccur = true;
            }else{
                mRfidEventListener.onError(new RfidException(e.getMessage()));

            }
        }
    }


    public short getCurIndex(byte []memory) {
        short rc = (short) (memory[8]<<8 | memory[9]);
        return rc;
    }


    public int writeIndex(TagInfo tag,short newIndex) {
        byte initBytes[] = {(byte)(newIndex>>8),(byte)newIndex};
        return rfidHandler.writeUserData(tag, 4, initBytes);
    }
}
