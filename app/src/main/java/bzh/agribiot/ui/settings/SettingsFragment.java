/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import bzh.agribiot.R;
import bzh.agribiot.logger.LoggerUtil;

public class SettingsFragment extends Fragment {
    public static final String LOG_TAG = "BluetoothFragment";
    SettingsTabAdapter settingsTabAdapter;
    private SettingsViewModel settingsViewModel;
    ViewPager2 settingsViewPager;
    int curTab = 0;
    TabLayout tabLayout;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if(bundle != null){
            if(bundle.getInt("tab") == 1){
                curTab = bundle.getInt("tab");
            }
        }
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        settingsTabAdapter = new SettingsTabAdapter(this);
        settingsViewPager = view.findViewById(R.id.pager);
        tabLayout = view.findViewById(R.id.settings_tabs);
        settingsViewPager.setAdapter(settingsTabAdapter);

        new TabLayoutMediator(tabLayout, settingsViewPager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                        if(position == 0){
                            tab.setText(R.string.tab_rfid_device);
                        }else if(position == 1){
                            tab.setText(R.string.tab_server);
                        }
                    }
                }).attach();
        if(curTab != 0){
            settingsViewPager.setCurrentItem(curTab);
        }
    }


    public class SettingsTabAdapter extends FragmentStateAdapter {
        private static final int nbrTabs = 2;
        public SettingsTabAdapter(Fragment fragment) {
            super(fragment);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            // Return a NEW fragment instance in createFragment(int)
            Fragment fragment = new BluetoothFragment();
            if(curTab != 0){
                position = curTab;
            }
            if(position == 0){
                return fragment;
            }else if(position == 1){
                fragment = new ServerFragment();
                return fragment;
            }
            return fragment;
        }

        @Override
        public int getItemCount() {
            return nbrTabs;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LoggerUtil.debug(LOG_TAG, "onActivityResult request code : " + requestCode + " result code : " + resultCode);
    }
}