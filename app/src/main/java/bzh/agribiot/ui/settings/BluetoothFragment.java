/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.settings;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.R;
import bzh.agribiot.database.Operation;
import bzh.agribiot.foregroundservice.ForegroundService;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.rfid.RFIDChafonHandler;
import bzh.agribiot.rfid.RFIDEventListener;
import bzh.agribiot.rfid.RFIDHandler;
import bzh.agribiot.rfid.RfidException;
import bzh.agribiot.rfid.TagInfo;
import bzh.agribiot.ui.AgribiotFragment;

public class BluetoothFragment extends AgribiotFragment implements RFIDEventListener, View.OnClickListener {
    public static final String LOG_TAG = "BluetoothFragment";
    private static final int REQUEST_ENABLE_BT = 11;
    private static final int REQUEST_SELECT_DEVICE = 10;
    protected static final int CONNECTED = 1;
    protected static final int DISCONNECTED = 2;
    public static final String  ACTION_GATT_SERVICES_DISCOVERED = "com.nordicsemi.nrfUART.ACTION_GATT_SERVICES_DISCOVERED";

    private SharedPreferences sharedPref = null;
    public int powerDbm = 0;
    String mDeviceAddress = null;
    public BluetoothAdapter mBtAdapter = null;
    RFIDHandler mRfidHandler;
    RFIDChafonHandler rfChafon = null;
    TextView tvBatteryValue;
    Switch autoConnectSwitch;
    Button connectBtnBluetooth;
    EditText rfidPowerEt;
    Button rfidPowerBtn;
    AgribiotApplication agribiotApplication ;
    BluetoothFragment mBluetoothFragment;
    public BluetoothFragment() {
        // Required empty public constructor
    }
    public static BluetoothFragment newInstance(Integer counter) {
        BluetoothFragment fragment = new BluetoothFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_settings_bluetooth, container, false);

        connectBtnBluetooth = (Button)root.findViewById(R.id.bluetooth_connect);
        rfidPowerEt = (EditText)root.findViewById(R.id.et_rfid_power);
        rfidPowerBtn = (Button)root.findViewById(R.id.btn_rfid_save);
        tvBatteryValue = (TextView)root.findViewById(R.id.tv_battery_value);
        autoConnectSwitch = (Switch)root.findViewById(R.id.autoconnect_switch);
        sharedPref = getActivity().getSharedPreferences("Agribiot",root.getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mDeviceAddress = sharedPref.getString("device_address", null);
        agribiotApplication = (AgribiotApplication) this.getActivity().getApplication();
        mRfidHandler = agribiotApplication.getRfidHandler();
        mBluetoothFragment = this;
        LoggerUtil.debug(LOG_TAG, "Saved bluetooth address [%s]", mDeviceAddress);


        if(agribiotApplication.isServiceRunning(ForegroundService.class.getName())){
            connectBtnBluetooth.setText(R.string.disconnect);
            setBatteryValue();

            RFIDHandler rfHandler = agribiotApplication.getRfidHandler();
            RFIDChafonHandler rfidChafonHandler = (RFIDChafonHandler)rfHandler;
            rfidPowerEt.setText(Integer.toString(rfidChafonHandler.getPower()));
        }else{
            connectBtnBluetooth.setText(R.string.connect);
        }


        Boolean autoConnect = sharedPref.getBoolean("autoconnect",false);
        autoConnectSwitch.setChecked(autoConnect);
        connectBtnBluetooth.setOnClickListener(this);
        powerDbm = sharedPref.getInt("powerDbm",12);
        rfidPowerEt.setText(Integer.toString(powerDbm));
        rfidPowerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int powerDbm = Integer.valueOf(rfidPowerEt.getText().toString());
                if(powerDbm <0){
                    powerDbm = 0;
                    rfidPowerEt.setText(Integer.toString(powerDbm));
                }else if(powerDbm > 30){
                    powerDbm = 30;
                    rfidPowerEt.setText(Integer.toString(powerDbm));
                }
                editor.putInt("powerDbm", powerDbm);
                LoggerUtil.debug(LOG_TAG,"Saved power dbm : "+powerDbm);
                editor.apply();

                RFIDHandler myRfidHandler = ((AgribiotApplication)mBluetoothFragment.getActivity().getApplication()).getRfidHandler();
                if(myRfidHandler != null && myRfidHandler.isConnected()){
                    try {
                        myRfidHandler.setReadPower(powerDbm);
                        myRfidHandler.setWritePower(powerDbm);
                    } catch (Exception e) {
                        LoggerUtil.debug(LOG_TAG,"Error settings powerdbm : "+e.getMessage());
                    }

                }
            }
        });
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                int intMsg = message.getData().getInt("IntMessage",-1);
                String strMsg = message.getData().getString("message");
                if(intMsg != -1){
                    if(intMsg == CONNECTED){
                        connectBtnBluetooth.setText(R.string.disconnect);
                        setBatteryValue();
                    }else if(intMsg == DISCONNECTED){
                        connectBtnBluetooth.setText(R.string.connect);
                    }
                }else if(strMsg != null){
                    showErrorString(strMsg);
                }

            }
        };
        /*
        autoConnectSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("autoconnect", isChecked);
                editor.commit();
            }
        });
        */

        IntentFilter iFilter = new IntentFilter(this.ACTION_GATT_SERVICES_DISCOVERED);
        this.getContext().registerReceiver(syncFinishedReceiver, iFilter);
        return root;
    }
    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    private void showBluetoothDevice(String deviceAddress) {
        if (mBtAdapter == null) {
            showErrorString("Bluetooth is not available");
            return;
        }
        if (!mBtAdapter.isEnabled()) {
            LoggerUtil.debug(LOG_TAG, "onClick - BT not enabled yet");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            Intent newIntent = new Intent(this.getContext(), DeviceListActivity.class);
            startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
        }
    }
    void setBatteryValue(){
        if(mRfidHandler != null){
            tvBatteryValue.setText(Integer.toString(mRfidHandler.getBattery()) + "%" );
        }
    }
    @Override
    public void onConnect() {
        SharedPreferences.Editor editor = sharedPref.edit();
        AgribiotApplication agribiotApplication = (AgribiotApplication) this.getActivity().getApplication();
        editor.putString("device_address", mDeviceAddress);
        editor.commit();
        LoggerUtil.debug(LOG_TAG, "Connected device address : [%s]", mDeviceAddress);
        editor.commit();

        if (rfChafon != null) {
            mRfidHandler = rfChafon;
            agribiotApplication.setRfidHandler(mRfidHandler);
            rfChafon.printVersion();
            rfChafon.setup();
            sendMessage(CONNECTED);
            startService();
        }
    }

    @Override
    public void onDisconnect() {
        rfChafon.close();
        mRfidHandler = null;
        agribiotApplication.setRfidHandler(mRfidHandler);
        stopService();
        sendMessage(DISCONNECTED);
    }

    @Override
    public void onError(RfidException e) {
        LoggerUtil.debug(LOG_TAG, "Error : " + e.getMessage());
        sendError(e.getMessage());
    }

    @Override
    public void onReadMemoryComplete(TagInfo tag) {

    }

    @Override
    public void onWriteComplete(Operation operation,TagInfo tag) {

    }

    @Override
    public void onClick(View v) {
        if(mRfidHandler == null || mRfidHandler.isConnected() == false){
            rfChafon= RFIDChafonHandler.getInstance();

            rfChafon.open(this,this.getContext());
            if (rfChafon.getConnectStatus() == RFIDChafonHandler.ChafonConnectionStatus.CONNECTING) {
                showErrorString(getString(R.string.connecting));
            } else {
                showBluetoothDevice(null);
            }

        }else{
            mRfidHandler.close();
            connectBtnBluetooth.setText(R.string.connect);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LoggerUtil.debug(LOG_TAG,"onActivityResult request code : " + requestCode + " result code : " + resultCode);
        switch (requestCode) {
            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (rfChafon.getConnectStatus() == RFIDChafonHandler.ChafonConnectionStatus.CONNECTED) {
                        rfChafon.close();
                    }
                    mDeviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    rfChafon.connect(mDeviceAddress);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    showErrorString("Bluetooth has turned on ");
                } else {
                    showErrorString("Problem in BT Turning ON ");
                }
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);

    }
    public void startService() {
        Intent serviceIntent = new Intent(this.getContext(), ForegroundService.class);
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this.getContext(), serviceIntent);
    }
    public void stopService() {
        Intent serviceIntent = new Intent(this.getContext(), ForegroundService.class);
        this.getContext().stopService(serviceIntent);
    }


    private BroadcastReceiver syncFinishedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            LoggerUtil.debug(LOG_TAG,"received " + BluetoothFragment.ACTION_GATT_SERVICES_DISCOVERED);

        }
    };
}
