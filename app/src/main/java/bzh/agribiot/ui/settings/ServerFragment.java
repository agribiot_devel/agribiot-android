/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import bzh.agribiot.R;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.AgribiotFragment;
import bzh.agribiot.volley.VolleyHelper;

public class ServerFragment extends AgribiotFragment {
    public static final String LOG_TAG = "ServerFragment";
    public SharedPreferences sharedPref = null;
    EditText apiUrlEt ;
    EditText apiTokenEt;
    ServerFragment mServerFragment;
    public ServerFragment() {
        // Required empty public constructor
    }
    public static ServerFragment newInstance(Integer counter) {
        ServerFragment fragment = new ServerFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_settings_server, container, false);
        sharedPref = getActivity().getSharedPreferences("Agribiot",root.getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        Button qrCodeBtn = (Button)root.findViewById(R.id.btn_qr_code);
        apiUrlEt = (EditText)root.findViewById(R.id.et_api_url) ;
        apiTokenEt = (EditText)root.findViewById(R.id.et_api_token) ;
        mServerFragment = this;
        loadParam();

        qrCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator.forSupportFragment(mServerFragment).initiateScan(); // `this` is the current Fragment
            }
        });
        return root;
    }
    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LoggerUtil.debug(LOG_TAG, "onActivityResult request code : " + requestCode + " result code : " + resultCode);
        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                String barcode = result.getContents();
                if (barcode != null) {
                    String[] parts = barcode.split(";");
                    LoggerUtil.debug(LOG_TAG, "Token result : " + barcode);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    apiUrlEt.setText(parts[0]);
                    apiTokenEt.setText(parts[1]);
                    VolleyHelper.TestConnection(mServerFragment,parts[0],parts[1],editor);

                }
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onTestOk(){
        loadParam();
        this.showErrorString("Serveur joignable, configuration ok");
    }

    public void loadParam(){
        String apiUrl = sharedPref.getString("apiUrl","");
        if(apiUrl.length()> 0){
            LoggerUtil.debug(LOG_TAG,"Saved url for api : "+apiUrl);
            apiUrlEt.setText(apiUrl);
        }
        String apiToken = sharedPref.getString("token","");
        if(apiUrl.length()> 0){
            LoggerUtil.debug(LOG_TAG,"Saved token for api : "+apiToken);
            apiTokenEt.setText(apiToken);
        }
    }
}
