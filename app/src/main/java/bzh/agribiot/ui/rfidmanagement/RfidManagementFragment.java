/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.rfidmanagement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;



import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.R;

import bzh.agribiot.foregroundservice.ForegroundService;
import bzh.agribiot.logger.LoggerUtil;

import bzh.agribiot.rfid.RFIDChafonHandler;
import bzh.agribiot.rfid.RFIDHandler;
import bzh.agribiot.rfid.TagInfo;
import bzh.agribiot.rfid.UnknownRFIDTagException;
import bzh.agribiot.ui.AgribiotFragment;
/*
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator;
import org.bouncycastle.crypto.params.Ed25519KeyGenerationParameters;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
*/
public class RfidManagementFragment extends AgribiotFragment {
    RfidManagementFragment mRfidManagementFragment;
    static final String LOG_TAG = "RfidManagementFragment";

    public static String RFID_MANAGEMENT_MESSAGE = "RFID_MANAGEMENT_MESSAGE";
    public enum RfidManagementMessageType {
        READ_TAG,
        ENABLE_TAG,
        DISABLE_TAG,
    };
    Button btnReadTag = null;
    Button btnEnableTag = null;
    Button btnDisableTag = null;
    Switch switchEnableBeep = null;
    EditText etEpc;
    EditText etTid;
    EditText etUser;
    EditText etVersion;
    EditText etLength;
    EditText etCounter;
    EditText etSecret;
    EditText etRfidDevName;
    Button btnRfidDevName;
    TagInfo currentTag ;
    AgribiotApplication mAgribiotApplication;
    AssetManager assetManager ;
    RfidManagementMessageType mRfidManagementMessageType;
    boolean initTag = false;
    public RfidManagementFragment() {
        // Required empty public constructor
    }
    public static RfidManagementFragment newInstance() {
        RfidManagementFragment fragment = new RfidManagementFragment();
        return fragment;
    }

    @SuppressLint("MissingPermission")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_rfid_management, container, false);
        //Button clearOperationBtn = root.findViewById(R.id.clear_synced_operation);
        mAgribiotApplication = (AgribiotApplication) this.getActivity().getApplication();

        btnReadTag = root.findViewById(R.id.btn_read);
        btnEnableTag = root.findViewById(R.id.btn_enable);
        btnDisableTag = root.findViewById(R.id.btn_disable);
        etEpc = root.findViewById(R.id.et_epc);
        etTid = root.findViewById(R.id.et_tid);
        etUser = root.findViewById(R.id.et_user);
        switchEnableBeep = root.findViewById(R.id.sw_rfid_dev_beep);

        etVersion = root.findViewById(R.id.et_version);
        etLength = root.findViewById(R.id.et_length);
        etCounter = root.findViewById(R.id.et_counter);
        etSecret = root.findViewById(R.id.et_secret);
        etRfidDevName = root.findViewById(R.id.et_rfid_dev_name);
        btnRfidDevName = root.findViewById(R.id.bt_save_rfid_dev_name);

        mRfidManagementFragment = this;
        assetManager = this.getContext().getAssets();
        if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())){
            LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this.getContext());
            Intent intent = new Intent(ForegroundService.SWITCH_MODE);
            intent.putExtra("mode",ForegroundService.RfidHandheldMode.RFID_SETTINGS_MODE.ordinal());
            lbm.sendBroadcast(intent);
            RFIDHandler rfHandler = mAgribiotApplication.getRfidHandler();
            RFIDChafonHandler rfidChafonHandler = (RFIDChafonHandler)rfHandler;
            etRfidDevName.setText(rfidChafonHandler.getRfidDevName());
        }else{
            this.showErrorString("L'appareil n'est pas connecté");
        }

        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(mServiceBroadcastReceiver, new IntentFilter(RfidManagementFragment.RFID_MANAGEMENT_MESSAGE));


        btnReadTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(view.getContext());
                Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                intent.putExtra("mode",ForegroundService.RfidHandheldMode.RFID_SETTINGS_MODE.ordinal());
                mRfidManagementMessageType = RfidManagementMessageType.READ_TAG;
                intent.putExtra("action",mRfidManagementMessageType.ordinal());
                lbm.sendBroadcast(intent);
            }
        });

        btnRfidDevName.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(etRfidDevName.getText() != null){
                    if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())){
                        RFIDHandler rfHandler = mAgribiotApplication.getRfidHandler();
                        RFIDChafonHandler rfidChafonHandler = (RFIDChafonHandler)rfHandler;
                        rfidChafonHandler.setRfidDevName(etRfidDevName.getText().toString());
                        etRfidDevName.setText(rfidChafonHandler.getRfidDevName());
                    }
                }
            }
        });
        btnEnableTag.setAlpha(.5f);
        btnEnableTag.setClickable(false);
        btnDisableTag.setAlpha(.5f);
        btnDisableTag.setClickable(false);

        btnEnableTag.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(v.getContext());
                Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                intent.putExtra("mode",ForegroundService.RfidHandheldMode.RFID_SETTINGS_MODE.ordinal());
                mRfidManagementMessageType = RfidManagementMessageType.ENABLE_TAG;
                intent.putExtra("action",mRfidManagementMessageType.ordinal());
                intent.putExtra("tag",currentTag);
                lbm.sendBroadcast(intent);
            }
        });
        btnDisableTag.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(v.getContext());
                Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                intent.putExtra("mode",ForegroundService.RfidHandheldMode.RFID_SETTINGS_MODE.ordinal());
                mRfidManagementMessageType = RfidManagementMessageType.DISABLE_TAG;
                intent.putExtra("action",mRfidManagementMessageType.ordinal());
                intent.putExtra("tag",currentTag);
                lbm.sendBroadcast(intent);
            }
        });

        switchEnableBeep.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())) {
                    RFIDHandler rfHandler = mAgribiotApplication.getRfidHandler();
                    RFIDChafonHandler rfidChafonHandler = (RFIDChafonHandler)rfHandler;
                    rfidChafonHandler.setBeep(isChecked);
                }
            }
        });
        return root;
    }


    private BroadcastReceiver mServiceBroadcastReceiver= new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!intent.hasExtra("tag")){
                return;
            }

            currentTag = (TagInfo) intent.getSerializableExtra("tag");
            etEpc.setText(currentTag.epcString());
            etTid.setText(currentTag.getTidString());
            etUser.setText(LoggerUtil.BytesToHex(currentTag.getUserMemData()));

            etVersion.setText(Integer.toString(Byte.toUnsignedInt(currentTag.getPayloadVersion())));
            etLength.setText(Integer.toString(Byte.toUnsignedInt(currentTag.getPayloadLength())));
            etCounter.setText(Integer.toString(currentTag.getCurIndex() & 0xffff));


            try {
                LoggerUtil.debug(LOG_TAG,"Validate TID :  " + currentTag.validateTid(getJSONString()));
                LoggerUtil.debug(LOG_TAG, "Tag vendor : " + currentTag.getBrandIdentifier());
                LoggerUtil.debug(LOG_TAG, "Tag model : " + currentTag.getModel());
                LoggerUtil.debug(LOG_TAG, "User memory size : " + currentTag.getUserMemorySize());
                LoggerUtil.debug(LOG_TAG, "Epc memory size : " + currentTag.getEpcMemorysize());

            } catch (UnknownRFIDTagException e) {
                showError(e);
            }
            if(currentTag.validateUserMemory() != 0x0){
                btnEnableTag.setAlpha(1f);
                btnEnableTag.setClickable(true);
                btnDisableTag.setAlpha(.5f);
                btnDisableTag.setClickable(false);
            }else{
                btnDisableTag.setAlpha(1f);
                btnDisableTag.setClickable(true);
                btnEnableTag.setAlpha(.5f);
                btnEnableTag.setClickable(false);
            }
            if(mRfidManagementMessageType == RfidManagementMessageType.ENABLE_TAG){
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                getParentFragmentManager().popBackStack();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Enable more tag ?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                mRfidManagementMessageType = RfidManagementMessageType.READ_TAG;
            }


/*
            if(tag != null) {
                if (initTag == true) {

                    try {
                        initMemory(tag);
                    } catch (Exception e) {
                        showError(e);
                        e.printStackTrace();
                    }


                }
            }
*/
        }
    };





    private String getJSONString()
    {
        String str = "";
        try
        {
            InputStream in = assetManager.open("mdid_list.json");
            InputStreamReader isr = new InputStreamReader(in);
            char [] inputBuffer = new char[100];

            int charRead;
            while((charRead = isr.read(inputBuffer))>0)
            {
                String readString = String.copyValueOf(inputBuffer,0,charRead);
                str += readString;
            }
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }

        return str;
    }



    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mServiceBroadcastReceiver);
        super.onDestroy();
    }


                /*
                try {
                    String strPrivateKey =  getResources().getString(R.string.privatekey);
                    String strPublicKey =  getResources().getString(R.string.publickey);
                    LoggerUtil.debug(LOG_TAG,"Private key : " + strPrivateKey);
                    LoggerUtil.debug(LOG_TAG,"Public key : " + strPublicKey);

                    byte[] privateKeyBytes = Base64.getMimeDecoder().decode(strPrivateKey);
                    byte[] publicKeyBytes = Base64.getMimeDecoder().decode(strPublicKey);






                    System.out.println("ED25519 with BC");
                    Security.addProvider(new BouncyCastleProvider());
                    Provider provider = Security.getProvider("BC");
                    System.out.println("Provider          :" + provider.getName() + " Version: " + provider.getVersion());
                    // generate ed25519 keys
                    SecureRandom RANDOM = new SecureRandom();
                    Ed25519KeyPairGenerator keyPairGenerator = new Ed25519KeyPairGenerator();
                    keyPairGenerator.init(new Ed25519KeyGenerationParameters(RANDOM));
                    AsymmetricCipherKeyPair asymmetricCipherKeyPair = keyPairGenerator.generateKeyPair();


                    Ed25519PrivateKeyParameters privateKey = new Ed25519PrivateKeyParameters(privateKeyBytes, 0);
                    Ed25519PublicKeyParameters publicKey = new Ed25519PublicKeyParameters(publicKeyBytes, 0);

                    LoggerUtil.debug(LOG_TAG,"Private key : " + LoggerUtil.BytesToHex(privateKey.getEncoded()));
                    LoggerUtil.debug(LOG_TAG,"Public key : " + LoggerUtil.BytesToHex(publicKey.getEncoded()));
                    // the message
                    byte[] message = "Message to sign".getBytes("utf-8");
                    // create the signature
                    Signer signer = new Ed25519Signer();
                    signer.init(true, privateKey);
                    signer.update(message, 0, message.length);
                    byte[] signature = signer.generateSignature();
                    // verify the signature
                    Signer verifier = new Ed25519Signer();
                    verifier.init(false, publicKey);
                    verifier.update(message, 0, message.length);
                    boolean shouldVerify = verifier.verifySignature(signature);
                    // output
                    byte[] privateKeyEncoded = privateKey.getEncoded();
                    byte[] publicKeyEncoded = publicKey.getEncoded();
                    System.out.println("privateKey Length :" + privateKeyEncoded.length + " Data:"
                            + LoggerUtil.BytesToHex(privateKeyEncoded));
                    System.out.println("publicKey Length  :" + publicKeyEncoded.length + " Data:"
                            + LoggerUtil.BytesToHex(publicKeyEncoded));
                    System.out.println(
                            "signature Length  :" + signature.length + " Data:" + LoggerUtil.BytesToHex(signature));
                    System.out.println("signature correct :" + shouldVerify);
                    // rebuild the keys
                    System.out.println("Rebuild the keys and verify the signature with rebuild public key");
                    Ed25519PrivateKeyParameters privateKeyRebuild = new Ed25519PrivateKeyParameters(privateKeyEncoded, 0);
                    Ed25519PublicKeyParameters publicKeyRebuild = new Ed25519PublicKeyParameters(publicKeyEncoded, 0);
                    byte[] privateKeyRebuildEncoded = privateKeyRebuild.getEncoded();
                    System.out.println("privateKey Length :" + privateKeyRebuild.getEncoded().length + " Data:"
                            + LoggerUtil.BytesToHex(privateKeyRebuild.getEncoded()));
                    byte[] publicKeyRebuildEncoded = publicKeyRebuild.getEncoded();
                    System.out.println("publicKey Length  :" + publicKeyRebuild.getEncoded().length + " Data:"
                            + LoggerUtil.BytesToHex(publicKeyRebuild.getEncoded()));
                    // compare the keys
                    System.out.println("private Keys Equal:" + Arrays.equals(privateKeyEncoded, privateKeyRebuildEncoded));
                    System.out.println("public Keys Equal :" + Arrays.equals(publicKeyEncoded, publicKeyRebuildEncoded));
                    // verify the signature with rebuild public key
                    Signer verifierRebuild = new Ed25519Signer();
                    verifierRebuild.init(false, publicKeyRebuild);
                    verifierRebuild.update(message, 0, message.length);
                    boolean shouldVerifyRebuild = verifierRebuild.verifySignature(signature);
                    System.out.println("signature correct :" + shouldVerifyRebuild + " with rebuild public key");
                }  catch (IOException e) {
                    e.printStackTrace();
                } catch (CryptoException e) {
                    e.printStackTrace();
                }
*/
                    /*
                try {
                    byte[] privateKeyBytes = Base64.getMimeDecoder().decode(strPrivateKey);
                    LoggerUtil.debug(LOG_TAG,"ED25519 with BC");
                    Security.addProvider(new BouncyCastleProvider());
                    Provider provider = Security.getProvider("BC");
                    LoggerUtil.debug(LOG_TAG,"Provider          :" + provider.getName() + " Version: " + provider.getVersion());
                    Ed25519PrivateKeyParameters privateKey = new Ed25519PrivateKeyParameters(privateKeyBytes, 0);

                    LoggerUtil.debug(LOG_TAG,"Private key : " + LoggerUtil.BytesToHex(privateKey.getEncoded()));
                    // the message
                    // create the signature
                    Signer signer = new Ed25519Signer();
                    signer.init(true, privateKey);
                    signer.update(tag.getTIDMemData(), 0, tag.getTIDMemData().length);
                    byte[] signature = signer.generateSignature();
                    byte initBytes[] = {TagInfo.MAGIC_NUMBER[0],TagInfo.MAGIC_NUMBER[1],TagInfo.MAGIC_NUMBER[2],TagInfo.MEMORY_MAPPING_VERSION,TagInfo.MEMORY_USED_BYTE,0x0,0x0,0x0,0x0,0x1};
                    LoggerUtil.debug(LOG_TAG,"Generated signature : "  + LoggerUtil.BytesToHex(signature));

                    String strPublicKey =  getResources().getString(R.string.publickey);
                    byte[] publicKeyBytes = Base64.getMimeDecoder().decode(strPublicKey);
                    Ed25519PublicKeyParameters publicKey = new Ed25519PublicKeyParameters(publicKeyBytes, 0);
                    Signer verifier = new Ed25519Signer();
                    verifier.init(false, publicKey);
                    verifier.update(tag.getTIDMemData(), 0, tag.getTIDMemData().length);
                    boolean shouldVerify = verifier.verifySignature(signature);
                    LoggerUtil.debug(LOG_TAG,"Check signature : "  + shouldVerify);

                    // mRfidHandler.writeUserData()
                } catch (CryptoException e) {
                    e.printStackTrace();
                }*/
}