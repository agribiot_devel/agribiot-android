/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.task;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.repository.AnnotationTemplateRepository;

public class TaskViewModel extends AndroidViewModel {

    private AnnotationTemplateRepository mRepository;

    private final LiveData<List<AnnotationTemplate>> mAllAnnotations;

    public TaskViewModel (Application application) {
        super(application);
        mRepository = new AnnotationTemplateRepository(application);
        mAllAnnotations = mRepository.getAllAnnotations();
    }


    public LiveData<List<AnnotationTemplate>> getAllAnnotations() { return mAllAnnotations; }

    public void insert(AnnotationTemplate annotationTemplate) { mRepository.insert(annotationTemplate); }
    public void update(AnnotationTemplate annotationTemplate) { mRepository.update(annotationTemplate); }



    public void deleteOldEntry(int []postGreID){
        mRepository.deleteOldEntry(postGreID);
    }
}