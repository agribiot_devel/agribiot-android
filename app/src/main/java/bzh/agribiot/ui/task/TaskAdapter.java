/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import bzh.agribiot.R;
import bzh.agribiot.database.AnnotationTemplate;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {

    private List<AnnotationTemplate> mAnnotationTemplates; // Cached copy of words
    private final LayoutInflater mInflater;

    TaskAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.row_task, parent, false);
        return new TaskViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        if(mAnnotationTemplates != null){
            AnnotationTemplate current = mAnnotationTemplates.get(position);
            holder.taskDescriptionTextView.setText(current.getDescription());
        }

    }
    void setAnnotationTemplates(List<AnnotationTemplate> annotationTemplates){
        this.mAnnotationTemplates = annotationTemplates;
        notifyDataSetChanged();
    }

    AnnotationTemplate getAnnotation(int position){
        return this.mAnnotationTemplates.get(position);
    }

    @Override
    public int getItemCount() {
        if (mAnnotationTemplates != null)
            return mAnnotationTemplates.size();
        else {
            return 0;
        }
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder {
        private final TextView taskDescriptionTextView;

        public TaskViewHolder(@NonNull View itemView) {
            super(itemView);
            taskDescriptionTextView = itemView.findViewById(R.id.rowdescription);
        }
    }
}