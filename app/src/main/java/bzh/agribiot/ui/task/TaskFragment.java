/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.task;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.R;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;

import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.AgribiotFragment;
import bzh.agribiot.ui.operation.OperationFragment;
import bzh.agribiot.ui.settings.SettingsFragment;
import bzh.agribiot.ui.tools.RecyclerItemClickListener;


public class TaskFragment extends AgribiotFragment {
    public TaskAdapter taskAdapter = null;
    private TaskViewModel mTaskViewModel;
    public SharedPreferences sharedPref = null;

    AgribiotDatabase agDb;
    AgribiotApplication agribiotApplication ;
    static String LOG_TAG = "TaskFragment";
    TaskFragment mTaskFragment;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_task, container, false);

        mTaskViewModel =
                new ViewModelProvider(this).get(TaskViewModel.class);

        RecyclerView recyclerView = root.findViewById(R.id.taskRecyclerList);
        sharedPref = getActivity().getSharedPreferences("Agribiot",root.getContext().MODE_PRIVATE);
        agribiotApplication = (AgribiotApplication) this.getActivity().getApplication();
        agDb = AgribiotDatabase.getDatabase(getContext());

        taskAdapter = new TaskAdapter(getContext());
        recyclerView.setAdapter(taskAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayout.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        mTaskFragment = this;

        mTaskViewModel.getAllAnnotations().observe(this.getViewLifecycleOwner(), annotationTemplates -> {
            // Update the cached copy of the words in the adapter.
            ArrayList<AnnotationTemplate> tasks = new ArrayList<AnnotationTemplate>();
            for(AnnotationTemplate annotationTemplate :annotationTemplates){
                //LoggerUtil.debug(LOG_TAG,annotationTemplate.getDescription() + " " + annotationTemplate.getAnnotationType());
                if(annotationTemplate.getAnnotationType().equals("TASK")){
                    tasks.add(annotationTemplate);
                }

                /*
                */

            }
            taskAdapter.setAnnotationTemplates(tasks);
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this.getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        TaskAdapter taskAdapter = (TaskAdapter) recyclerView.getAdapter();
                        Bundle bundle = new Bundle();
                        bundle.putInt("annotationTemplateId",taskAdapter.getAnnotation(position).getAnnotationTemplateId());
                        Navigation.findNavController(view).navigate(R.id.action_nav_task_to_nav_operation,bundle);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
        String apiUrl = sharedPref.getString("apiUrl",null);
        if(apiUrl == null){
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Bundle bundle = new Bundle();
                            bundle.putInt("tab",1);
                            Navigation.findNavController(TaskFragment.this.getView()).navigate(R.id.action_nav_task_to_server_settings,bundle);
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("La connexion au serveur doit être configurer").setPositiveButton("Configurer le serveur", dialogClickListener)
                    .setNegativeButton("Plus tard", dialogClickListener).show();
        }

        return root;
    }




}