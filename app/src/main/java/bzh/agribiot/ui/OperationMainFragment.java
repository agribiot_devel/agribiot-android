package bzh.agribiot.ui;

import android.app.Activity;
import android.content.Intent;

import java.util.ArrayList;

import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationWithAllForeign;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.observation.ObservationAdapter;
import bzh.agribiot.ui.observation.ObservationViewModel;

public class OperationMainFragment extends AgribiotFragment   {
    protected ObservationViewModel mObservationViewModel;
    protected AgribiotDatabase agDb;
    protected ArrayList<OperationWithAllForeign> mOperations = null;
    protected ObservationAdapter mObservationAdapter;
    public static String OPERATION_MESSAGE = "OPERATION_MESSAGE";

    static String LOG_TAG = "OperationMainFragment";
    Operation curImageOperation = null;
    String curImagePath = null;
    public void setCurImageOperation(Operation operation, String imagePath) {
        this.curImageOperation = operation;
        this.curImagePath = imagePath;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AgribiotFragment.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            LoggerUtil.debug(LOG_TAG,"onActivityResult for image capture on operation : "+ curImageOperation.getOperationId() + " " + curImagePath);
            if(curImagePath != null && curImageOperation != null){
                curImageOperation.setPhoto(curImagePath);
                mObservationViewModel.update(curImageOperation);
            }
            //imageView.setImageBitmap(imageBitmap);
            curImageOperation = null;
            curImagePath = null;
        }
    }
    public void onClickOnOperation(Operation operation, AnnotationTemplate annotationTemplate){
        /**
         * Implement on child class
         */
    }
}
