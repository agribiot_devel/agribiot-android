/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.object;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.MapTileIndex;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import bzh.agribiot.R;
import bzh.agribiot.database.SensorObject;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.AgribiotFragment;
import bzh.agribiot.ui.tools.RecyclerItemClickListener;


public class ObjectFragment extends AgribiotFragment {
    ObjectAdapter mObjectAdapter;
    ObjectViewModel mObjectViewModel;
    private MapView myOpenMapView;
    MyLocationNewOverlay mLocationOverlay;
    public ObjectFragment() {
        // Required empty public constructor
    }
    public static ObjectFragment newInstance() {
        ObjectFragment fragment = new ObjectFragment();
        return fragment;
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_objects, container, false);
        RecyclerView recyclerView = root.findViewById(R.id.objectRecyclerList);
        Drawable marker = getResources().getDrawable(R.drawable.ic_agribiot_tag);
        ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();

        Configuration.getInstance().load(root.getContext(), PreferenceManager.getDefaultSharedPreferences(root.getContext()));

        myOpenMapView = (MapView)root.findViewById(R.id.mapview);
        myOpenMapView.setBuiltInZoomControls(true);
        myOpenMapView.setClickable(true);

        myOpenMapView.setTileSource(new OnlineTileSourceBase("IGN", 0, 20, 256, "",
                new String[] { ""
                         }) {

            @Override
            public String getTileURLString(long pMapTileIndex) {
                return "https://wxs.ign.fr/pratique/geoportail/wmts?"+
                        "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&TILEMATRIXSET=PM"+
                        "&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&STYLE=normal&FORMAT=image/jpeg"+
                        "&TILECOL=" + MapTileIndex.getX(pMapTileIndex)+
                        "&TILEROW=" + MapTileIndex.getY(pMapTileIndex) +
                        "&TILEMATRIX=" +  MapTileIndex.getZoom(pMapTileIndex);
            }
        });


        myOpenMapView.getController().setZoom(18);
        myOpenMapView.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.NEVER);
        myOpenMapView.setMultiTouchControls(true);
        mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(root.getContext()),myOpenMapView);
        this.mLocationOverlay.enableFollowLocation();
        this.mLocationOverlay.enableMyLocation();
        this.mLocationOverlay.setDrawAccuracyEnabled(false);


        mObjectAdapter = new ObjectAdapter(new ObjectAdapter.SensorObjectDiff());
        recyclerView.setAdapter(mObjectAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayout.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        mObjectViewModel =
                new ViewModelProvider(this).get(ObjectViewModel.class);
        mObjectViewModel.getAllSensorObjects().observe(this.getViewLifecycleOwner(), objects -> {
            LoggerUtil.debug(LOG_TAG,"Received : " + objects.size() + " objects");

            // Update the cached copy of the words in the adapter.
            Collections.sort(objects, new Comparator<SensorObject>() {
                @Override
                public int compare(SensorObject lhs, SensorObject rhs) {
                    // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                    return -1;
                }
            });
            mObjectAdapter.submitList(objects);
            for(SensorObject sObject :objects){
                Marker tagMarker = new Marker(myOpenMapView);
                tagMarker.setPosition( new GeoPoint(sObject.getLat(),sObject.getLon()));
                tagMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                tagMarker.setIcon(marker);
                //tagMarker.setTextIcon(sObject.getUuid());
                myOpenMapView.getOverlays().add(tagMarker);
            }

        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this.getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        myOpenMapView.getController().setCenter(new GeoPoint(mObjectAdapter.getObject(position).getLat(),mObjectAdapter.getObject(position).getLon()));
                        myOpenMapView.getController().animateTo(new GeoPoint(mObjectAdapter.getObject(position).getLat(),mObjectAdapter.getObject(position).getLon()));
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );



        mLocationOverlay.runOnFirstFix(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        myOpenMapView.getController().setCenter(mLocationOverlay.getMyLocation());
                        myOpenMapView.getController().animateTo(mLocationOverlay.getMyLocation());
                    }
                });

            }
        });
        myOpenMapView.getOverlays().add(this.mLocationOverlay);


        return root;
    }

}
