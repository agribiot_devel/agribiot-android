/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.object;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import bzh.agribiot.database.SensorObject;

public class ObjectAdapter extends ListAdapter<SensorObject, ObjectViewHolder> {
    public ObjectAdapter(@NonNull DiffUtil.ItemCallback<SensorObject> diffCallback) {
        super(diffCallback);
    }

    @Override
    public ObjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ObjectViewHolder.create(parent);
    }
    public SensorObject getObject(int position) {
        return this.getItem(position);
    }
    @Override
    public void onBindViewHolder(@NonNull ObjectViewHolder holder, int position) {
        SensorObject current = getItem(position);
        holder.bind(current);

    }
    public static class SensorObjectDiff extends DiffUtil.ItemCallback<SensorObject> {

        @Override
        public boolean areItemsTheSame(@NonNull SensorObject oldItem, @NonNull SensorObject newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull SensorObject oldItem, @NonNull SensorObject newItem) {
            return oldItem.getSensorObjectId() == newItem.getSensorObjectId();
        }
    }
}
