/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.object;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import bzh.agribiot.R;
import bzh.agribiot.database.SensorObject;

public class ObjectViewHolder extends RecyclerView.ViewHolder {
    private final TextView uuidTextView;
    private final TextView descriptionTextView;
    private final TextView typeTextView;
    static String LOG_TAG = "OperationViewHolder";

    public ObjectViewHolder(@NonNull View itemView) {
        super(itemView);
        uuidTextView = (TextView) itemView.findViewById(R.id.row_uuid);
        descriptionTextView = (TextView) itemView.findViewById(R.id.row_description);
        typeTextView = (TextView) itemView.findViewById(R.id.row_type);
    }



    public void bind(SensorObject sensorObject) {
        uuidTextView.setText(sensorObject.getUuid());
        descriptionTextView.setText(sensorObject.getDescription());
        typeTextView.setText(sensorObject.getType() + " " + sensorObject.getUnit());
    }

    public static ObjectViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_object, parent, false);
        return new ObjectViewHolder(view);
    }
}
