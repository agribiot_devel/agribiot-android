/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.object;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.SensorObject;
import bzh.agribiot.repository.AnnotationTemplateRepository;
import bzh.agribiot.repository.OperationRepository;
import bzh.agribiot.repository.SensorObjectRepository;

public class ObjectViewModel  extends AndroidViewModel {

    private SensorObjectRepository mSensorObjectRepository;

    private final LiveData<List<SensorObject>> mAllSensorObject;


    public ObjectViewModel (Application application) {
        super(application);
        mSensorObjectRepository = new SensorObjectRepository(application);
        mAllSensorObject = mSensorObjectRepository.getmAllSensorObjects();
    }
    public LiveData<List<SensorObject>> getAllSensorObjects() {return mAllSensorObject;}


}
