/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.operation;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.R;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationWithAllForeign;
import bzh.agribiot.foregroundservice.ForegroundService;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.repository.AnnotationTemplateRepository;
import bzh.agribiot.ui.AgribiotFragment;
import bzh.agribiot.ui.OperationMainFragment;
import bzh.agribiot.ui.dimension.DimensionDialogFragment;
import bzh.agribiot.ui.observation.ObservationAdapter;
import bzh.agribiot.ui.observation.ObservationViewModel;
import bzh.agribiot.ui.tools.RecyclerItemClickListener;

public class OperationFragment extends OperationMainFragment {
    static final String LOG_TAG = "OperationFragment";

    int annotationTemplateId ;
    OperationFragment mOperationFragment;
    RecyclerView mOperationRecyclerView;
    AgribiotApplication mAgribiotApplication;
    AnnotationTemplateRepository mAnnotationTemplateRepository;
    DimensionDialogFragment dimensionDialogFragment = null;

    boolean writeRequested = false;

    @SuppressLint("MissingPermission")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_operation, container, false);
        TextView taskDescTv = root.findViewById(R.id.TaskDescription);
        mAgribiotApplication = (AgribiotApplication) this.getActivity().getApplication();
        Button btnWriteTag = root.findViewById(R.id.btn_write_tag);
        mOperationRecyclerView = root.findViewById(R.id.operationRecyclerList);

        mObservationViewModel =
                new ViewModelProvider(this).get(ObservationViewModel.class);
        mObservationAdapter = new ObservationAdapter(this);
        mOperationRecyclerView.setAdapter(mObservationAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mOperationRecyclerView.getContext(),
                LinearLayout.VERTICAL);
        mOperationRecyclerView.addItemDecoration(dividerItemDecoration);
        mOperationRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        agDb = AgribiotDatabase.getDatabase(getContext());

        mOperationFragment = this;
        Bundle bundle = getArguments();
        annotationTemplateId = bundle.getInt("annotationTemplateId");
        mAnnotationTemplateRepository = new AnnotationTemplateRepository(this.mAgribiotApplication,annotationTemplateId);



        agDb.operationWithAllForeignDao().findAllByAnnotationTemplateId(annotationTemplateId).observe(this.getViewLifecycleOwner(),operationWithAlls -> {
            if(operationWithAlls != null){
                mOperations = (ArrayList<OperationWithAllForeign>) operationWithAlls;
                /*for(int i = 0;i<operationWithAlls.size();i++){
                    LoggerUtil.debug(LOG_TAG,"---- "+i+" ----");
                    LoggerUtil.debug(LOG_TAG,"Operation : " + operationWithAlls.get(i).getOperation().toString());
                    LoggerUtil.debug(LOG_TAG,"Annotation : " + operationWithAlls.get(i).getAnnotationTemplate().toString());
                    LoggerUtil.debug(LOG_TAG,"Dimensionvalue Size : " + operationWithAlls.get(i).getNbrDimensionValues());

                }*/
                if(operationWithAlls.size()> 0){
                    refreshObservationList(mOperations);
                }
                if(writeRequested){
                    createDimensionDialog(mOperations.get(0).getOperation());
                    writeRequested = false;
                }
            }
        });
        mAnnotationTemplateRepository.get().observe(this.getViewLifecycleOwner(),annotationTemplate -> {
            taskDescTv.setText(annotationTemplate.getDescription());
            if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())){
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this.getContext());
                Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                intent.putExtra("mode",ForegroundService.RfidHandheldMode.TASK_MODE.ordinal());
                intent.putExtra("AnnotationTemplate",annotationTemplate);
                lbm.sendBroadcast(intent);
            }

            btnWriteTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())){
                        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(view.getContext());
                        Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                        intent.putExtra("mode",ForegroundService.RfidHandheldMode.TASK_MODE.ordinal());
                        intent.putExtra("AnnotationTemplate",annotationTemplate);
                        intent.putExtra("write",writeRequested);
                        lbm.sendBroadcast(intent);
                    }else{
                        showErrorString("L'appareil n'est pas connecté");
                    }
                }
            });
        });
        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(mServiceBroadcastReceiver, new IntentFilter(OperationMainFragment.OPERATION_MESSAGE));

        return root;
    }
    public void refreshObservationList(List<OperationWithAllForeign> operations){
        Collections.sort(operations, new Comparator<OperationWithAllForeign>() {
            @Override
            public int compare(OperationWithAllForeign lhs, OperationWithAllForeign rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                if (lhs.getOperation().getCreated().compareTo(rhs.getOperation().getCreated()) < 0) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
        mObservationAdapter.setOperations(operations);
    }

    @Override
    public void onClickOnOperation(Operation operation, AnnotationTemplate annotationTemplate) {
        LoggerUtil.debug(LOG_TAG,"onItemClick for " + operation.getOperationId() + " " + annotationTemplate.getAnnotationTemplateId());
        Bundle bundle = new Bundle();
        bundle.putInt("operationId",operation.getOperationId());
        bundle.putInt("annotationTemplateId",annotationTemplate.getAnnotationTemplateId());
        Navigation.findNavController(this.getView()).navigate(R.id.action_nav_operation_to_nav_dimension,bundle);
    }
    private BroadcastReceiver mServiceBroadcastReceiver= new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!intent.hasExtra("operation")) {
                return;
            }
            writeRequested = true;
        }
    };
    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mServiceBroadcastReceiver);
        super.onDestroy();
    }
    public void createDimensionDialog(Operation operation){
        if(dimensionDialogFragment == null){
            dimensionDialogFragment = DimensionDialogFragment.newInstance(operation.getAnnotationTemplateId(),operation.getOperationId());
        }else{
            if(dimensionDialogFragment.isVisible()){
                dimensionDialogFragment.dismiss();
            }
            dimensionDialogFragment = DimensionDialogFragment.newInstance(operation.getAnnotationTemplateId(),operation.getOperationId());
        }
        dimensionDialogFragment.show(this.getActivity().getSupportFragmentManager(),"Valeurs");
    }
}