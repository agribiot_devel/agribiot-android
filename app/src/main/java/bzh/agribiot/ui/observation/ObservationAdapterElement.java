/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.observation;


import java.util.ArrayList;

import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationImage;

public class ObservationAdapterElement {
    Operation operation;
    AnnotationTemplate annotationTemplate;
    ArrayList<DimensionValue> dimensionValues;
    ArrayList<OperationImage> operationImages;

    public ObservationAdapterElement(Operation operation, AnnotationTemplate annotationTemplate) {
        this.operation = operation;
        this.annotationTemplate = annotationTemplate;

    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public AnnotationTemplate getAnnotation() {
        return annotationTemplate;
    }

    public void setAnnotation(AnnotationTemplate annotation) {
        this.annotationTemplate = annotation;
    }



}
