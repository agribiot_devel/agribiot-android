/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.observation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.R;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationDao;
import bzh.agribiot.database.OperationWithAllForeign;
import bzh.agribiot.database.SensorObject;
import bzh.agribiot.database.SensorObjectDao;
import bzh.agribiot.foregroundservice.ForegroundService;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.OperationMainFragment;
import bzh.agribiot.ui.dimension.DimensionDialogFragment;
import bzh.agribiot.ui.rfidmanagement.RfidManagementFragment;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class ObservationFragment extends OperationMainFragment{
    static String LOG_TAG = "ObservationFragment";
    ObservationFragment mObservationFragment;
    Button buttonWriteObservation = null;
    Spinner spinner = null;
    LayoutInflater myInflater;

    DimensionDialogFragment dimensionDialogFragment = null;
    ArrayList<AnnotationTemplate> mAllObservation;
    AgribiotApplication mAgribiotApplication;
    boolean writeRequested = false;
    AnnotationTemplate newAnnotationTemplate = null;

    @SuppressLint("MissingPermission")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mObservationViewModel =
                new ViewModelProvider(this).get(ObservationViewModel.class);
        View root = inflater.inflate(R.layout.fragment_observation, container, false);
        RecyclerView observationRecyclerView = root.findViewById(R.id.operationRecyclerList);
        spinner = (Spinner)root.findViewById(R.id.observation_spinner);
        MyObservationadapter spinnerAdapter = new MyObservationadapter(this.getActivity(), android.R.layout.simple_spinner_item,new ArrayList<AnnotationTemplate>());
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        agDb = AgribiotDatabase.getDatabase(getContext());
        buttonWriteObservation = (Button)root.findViewById(R.id.btn_write_observation);
        this.mObservationFragment = this;
        myInflater = (LayoutInflater)this.getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);

        mObservationAdapter = new ObservationAdapter(this);
        observationRecyclerView.setAdapter(mObservationAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(observationRecyclerView.getContext(),
                LinearLayout.VERTICAL);
        observationRecyclerView.addItemDecoration(dividerItemDecoration);
        observationRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mAgribiotApplication = (AgribiotApplication) this.getActivity().getApplication();

        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(mServiceBroadcastReceiver, new IntentFilter(OperationMainFragment.OPERATION_MESSAGE));

        agDb.operationWithAllForeignDao().findAll().observe(this.getViewLifecycleOwner(),operationWithAlls -> {
            LoggerUtil.debug(LOG_TAG,"operationWithAllForeignDao Observe");

            if(operationWithAlls != null){
                boolean shouldSelectLastAnnot = false;
                if(mOperations == null){
                    shouldSelectLastAnnot = true;
                }
                mOperations = (ArrayList<OperationWithAllForeign>) operationWithAlls;
                /*for(int i = 0;i<operationWithAlls.size();i++){
                    LoggerUtil.debug(LOG_TAG,"---- "+i+" ----");
                    LoggerUtil.debug(LOG_TAG,"Operation : " + operationWithAlls.get(i).getOperation().toString());
                    LoggerUtil.debug(LOG_TAG,"Annotation : " + operationWithAlls.get(i).getAnnotationTemplate().toString());
                    LoggerUtil.debug(LOG_TAG,"Dimensionvalue Size : " + operationWithAlls.get(i).getNbrDimensionValues());

                }*/
                refreshObservationList(mOperations);

                if(writeRequested){
                    createDimensionDialog(mOperations.get(0).getOperation());
                    writeRequested = false;
                }
                if(mAllObservation != null && mAgribiotApplication.isServiceRunning(ForegroundService.class.getName()) == false && mAllObservation.size() > 0) {
                    //this.showErrorString("L'appareil n'est pas connecté");
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this.getContext());
                    Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                    intent.putExtra("mode",ForegroundService.RfidHandheldMode.OBSERVATION_MODE.ordinal());
                    intent.putExtra("AnnotationTemplate",mAllObservation.get(0));
                    lbm.sendBroadcast(intent);
                }

                if(shouldSelectLastAnnot){
                    selectLastAnnotationTemplate();
                }
            }
        });

        mObservationViewModel.getAllAnnotations().observe(this.getViewLifecycleOwner(), annotationTemplates -> {
            LoggerUtil.debug(LOG_TAG,"allAnnotations Observe");
            mAllObservation = new ArrayList<AnnotationTemplate>();
            for(int i = 0;i < annotationTemplates.size();i++){
                if(annotationTemplates.get(i).getAnnotationType().equals("OBSV")){
                    mAllObservation.add(annotationTemplates.get(i));
                }
            }
            AnnotationTemplate newObs = new AnnotationTemplate();
            newObs.setDescription("+ Nouvelle observation");
            mAllObservation.add(newObs);
            spinnerAdapter.clear();
            spinnerAdapter.addAll(mAllObservation);

            if(newAnnotationTemplate != null){
                LoggerUtil.debug(LOG_TAG, "Search for rfid");
                selectCreatedAnnotationTemplate();

                if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())){
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this.getContext());
                    Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                    intent.putExtra("mode",ForegroundService.RfidHandheldMode.OBSERVATION_MODE.ordinal());
                    intent.putExtra("AnnotationTemplate",mAllObservation.get((int) spinner.getSelectedItemId()));
                    lbm.sendBroadcast(intent);
                }
            }else{
                selectLastAnnotationTemplate();
            }

        });


        buttonWriteObservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoggerUtil.debug(LOG_TAG, "Search for rfid");

                if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())){
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(v.getContext());
                    writeRequested = true;
                    Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                    intent.putExtra("mode",ForegroundService.RfidHandheldMode.OBSERVATION_MODE.ordinal());
                    intent.putExtra("AnnotationTemplate",mAllObservation.get((int) spinner.getSelectedItemId()));
                    intent.putExtra("write",writeRequested);
                    lbm.sendBroadcast(intent);
                }else{
                    showErrorString("L'appareil n'est pas connecté");
                }
               // createFakeOperation();



            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if(((AnnotationTemplate)spinner.getSelectedItem()).getDescription().equals("+ Nouvelle observation")){
                    LoggerUtil.debug(LOG_TAG,"Nouvelle observation");
                    Dialog myDialog = createNewAnnotationDialog();
                    myDialog.show();
                }else{
                    AnnotationTemplate curAnnot = (AnnotationTemplate)spinner.getSelectedItem();
                    if(mAgribiotApplication.isServiceRunning(ForegroundService.class.getName())){
                        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(selectedItemView.getContext());
                        Intent intent = new Intent(ForegroundService.SWITCH_MODE);
                        intent.putExtra("mode",ForegroundService.RfidHandheldMode.OBSERVATION_MODE.ordinal());
                        intent.putExtra("AnnotationTemplate",curAnnot);
                        lbm.sendBroadcast(intent);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        return root;
    }

    public void createDimensionDialog(Operation operation){
        if(dimensionDialogFragment == null){
            dimensionDialogFragment = DimensionDialogFragment.newInstance(operation.getAnnotationTemplateId(),operation.getOperationId());
        }else{
            if(dimensionDialogFragment.isVisible()){
                dimensionDialogFragment.dismiss();
            }
            dimensionDialogFragment = DimensionDialogFragment.newInstance(operation.getAnnotationTemplateId(),operation.getOperationId());
        }
        dimensionDialogFragment.show(this.getActivity().getSupportFragmentManager(),"Valeurs");
    }


    @Override
    public void onClickOnOperation(Operation operation, AnnotationTemplate annotationTemplate) {
        LoggerUtil.debug(LOG_TAG,"onItemClick for " + operation.getOperationId() + " " + annotationTemplate.getAnnotationTemplateId());
        Bundle bundle = new Bundle();
        bundle.putInt("operationId",operation.getOperationId());
        bundle.putInt("annotationTemplateId",annotationTemplate.getAnnotationTemplateId());
        Navigation.findNavController(this.getView()).navigate(R.id.action_nav_observation_to_nav_dimension,bundle);
    }

    public void refreshObservationList(List<OperationWithAllForeign> operations){
        Collections.sort(operations, new Comparator<OperationWithAllForeign>() {
            @Override
            public int compare(OperationWithAllForeign lhs, OperationWithAllForeign rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                if (lhs.getOperation().getCreated().compareTo(rhs.getOperation().getCreated()) < 0) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
        mObservationAdapter.setOperations(operations);
    }
    public void selectLastAnnotationTemplate(){
        if(mAllObservation != null && mOperations != null){
            for(int i = 0;i<mAllObservation.size();i++){
                if(mOperations.size() >0 && mAllObservation.get(i).getAnnotationTemplateId() == mOperations.get(0).getAnnotationTemplate().getAnnotationTemplateId()){
                    spinner.setSelection(i);
                }
            }

        }
    }

    public void selectCreatedAnnotationTemplate(){
        if(mAllObservation != null && newAnnotationTemplate != null){
            for(int i = 0;i<mAllObservation.size();i++){
                if(mAllObservation.get(i).equals( newAnnotationTemplate)){
                    spinner.setSelection(i);
                    newAnnotationTemplate= null;
                }
            }
        }
    }

    private void createFakeOperation(){
        Operation resultOperation = new Operation();
        resultOperation.setAnnotationTemplateId(mAllObservation.get((int) spinner.getSelectedItemId()).getAnnotationTemplateId());
        OperationDao operationDao = agDb.operationDao();
        SensorObjectDao soDao = agDb.sensorObjectDao();


        agDb.databaseWriteExecutor.execute(() -> {
            SensorObject sensorObject = soDao.findSensorObject("FAKETID");
            if (sensorObject == null) {
                sensorObject = new SensorObject();
                sensorObject.setDescription("");
                sensorObject.setType("tag");
                sensorObject.setUuid("FAKETID");
                sensorObject.setSensorObjectId((int) soDao.insert(sensorObject));
            }
            resultOperation.setCreated(Calendar.getInstance().getTime());
            resultOperation.setPostgreId(-1);
            resultOperation.setTagEmbeddedTaskIndex(0);
            resultOperation.setSynchronizedFlag(false);
            resultOperation.setSensorObjectId(sensorObject.getSensorObjectId());
            long id = operationDao.insert(resultOperation);
            resultOperation.setOperationId((int) id);
        });
    }
    private Dialog createNewAnnotationDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        builder.setTitle("Nouveau modèle d'observation");
        View myView = inflater.inflate(R.layout.dialog_new_observation, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(myView)
                // Add action buttons
                .setPositiveButton("Créer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final EditText descriptionEt =(EditText) myView.findViewById(R.id.description_et);

                        newAnnotationTemplate = new AnnotationTemplate();
                        newAnnotationTemplate.setPostgreId(-1);
                        newAnnotationTemplate.setDescription(descriptionEt.getText().toString().trim());
                        newAnnotationTemplate.setAnnotationType("OBSV");
                        newAnnotationTemplate.setSynchronizedFlag(false);
                        AgribiotDatabase.databaseWriteExecutor.execute(() -> {

                            try {

                                long insertId = mObservationViewModel.insert(newAnnotationTemplate);
                                newAnnotationTemplate.setAnnotationTemplateId((int) insertId);
                                LoggerUtil.debug(LOG_TAG, "Have add " + newAnnotationTemplate.toString());
                            }catch(SQLiteConstraintException e ){
                                LoggerUtil.debug(LOG_TAG,e.getMessage());
                                sendError("Cette observation existe déjà");
                            }
                        });

                    }
                })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }



    class MyObservationadapter extends ArrayAdapter<AnnotationTemplate> {

        // Your sent context
        private Context context;
        // Your custom values for the spinner (User)
        LayoutInflater inflater;
        public MyObservationadapter(Activity context, int textViewResourceId,
                                    ArrayList<AnnotationTemplate> annotationTemplates) {
            super(context, textViewResourceId, annotationTemplates);
            this.context = context;
            inflater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
            AnnotationTemplate annotationTemplate = getItem(position);
            View rowview = inflater.inflate(R.layout.spinner_observation,null,true);
            TextView description = (TextView) rowview.findViewById(R.id.tv_observation_desc);
            ImageView imageView = (ImageView) rowview.findViewById(R.id.iv_observation_photo);
            description.setText(annotationTemplate.getDescription());
            if(annotationTemplate.getPhoto() != null && !annotationTemplate.getPhoto().equals("null")){
                Bitmap myBitmap = BitmapFactory.decodeFile(context.getFilesDir().getPath() + "/"+annotationTemplate.getPhoto());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(50, 50);
                imageView.setLayoutParams(layoutParams);
                imageView.setImageBitmap(myBitmap);
            }

            // And finally return your dynamic (or custom) view for each spinner item
            return rowview;
        }
        // And here is when the "chooser" is popped up
        // Normally is the same view, but you can customize it if you want
        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
            AnnotationTemplate annotationTemplate = getItem(position);
            View rowview = inflater.inflate(R.layout.spinner_observation,null,true);
            TextView description = (TextView) rowview.findViewById(R.id.tv_observation_desc);
            ImageView imageView = (ImageView) rowview.findViewById(R.id.iv_observation_photo);
            description.setText(annotationTemplate.getDescription());
            if(annotationTemplate.getPhoto() != null && !annotationTemplate.getPhoto().equals("null")) {
                Bitmap myBitmap = BitmapFactory.decodeFile(context.getFilesDir().getPath() + "/" + annotationTemplate.getPhoto());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(50, 50);
                imageView.setLayoutParams(layoutParams);
                imageView.setImageBitmap(myBitmap);
            }

            return rowview;
        }
    }


    private BroadcastReceiver mServiceBroadcastReceiver= new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!intent.hasExtra("operation")) {
                return;
            }
            writeRequested = true;
        }
    };
    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mServiceBroadcastReceiver);
        super.onDestroy();
    }

}