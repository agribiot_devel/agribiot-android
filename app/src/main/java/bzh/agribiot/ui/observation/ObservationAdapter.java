/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.observation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Insets;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Layout;
import android.util.Size;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import bzh.agribiot.R;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationWithAllForeign;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.AgribiotFragment;
import bzh.agribiot.ui.OperationMainFragment;


public class ObservationAdapter extends RecyclerView.Adapter<ObservationAdapter.ObservationViewHolder> {
    static String LOG_TAG = "ObservationAdapter";

    private List<OperationWithAllForeign> mOperations; // Cached copy of words
    OperationMainFragment mOperationFragment;
    View itemView;
    public ObservationAdapter(OperationMainFragment obsFragment) {
        this.mOperationFragment = obsFragment;
    }


    @Override
    public ObservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_observation, parent, false);
        return new ObservationViewHolder(itemView,mOperationFragment);
    }
    @Override
    public void onBindViewHolder(@NonNull ObservationViewHolder holder, int position) {
        if(mOperations != null){
            // Get the primary text color of the theme
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = itemView.getContext().getTheme();
            theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
            TypedArray arr =
                    itemView.getContext().obtainStyledAttributes(typedValue.data, new int[]{
                            android.R.attr.textColorPrimary});
            int primaryColor = arr.getColor(0, -1);
            OperationWithAllForeign current = mOperations.get(position);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String strDate = dateFormat.format(current.getOperation().getCreated());
            holder.setOperation(current.getOperation());
            holder.annotationTextView.setText(current.getAnnotationTemplate().getDescription());
            holder.sensorObjectDescriptionTextView.setText(current.getSensorObject().getDescription());
            holder.dateTextView.setText(strDate);
            if(current.getNbrDimensionValues() >1){
                holder.valueTextView.setText(current.getNbrDimensionValues() + " valeurs");
            }else{
                holder.valueTextView.setText(current.getNbrDimensionValues() + " valeur");
            }
            holder.indexTextView.setText(Integer.toString(current.getOperation().getTagEmbeddedTaskIndex()));
            holder.tidTextView.setText(current.getSensorObject().getUuid());

            if(current.getOperation().getPhoto() != null && current.getOperation().getPhoto().equals("null") == false){
                LoggerUtil.debug(LOG_TAG,current.getOperation().getPhoto());
                holder.ivPhotoexist.setColorFilter(Color.parseColor("#40a832"));
            }else{
                holder.ivPhotoexist.setColorFilter(primaryColor);
            }
            holder.ivTakePicture.setColorFilter(primaryColor);
            if(current.getOperation().isSynchronizedFlag()){
                holder.annotationTextView.setTextColor(Color.parseColor("#40a832"));
                holder.sensorObjectDescriptionTextView.setTextColor(Color.parseColor("#40a832"));
                holder.dateTextView.setTextColor(Color.parseColor("#40a832"));
                holder.valueTextView.setTextColor(Color.parseColor("#40a832"));
                holder.indexTextView.setTextColor(Color.parseColor("#40a832"));
                holder.tidTextView.setTextColor(Color.parseColor("#40a832"));
                holder.ivTakePicture.setVisibility(View.INVISIBLE);
                holder.ivPhotoexist.setVisibility(View.INVISIBLE);

            }else{
                holder.annotationTextView.setTextColor(primaryColor);
                holder.sensorObjectDescriptionTextView.setTextColor(primaryColor);
                holder.dateTextView.setTextColor(primaryColor);
                holder.valueTextView.setTextColor(primaryColor);
                holder.indexTextView.setTextColor(primaryColor);
                holder.tidTextView.setTextColor(primaryColor);
                holder.ivTakePicture.setVisibility(View.VISIBLE);
                holder.ivPhotoexist.setVisibility(View.VISIBLE);
            }
            WindowManager wManager = (WindowManager) itemView.getContext().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wManager.getDefaultDisplay();

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(display.getWidth()/8, display.getWidth()/6);
            layoutParams.gravity = Gravity.CENTER;
            holder.ivObservation.setLayoutParams(layoutParams);

            if(current.getAnnotationTemplate().getPhoto() != null && current.getAnnotationTemplate().getPhoto().equals("null") != true) {
                Bitmap myBitmap = BitmapFactory.decodeFile(itemView.getContext().getFilesDir().getPath() + "/" + current.getAnnotationTemplate().getPhoto());
                holder.ivObservation.setImageBitmap(myBitmap);
            }else{
                int id = itemView.getContext().getResources().getIdentifier("@android:drawable/ic_menu_myplaces", null, null);
                holder.ivObservation.setImageResource(id);
            }


            holder.layoutObsRowCenter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOperationFragment.onClickOnOperation(current.getOperation(),current.getAnnotationTemplate());

                }
            });


        }

    }
    public void setOperations(List<OperationWithAllForeign> operations){
        this.mOperations = operations;
        notifyDataSetChanged();
    }

    OperationWithAllForeign getOperation(int position){
        return this.mOperations.get(position);
    }

    @Override
    public int getItemCount() {
        if (mOperations != null)
            return mOperations.size();
        else return 0;
    }

    public class ObservationViewHolder extends RecyclerView.ViewHolder {
        String LOG_TAG = "ObservationViewHolder";

        private final TextView annotationTextView;
        private final TextView sensorObjectDescriptionTextView;
        private final TextView dateTextView;
        private final TextView valueTextView;
        private final TextView tidTextView;
        private final TextView indexTextView;
        private final ImageView ivObservation;
        private final ImageView ivTakePicture;
        private final ImageView ivPhotoexist;
        private final LinearLayout layoutObsRowCenter ;
        private Operation mOperation;
        private OperationMainFragment mOperationFragment;
        public void setOperation(Operation operation){
            mOperation = operation;
        }
        public ObservationViewHolder(@NonNull View itemView, OperationMainFragment operationMainFragment) {
            super(itemView);

            layoutObsRowCenter = (LinearLayout) itemView.findViewById(R.id.layout_obs_row_center);
            annotationTextView = (TextView) itemView.findViewById(R.id.row_annotation);
            sensorObjectDescriptionTextView = (TextView) itemView.findViewById(R.id.row_sensor_description);
            dateTextView = (TextView) itemView.findViewById(R.id.row_date);
            valueTextView = (TextView) itemView.findViewById(R.id.row_description);
            tidTextView = (TextView) itemView.findViewById(R.id.row_tid);
            indexTextView = (TextView) itemView.findViewById(R.id.row_index);
            ivObservation = (ImageView)itemView.findViewById(R.id.iv_observation);
            ivTakePicture = (ImageView)itemView.findViewById(R.id.iv_take_picture);
            ivPhotoexist = (ImageView)itemView.findViewById(R.id.iv_photo_exist);
            this.mOperationFragment = operationMainFragment;
            ivTakePicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoggerUtil.debug(LOG_TAG,"Take picture");
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    if (takePictureIntent.resolveActivity(((Activity) v.getContext()).getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            return;
                        }
                        mOperationFragment.setCurImageOperation(mOperation, photoFile.getAbsolutePath());
                        LoggerUtil.debug(LOG_TAG,"Start photo activity for result with operation id : " + mOperation.getOperationId()+ " and image : " + photoFile.getAbsolutePath());

                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(v.getContext(),
                                    "bzh.agribiot.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            ((Activity) v.getContext()).startActivityForResult(takePictureIntent, AgribiotFragment.REQUEST_IMAGE_CAPTURE);
                        }
                    }


                }
            });

        }
        String currentPhotoPath;

        private File createImageFile() throws IOException {
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.mOperationFragment.getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = image.getAbsolutePath();
            return image;
        }


    }
}