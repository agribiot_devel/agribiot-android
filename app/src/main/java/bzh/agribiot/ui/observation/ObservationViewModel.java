/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.observation;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import java.util.List;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Operation;
import bzh.agribiot.repository.AnnotationTemplateRepository;
import bzh.agribiot.repository.OperationRepository;

public class ObservationViewModel extends AndroidViewModel {

    private OperationRepository mOperationRepository;
    private AnnotationTemplateRepository mAnnotationRepository;

    private final LiveData<List<Operation>> mAllOperations;
    private final LiveData<List<AnnotationTemplate>> mAllAnnotations;


    public ObservationViewModel (Application application) {
        super(application);
        mOperationRepository = new OperationRepository(application);
        mOperationRepository.initWithAllOperation();
        mAnnotationRepository = new AnnotationTemplateRepository(application);
        mAllOperations = mOperationRepository.getAllOperation();
        mAllAnnotations = mAnnotationRepository.getAllAnnotations();
    }
    public LiveData<List<Operation>> getAllOperations() {return mAllOperations;}
    public LiveData<List<AnnotationTemplate>> getAllAnnotations() {return mAllAnnotations;}

    public void insert(Operation operation) {
        mOperationRepository.insert(operation);
    }
    public long insert(AnnotationTemplate annotationTemplate) throws SQLiteConstraintException {
        return mAnnotationRepository.insert(annotationTemplate);
    }

    public void remove(int operationId) { mOperationRepository.remove(operationId); }
    public void update(Operation operation){
        mOperationRepository.update(operation);
    }
}