/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.foregroundservice.ForegroundService;
import bzh.agribiot.logger.LoggerUtil;

public class AgribiotFragment extends Fragment {

    public static final int REQUEST_IMAGE_CAPTURE = 92;


    public static final String LOG_TAG = "AgribiotFragment";
    protected Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            String msg = message.getData().getString("StrMessage");
            showErrorString(msg);
        }
    };
    public void showError(Exception e) {
        int duration = Toast.LENGTH_LONG;
        CharSequence text = e.getMessage();
        Toast toast = Toast.makeText(this.getContext(), text, duration);
        toast.show();
    }
    public void showErrorString(String e) {
        int duration = Toast.LENGTH_SHORT;
        CharSequence text = e;
        //LoggerUtil.debug(LOG_TAG,"showErrorString: "+ this.getContext() + " " + text + " " + duration);
        Toast toast = Toast.makeText(this.getContext(), text, duration);
        toast.show();
    }
    public void sendError(String message) {
        Message myMessage = this.mHandler.obtainMessage();
        Bundle messageBundle = new Bundle();
        //Ajouter des données à transmettre au Handler via le Bundle
        messageBundle.putString("StrMessage", message);
        //Ajouter le Bundle au message
        myMessage.setData(messageBundle);
        //Envoyer le message
        mHandler.sendMessage(myMessage);
    }
    public void sendMessage(int message) {
        Message myMessage = this.mHandler.obtainMessage();
        Bundle messageBundle = new Bundle();
        //Ajouter des données à transmettre au Handler via le Bundle
        messageBundle.putInt("IntMessage", message);
        //Ajouter le Bundle au message
        myMessage.setData(messageBundle);
        //Envoyer le message
        mHandler.sendMessage(myMessage);
    }

    public void releaseKey(){
        if(((AgribiotApplication) this.getActivity().getApplication()).isServiceRunning(ForegroundService.class.getName())){
            LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this.getContext());
            Intent intent = new Intent(ForegroundService.SWITCH_MODE);
            intent.putExtra("mode",ForegroundService.RfidHandheldMode.RELEASED_KEY_MODE.ordinal());
            lbm.sendBroadcast(intent);
        }
    }
  /*  @Override
    public void onDestroyView() {
        super.onDestroyView();
        LoggerUtil.debug(LOG_TAG,"onDestroyView");

    }
*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        releaseKey();
        super.onCreate(savedInstanceState);
        LoggerUtil.debug(LOG_TAG,"onCreate ");
    }

/*
    @Override
    public void onPause() {
        super.onPause();
        LoggerUtil.debug(LOG_TAG,"onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LoggerUtil.debug(LOG_TAG,"onDetach");
    }

    @Override
    public void onStop() {
        super.onStop();
        LoggerUtil.debug(LOG_TAG,"onStop");
    }

    @Override
    public void onDestroy() {
        LoggerUtil.debug(LOG_TAG,"onDestroy");
        super.onDestroy();
    }*/
}
