package bzh.agribiot.ui.dimension;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import bzh.agribiot.R;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.logger.LoggerUtil;

public class DimensionDialogFragment extends DialogFragment  implements View.OnClickListener{
    public static final String LOG_TAG = "DimensionDialogFragment";

    private DimensionViewModel mDimensionViewModel;
    DimensionElementData candidateData = null;
    DimensionElementData currentData = null;
    AppCompatImageButton btnAddDimension;
    RecyclerView dimensionRecycler ;
    DimensionAdapter dimensionAdapter;
    AgribiotDatabase db;

    public int mAnnotationTemplateId = 0;
    public int mOperationId = 0;

    public static DimensionDialogFragment newInstance(int annotationTemplateId, int operationId) {
        DimensionDialogFragment dimDialogFragment = new DimensionDialogFragment();
        dimDialogFragment.mAnnotationTemplateId = annotationTemplateId;
        dimDialogFragment.mOperationId = operationId;
        return dimDialogFragment;
    }

    public DimensionDialogFragment(){

    }
    @Override
    public void onStop() {
        super.onStop();
        if(dimensionAdapter != null){
            dimensionAdapter.saveRecyclerList();
        }
    }
    public void onResume() {

        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams)params);
        // Call super onResume after sizing

        super.onResume();

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_dimension_value, container);

        return v;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        db = AgribiotDatabase.getDatabase(getContext());
        dimensionRecycler= (RecyclerView) view.findViewById(R.id.dimensionRecyclerDialog);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayout.VERTICAL);
        dimensionRecycler.addItemDecoration(dividerItemDecoration);

        btnAddDimension = view.findViewById(R.id.btn_add_dimension);

        Bundle bundle = getArguments();
        mDimensionViewModel = new DimensionViewModelProvider(this.getActivity().getApplication(),mAnnotationTemplateId,mOperationId).create(DimensionViewModel.class);
        if(candidateData == null){
            candidateData = new DimensionElementData();
        }


        mDimensionViewModel.getAllDimension().observe(this.getViewLifecycleOwner(),dimensions -> {
            Dimension addDim = new Dimension();
            addDim.setName("+ Ajouter une dimension");
            addDim.setDimensionId(-1);
            dimensions.add(addDim);
            candidateData.setDimensionList(dimensions);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });
        mDimensionViewModel.getAllDimensionValue().observe(this.getViewLifecycleOwner(),dimensionValues -> {
            candidateData.setDimensionValueList(dimensionValues);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });
        mDimensionViewModel.getOperationLiveData().observe(this.getViewLifecycleOwner(),operation -> {
            candidateData.setOperationWithAllForeign(operation);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });
        mDimensionViewModel.getLastUsedDimension().observe(this.getViewLifecycleOwner(),lastUsedDimension -> {
            candidateData.setLastUsedDimensionList(lastUsedDimension);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });
        btnAddDimension.setOnClickListener(this);
    }



    public LinkedHashMap<Dimension, DimensionValue> prepareDimensionValueMap(DimensionElementData data){
        LinkedHashMap<Dimension, DimensionValue> dimensionValueMap = new LinkedHashMap<>();
        for(int i = 0;i< data.getDimensionValueList().size();i++){
            for(Dimension dim :data.getDimensionList()){
                if(dim.getDimensionId() == data.getDimensionValueList().get(i).getDimensionId()){
                    dimensionValueMap.put(dim, data.getDimensionValueList().get(i));
                }
            }
        }
        if(!currentData.getOperationWithAllForeign().getOperation().isSynchronizedFlag()){
            if(currentData.getDimensionValueList().size()== 0 && currentData.getLastUsedDimensionList().size() > 0){
                for(Dimension dim :currentData.getLastUsedDimensionList()){
                            dimensionValueMap.put(dim,null);
                }
            }
        }
        return dimensionValueMap;
    }
    public void refreshAdapter(LinkedHashMap<Dimension, DimensionValue> dimensionValueMap){
        ArrayList<Map.Entry<Dimension, DimensionValue>> entryList = new ArrayList<Map.Entry<Dimension, DimensionValue>>(dimensionValueMap.entrySet());
        if(dimensionAdapter == null){
            dimensionAdapter = new DimensionAdapter(this.getContext(), entryList, (ArrayList<Dimension>) currentData.getDimensionList(),currentData.getOperationWithAllForeign().getOperation());
            dimensionRecycler.setAdapter(dimensionAdapter);
            dimensionRecycler.setLayoutManager(new LinearLayoutManager(this.getContext()));
        }else {

            if(dimensionAdapter.getInsertedDim() != -1) {
                /**
                 * We just create a new dimension
                 */
                for (Dimension dim : currentData.getDimensionList()) {
                    if (dim.getDimensionId() == dimensionAdapter.getInsertedDim()) {
                        entryList.add(new AbstractMap.SimpleEntry<>(dim, null));
                        dimensionAdapter.setInsertedDim(-1);
                        break;
                    }
                }
            }
            dimensionAdapter.setBothList((ArrayList<Dimension>) currentData.getDimensionList(),entryList);

        }


    }
    public void refreshUi(){
        LinkedHashMap<Dimension, DimensionValue> dimensionValueMap = prepareDimensionValueMap(currentData);

        if(currentData.getOperationWithAllForeign().getOperation().isSynchronizedFlag()) {
            btnAddDimension.setVisibility(View.GONE);
        }else{

            btnAddDimension.setVisibility(View.VISIBLE);
            btnAddDimension.setOnClickListener(this);
        }
        refreshAdapter(dimensionValueMap);

    }


    @Override
    public void onClick(View v) {
        LinkedHashMap<Dimension, DimensionValue> dimensionValueMap = new LinkedHashMap<>();

        ArrayList<Map.Entry<Dimension, DimensionValue>> currentList = dimensionAdapter.getList();
        for (Map.Entry<Dimension, DimensionValue> entry : currentList) {
            dimensionValueMap.put(entry.getKey(), entry.getValue());
        }

        for (Dimension dim : currentData.getDimensionList()) {
            if (dimensionValueMap.containsKey(dim)) {
                continue;
            } else {
                dimensionValueMap.put(dim, null);
                break;
            }
        }
        refreshAdapter(dimensionValueMap);
    }

}
