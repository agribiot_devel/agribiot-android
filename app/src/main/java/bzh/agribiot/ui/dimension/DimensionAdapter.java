/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.dimension;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;

import java.util.AbstractMap;
import java.util.ArrayList;

import java.util.Map;

import bzh.agribiot.R;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionDao;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.DimensionValueDao;
import bzh.agribiot.database.Operation;
import bzh.agribiot.logger.LoggerUtil;


public class DimensionAdapter extends RecyclerView.Adapter< DimensionAdapter.DimensionViewHolder> {
    public static final String LOG_TAG = "DimensionAdapter";
    ArrayList <Map.Entry<Dimension, DimensionValue>> mList;
    private Operation mOperation;
    ArrayList<Dimension> mDimensions;
    DimensionAdapter.MyDimensionLabelAdapter spinnerAdapter;
    Context mContext;
    public long insertedDim = -1;
    protected DimensionAdapter(Context context,ArrayList <Map.Entry<Dimension, DimensionValue>> list, ArrayList<Dimension> dimensionArrayList,Operation operation) {
        mDimensions = dimensionArrayList;
        mOperation = operation;
        mContext = context;
        mList = list;
        insertedDim = -1;
    }

    @Override
    public DimensionAdapter.DimensionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.row_dimension, parent, false);
        DimensionAdapter.DimensionViewHolder dimensionViewHolder = new DimensionViewHolder(itemView);

        dimensionViewHolder.getBtnRemoveDimension().setOnClickListener(dimensionViewHolder);
        return dimensionViewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull DimensionViewHolder holder, int position) {

        Map.Entry<Dimension, DimensionValue> current = getList().get(position);

        if(mOperation.isSynchronizedFlag()){
            holder.getSpinnerDimensionLabel().setVisibility(View.GONE);
            holder.getEtDimensionValue().setVisibility(View.GONE);
            holder.getBtnRemoveDimension().setVisibility(View.GONE);

            holder.getTvDimensionLabel().setVisibility(View.VISIBLE);
            holder.getTvDimensionValue().setVisibility(View.VISIBLE);
            holder.getTvDimensionLabel().setText(current.getKey().getName()+" ("+current.getKey().getUnit()+")");
            holder.getTvDimensionValue().setText(Double.toString(current.getValue().getValue()));
        }else{
            spinnerAdapter = new DimensionAdapter.MyDimensionLabelAdapter((Activity) this.mContext, android.R.layout.simple_spinner_item,mDimensions);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.getSpinnerDimensionLabel().setAdapter(spinnerAdapter);
            holder.getSpinnerDimensionLabel().setOnItemSelectedListener(holder);
            holder.getSpinnerDimensionLabel().setVisibility(View.VISIBLE);

            holder.getSpinnerDimensionLabel().setSelection(mDimensions.indexOf(current.getKey()));
            holder.getEtDimensionValue().setVisibility(View.VISIBLE);
            holder.getTvDimensionLabel().setVisibility(View.GONE);
            holder.getTvDimensionValue().setVisibility(View.GONE);
            if(current.getValue() != null){
                holder.getBtnRemoveDimension().setVisibility(View.VISIBLE);
                holder.getEtDimensionValue().setText(Double.toString(current.getValue().getValue()));
            }else {
                holder.getBtnRemoveDimension().setVisibility(View.VISIBLE);
                holder.getEtDimensionValue().setText("");
            }
        }
    }

    public ArrayList<Map.Entry<Dimension, DimensionValue>> getmList() {
        return mList;
    }

    public void setmList(ArrayList<Map.Entry<Dimension, DimensionValue>> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public ArrayList<Dimension> getmDimensions() {
        return mDimensions;
    }

    public void setmDimensions(ArrayList<Dimension> mDimensions) {
        this.mDimensions = mDimensions;
        if(spinnerAdapter != null){
            spinnerAdapter.clear();
            spinnerAdapter.addAll(mDimensions);
            spinnerAdapter.notifyDataSetChanged();
        }
        notifyDataSetChanged();

    }
    public void setBothList(ArrayList<Dimension> mDimensions,ArrayList<Map.Entry<Dimension, DimensionValue>> mList){
        this.mList = mList;
        this.mDimensions = mDimensions;
        /*if(spinnerAdapter != null){
            spinnerAdapter.clear();
            spinnerAdapter.addAll(mDimensions);
            //spinnerAdapter.notifyDataSetChanged();
        }*/
        notifyDataSetChanged();

    }

    public long getInsertedDim() {
        return insertedDim;
    }

    public void setInsertedDim(long insertedDim) {
        this.insertedDim = insertedDim;
    }
    public void saveRecyclerList(){

        ArrayList<Map.Entry<Dimension, DimensionValue>> list = this.getList();

        for(Map.Entry<Dimension, DimensionValue> entry:list){
            if(entry.getKey() == null || entry.getValue() == null) {
                return;
            }
            AgribiotDatabase db = AgribiotDatabase.getDatabase(this.mContext);
            DimensionValueDao dimensionValueDao = db.dimensionValueDao();

            AgribiotDatabase.databaseWriteExecutor.execute(() -> {
                if(entry.getValue().getDimensionValueId() == 0){
                    long id = dimensionValueDao.insert(entry.getValue());
                }else{
                    long id = dimensionValueDao.update(entry.getValue());
                }
            });
        }
    }
    public void addDimension(Dimension dimension){
        AgribiotDatabase db = AgribiotDatabase.getDatabase(this.mContext);
        DimensionDao dimensionDao = db.dimensionDao();
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            insertedDim = dimensionDao.insert(dimension);
        });
    }
    public void removeData(int position){
        AgribiotDatabase db;
        try {
            Map.Entry<Dimension, DimensionValue> current = mList.get(position);
            if (current.getKey() == null || current.getValue() == null) {
                return;
            }
            db = AgribiotDatabase.getDatabase(this.mContext);
            DimensionValueDao dimensionValueDao = db.dimensionValueDao();

            AgribiotDatabase.databaseWriteExecutor.execute(() -> {
                dimensionValueDao.delete(current.getValue());
            });
        }catch(ArrayIndexOutOfBoundsException e){
            LoggerUtil.debug(LOG_TAG,"Exception " + e.getMessage());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public ArrayList<Map.Entry<Dimension, DimensionValue>> getList() {
        return mList;
    }

    private Dialog createNewDimensionDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(this.mContext);
        builder.setTitle("Nouvelle dimension");
        View myView = inflater.inflate(R.layout.new_dimension, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(myView)
                // Add action buttons
                .setPositiveButton("Créer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final EditText etName =(EditText) myView.findViewById(R.id.et_dimension_name);
                        final EditText etUnit =(EditText) myView.findViewById(R.id.et_dimension_unit);

                        Dimension dim = new Dimension();
                        dim.setName(etName.getText().toString().trim());
                        dim.setUnit(etUnit.getText().toString().trim());
                        addDimension(dim);
                    }
                })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }



    public class DimensionViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemSelectedListener, View.OnClickListener{
        public static final String LOG_TAG = "DimensionViewHolder";

        private final Spinner spinnerDimensionLabel;
        private final EditText etDimensionValue;
        private final TextView tvDimensionLabel;
        private final TextView tvDimensionValue;
        private final AppCompatImageButton btnRemoveDimension;
        int previousSpinnerPos = -1;
        ArrayList<Dimension> mDimensions;
        Context context;
        public DimensionViewHolder(@NonNull View itemView) {
            super(itemView);
            spinnerDimensionLabel = itemView.findViewById(R.id.spinner_dimension_label);
            etDimensionValue = itemView.findViewById(R.id.et_dimension_value);
            tvDimensionLabel = itemView.findViewById(R.id.tv_dimansion_label);
            tvDimensionValue = itemView.findViewById(R.id.tv_dimension_value);
            btnRemoveDimension = itemView.findViewById(R.id.btn_remove_dimension);
            context = itemView.getContext();
            etDimensionValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try{
                        Map.Entry<Dimension, DimensionValue> current = DimensionAdapter.this.getList().get(getAdapterPosition());
                        if(current.getValue() == null && editable.toString().length() >0){
                            DimensionValue dimValue = new DimensionValue();
                            dimValue.setOperationId(mOperation.getOperationId());
                            dimValue.setDimensionId(((Dimension)spinnerDimensionLabel.getSelectedItem()).getDimensionId());
                            dimValue.setValue(Double.parseDouble(editable.toString()));
                            current.setValue(dimValue);
                            DimensionAdapter.this.getList().set(getAdapterPosition(),current);
                        }else if(current.getValue() != null && current.getValue().getValue() != Double.parseDouble(editable.toString())){
                            current.getValue().setValue(Double.parseDouble(editable.toString()));
                            DimensionAdapter.this.getList().set(getAdapterPosition(),current);
                        }
                    }catch(NumberFormatException e){

                    }

                }
            });

        }


        public EditText getEtDimensionValue() {
            return etDimensionValue;
        }

        public Spinner getSpinnerDimensionLabel() {
            return spinnerDimensionLabel;
        }

        public TextView getTvDimensionLabel() {
            return tvDimensionLabel;
        }

        public TextView getTvDimensionValue() {
            return tvDimensionValue;
        }

        public AppCompatImageButton getBtnRemoveDimension() {
            return btnRemoveDimension;
        }


        DimensionViewHolder create(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_dimension, parent, false);
            return new DimensionViewHolder(view);
        }


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Dimension currentDim = (Dimension) spinnerDimensionLabel.getSelectedItem();

            if(currentDim.getDimensionId() == -1){
                // Create new dimension, save current value list before
                saveRecyclerList();
                Dialog createDialog = createNewDimensionDialog();
                createDialog.show();
            }else{
                if(getAdapterPosition() == RecyclerView.NO_POSITION){
                    return;
                }
                Map.Entry<Dimension, DimensionValue> currentEntry = DimensionAdapter.this.getList().get(getAdapterPosition());
                for(int i = 0;i< DimensionAdapter.this.getList().size();i++){
                    if(i != this.getAdapterPosition()){
                        if(currentDim.equals(DimensionAdapter.this.getList().get(i).getKey())){
                            /**
                             * The user select an already existing Dimension in list
                             */
                            spinnerDimensionLabel.setSelection(previousSpinnerPos);
                            return;
                        }

                    }
                }
                previousSpinnerPos = position;
                DimensionAdapter.this.getList().set(getAdapterPosition(),new AbstractMap.SimpleEntry<>((Dimension) spinnerDimensionLabel.getSelectedItem(), currentEntry.getValue()));
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

        @Override
        public void onClick(View v) {
            try {
                DimensionAdapter.this.removeData(getAdapterPosition());
                DimensionAdapter.this.getList().remove(getAdapterPosition());
                DimensionAdapter.this.notifyItemRemoved(getAdapterPosition());
            }catch(ArrayIndexOutOfBoundsException e){
                LoggerUtil.debug(LOG_TAG,"Exception " + e.getMessage());
            }
        }
    }
    public static class MyDimensionLabelAdapter extends ArrayAdapter<Dimension> {
        public static final String LOG_TAG = "MyDimensionLabelAdapter";

        // Your sent context
        private Context context;
        LayoutInflater inflater;
        public MyDimensionLabelAdapter(Activity activity, int textViewResourceId,
                                       ArrayList<Dimension> dimensions) {
            super(activity, textViewResourceId, dimensions);
            this.context = activity;
            inflater = activity.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try{
                Dimension dimension = getItem(position);
                View rowview = inflater.inflate(R.layout.spinner_dimension,null,true);
                TextView tvDimensionName = (TextView) rowview.findViewById(R.id.tv_dimension_name);
                if(dimension.getDimensionId() == -1){
                    tvDimensionName.setText(dimension.getName());
                }else{
                    tvDimensionName.setText(dimension.getName() + "("+dimension.getUnit()+")");
                }
                return rowview;

            }catch(IndexOutOfBoundsException e){
                View rowview = inflater.inflate(R.layout.spinner_dimension,null,true);
                return rowview;

            }

        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            Dimension dimension = getItem(position);
            View rowview = inflater.inflate(R.layout.spinner_dimension,null,true);
            TextView tvDimensionName = (TextView) rowview.findViewById(R.id.tv_dimension_name);
            if(dimension.getDimensionId() == -1){
                tvDimensionName.setText(dimension.getName());
            }else{
                tvDimensionName.setText(dimension.getName() + "("+dimension.getUnit()+")");
            }
            return rowview;
        }
    }
}