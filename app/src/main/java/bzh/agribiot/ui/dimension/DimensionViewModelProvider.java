package bzh.agribiot.ui.dimension;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class DimensionViewModelProvider implements ViewModelProvider.Factory {
    private Application mApplication;
    private int mAnnotationTemplateId;
    private int mOperationId;


    public DimensionViewModelProvider(Application application, int annotationTemplateId, int operationId) {
        mApplication = application;
        mAnnotationTemplateId = annotationTemplateId;
        mOperationId = operationId;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new DimensionViewModel(mApplication, mAnnotationTemplateId, mOperationId);
    }
}
