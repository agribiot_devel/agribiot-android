/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.dimension;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import bzh.agribiot.R;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.DimensionValueDao;
import bzh.agribiot.database.OperationDao;
import bzh.agribiot.database.SensorObjectDao;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.ui.AgribiotFragment;


public class DimensionFragment extends AgribiotFragment implements View.OnClickListener{
    public static final String LOG_TAG = "DimensionFragment";

    private DimensionViewModel mDimensionViewModel;
    TextView tvAnnotationTemplate;
    DimensionElementData candidateData = null;
    DimensionElementData currentData = null;
    AppCompatImageButton btnAddDimension;
    RecyclerView dimensionRecycler ;
    DimensionAdapter dimensionAdapter;
    Button btnRelocTag ;
    EditText etTagDesc;
    AgribiotDatabase db;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dimension, container, false);
        db = AgribiotDatabase.getDatabase(this.getActivity());
        tvAnnotationTemplate = (TextView)root.findViewById(R.id.tv_annot);
        dimensionRecycler= (RecyclerView) root.findViewById(R.id.dimensionRecycler);
        btnRelocTag = (Button) root.findViewById(R.id.btn_reloc_tag);
        etTagDesc = (EditText) root.findViewById(R.id.et_tag_desc);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this.getContext(),
                LinearLayout.VERTICAL);
        dimensionRecycler.addItemDecoration(dividerItemDecoration);

        btnAddDimension = root.findViewById(R.id.btn_add_dimension);

        Bundle bundle = getArguments();
        mDimensionViewModel = new DimensionViewModelProvider(this.getActivity().getApplication(),bundle.getInt("annotationTemplateId"),bundle.getInt("operationId")).create(DimensionViewModel.class);
        if(candidateData == null){
            candidateData = new DimensionElementData();
        }


        mDimensionViewModel.getAllDimension().observe(this.getViewLifecycleOwner(),dimensions -> {
            Dimension addDim = new Dimension();
            addDim.setName("+ Ajouter une dimension");
            addDim.setDimensionId(-1);
            dimensions.add(addDim);
            candidateData.setDimensionList(dimensions);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });
        mDimensionViewModel.getAllDimensionValue().observe(this.getViewLifecycleOwner(),dimensionValues -> {
            candidateData.setDimensionValueList(dimensionValues);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });

        mDimensionViewModel.getOperationLiveData().observe(this.getViewLifecycleOwner(),operation -> {
            candidateData.setOperationWithAllForeign(operation);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });
        mDimensionViewModel.getLastUsedDimension().observe(this.getViewLifecycleOwner(),lastUsedDimension -> {
            candidateData.setLastUsedDimensionList(lastUsedDimension);
            if(candidateData.isComplete()){
                currentData = new DimensionElementData(candidateData);
                refreshUi();
            }
        });
        btnAddDimension.setOnClickListener(this);

        btnRelocTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                currentData.getOperationWithAllForeign().getSensorObject().setLat(currentData.getOperationWithAllForeign().getOperation().getOpLat());
                                currentData.getOperationWithAllForeign().getSensorObject().setLon(currentData.getOperationWithAllForeign().getOperation().getOpLon());
                                currentData.getOperationWithAllForeign().getSensorObject().setSynchronizedFlag(false);
                                SensorObjectDao soDao = db.sensorObjectDao();
                                AgribiotDatabase.databaseWriteExecutor.execute(() -> {
                                    soDao.updateSensorObject(currentData.getOperationWithAllForeign().getSensorObject());
                                });
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Déplacer le tag à la position courante de l'opération ?").setPositiveButton("Oui", dialogClickListener)
                        .setNegativeButton("Annuler", dialogClickListener).show();


            }
        });
        return root;
    }



    @Override
    public void onStop() {
        super.onStop();
        AgribiotDatabase db = AgribiotDatabase.getDatabase(this.getContext());
        SensorObjectDao sensorObjectDao = db.sensorObjectDao();

        if (etTagDesc.getText().toString().length() > 0 && etTagDesc.getText().toString().equals(currentData.operationWithAllForeign.getSensorObject().getDescription()) == false) {
            AgribiotDatabase.databaseWriteExecutor.execute(() -> {
                currentData.operationWithAllForeign.getSensorObject().setDescription(etTagDesc.getText().toString().trim());
                currentData.operationWithAllForeign.getSensorObject().setSynchronizedFlag(false);
                sensorObjectDao.updateSensorObject(currentData.operationWithAllForeign.getSensorObject());
            });
        }

        if(dimensionAdapter != null){
            dimensionAdapter.saveRecyclerList();
        }
    }

    public LinkedHashMap<Dimension, DimensionValue> prepareDimensionValueMap(DimensionElementData data){
        LinkedHashMap<Dimension, DimensionValue> dimensionValueMap = new LinkedHashMap<>();
        for(int i = 0;i< data.getDimensionValueList().size();i++){
            for(Dimension dim :data.getDimensionList()){
                if(dim.getDimensionId() == data.getDimensionValueList().get(i).getDimensionId()){
                    dimensionValueMap.put(dim, data.getDimensionValueList().get(i));
                }
            }
        }

        if(!currentData.getOperationWithAllForeign().getOperation().isSynchronizedFlag()){
            if(currentData.getDimensionValueList().size()== 0 && currentData.getLastUsedDimensionList().size() > 0){
                for(Dimension dim :currentData.getLastUsedDimensionList()){
                            dimensionValueMap.put(dim,null);
                }
            }
        }
        return dimensionValueMap;
    }
    public void refreshAdapter(LinkedHashMap<Dimension, DimensionValue> dimensionValueMap){
        ArrayList<Map.Entry<Dimension, DimensionValue>> entryList = new ArrayList<Map.Entry<Dimension, DimensionValue>>(dimensionValueMap.entrySet());

        if(dimensionAdapter == null){
            dimensionAdapter = new DimensionAdapter(this.getContext(), entryList, (ArrayList<Dimension>) currentData.getDimensionList(),currentData.getOperationWithAllForeign().getOperation());
            dimensionRecycler.setAdapter(dimensionAdapter);
            dimensionRecycler.setLayoutManager(new LinearLayoutManager(this.getContext()));
        }else {

            if(dimensionAdapter.getInsertedDim() != -1) {
                /**
                 * We just create a new dimension
                 */
                for (Dimension dim : currentData.getDimensionList()) {
                    if (dim.getDimensionId() == dimensionAdapter.getInsertedDim()) {
                        entryList.add(new AbstractMap.SimpleEntry<>(dim, null));
                        dimensionAdapter.setInsertedDim(-1);
                        break;
                    }
                }
            }

            dimensionAdapter.setBothList((ArrayList<Dimension>) currentData.getDimensionList(),entryList);

        }


    }
    public void refreshUi(){
        tvAnnotationTemplate.setText(currentData.getOperationWithAllForeign().getAnnotationTemplate().getDescription());
        LinkedHashMap<Dimension, DimensionValue> dimensionValueMap = prepareDimensionValueMap(currentData);

        if(currentData.getOperationWithAllForeign().getOperation().isSynchronizedFlag()) {
            btnAddDimension.setVisibility(View.GONE);
            btnRelocTag.setVisibility(View.GONE);
            etTagDesc.setFocusable(false);
            etTagDesc.setFocusableInTouchMode(false);
            etTagDesc.setClickable(false);

        }else{

            btnAddDimension.setVisibility(View.VISIBLE);
            btnAddDimension.setOnClickListener(this);
            btnRelocTag.setVisibility(View.VISIBLE);
            etTagDesc.setFocusable(true);
            etTagDesc.setFocusableInTouchMode(true);
            etTagDesc.setClickable(true);
        }
        etTagDesc.setText(currentData.getOperationWithAllForeign().getSensorObject().getDescription());
        refreshAdapter(dimensionValueMap);

    }


    @Override
    public void onClick(View v) {
        LinkedHashMap<Dimension, DimensionValue> dimensionValueMap = new LinkedHashMap<>();

        ArrayList<Map.Entry<Dimension, DimensionValue>> currentList = dimensionAdapter.getList();
        for (Map.Entry<Dimension, DimensionValue> entry : currentList) {
            dimensionValueMap.put(entry.getKey(), entry.getValue());
        }

        for (Dimension dim : currentData.getDimensionList()) {
            if (dimensionValueMap.containsKey(dim)) {
                continue;
            } else {
                dimensionValueMap.put(dim, null);
                break;
            }
        }
        refreshAdapter(dimensionValueMap);
    }


}
