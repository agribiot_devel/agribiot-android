package bzh.agribiot.ui.dimension;

import java.util.ArrayList;
import java.util.List;

import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationWithAllForeign;
import bzh.agribiot.database.SensorObject;

public class DimensionElementData {
    OperationWithAllForeign operationWithAllForeign;
    List<Dimension> dimensionList;
    List<DimensionValue> dimensionValueList;
    List<Dimension> lastUsedDimensionList;
    public DimensionElementData(){
        operationWithAllForeign = null;
        dimensionList = new ArrayList<Dimension>();
        dimensionValueList = new ArrayList<DimensionValue>();
        lastUsedDimensionList = new ArrayList<Dimension>();

    }
    public DimensionElementData(DimensionElementData myDimensionElementData){
        operationWithAllForeign = myDimensionElementData.operationWithAllForeign;
        dimensionList = myDimensionElementData.dimensionList;
        dimensionValueList = myDimensionElementData.dimensionValueList;
        lastUsedDimensionList = myDimensionElementData.lastUsedDimensionList;
    }

    public OperationWithAllForeign getOperationWithAllForeign() {
        return operationWithAllForeign;
    }

    public void setOperationWithAllForeign(OperationWithAllForeign operationWithAllForeign) {
        this.operationWithAllForeign = operationWithAllForeign;
    }



    public List<Dimension> getDimensionList() {
        return dimensionList;
    }

    public List<DimensionValue> getDimensionValueList() {
        return dimensionValueList;
    }


    public void setDimensionList(List<Dimension> dimensionList) {
        this.dimensionList = dimensionList;
    }

    public void setDimensionValueList(List<DimensionValue> dimensionValueList) {
        this.dimensionValueList = dimensionValueList;
    }

    public List<Dimension> getLastUsedDimensionList() {
        return lastUsedDimensionList;
    }

    public void setLastUsedDimensionList(List<Dimension> lastUsedDimensionList) {
        this.lastUsedDimensionList = lastUsedDimensionList;
    }



    public boolean isComplete(){
        if(this.operationWithAllForeign != null){
            return true;
        }else{
            return false;
        }
    }
}
