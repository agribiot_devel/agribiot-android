/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.ui.dimension;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationWithAllForeign;
import bzh.agribiot.repository.AnnotationTemplateRepository;
import bzh.agribiot.repository.DimensionRepository;
import bzh.agribiot.repository.DimensionValueRepository;
import bzh.agribiot.repository.OperationRepository;
import bzh.agribiot.repository.OperationWithAllForeignRepository;
import bzh.agribiot.repository.SensorObjectRepository;

public class DimensionViewModel extends AndroidViewModel {
    private DimensionRepository mDimensionRepository;
    private DimensionValueRepository mDimensionValueRepository;
    private OperationWithAllForeignRepository mOperationWithAllForeignRepository;
    private final LiveData<List<Dimension>> mAllDimension;
    private final LiveData<List<DimensionValue>> mAllDimensionValue;
    private final LiveData<OperationWithAllForeign> mOperationWithAllForeign;
    private final LiveData<List<Dimension>> mLastUsedDimension;
    public DimensionViewModel (Application application, int annotationTemplateId, int operationId) {
        super(application);
        mDimensionRepository = new DimensionRepository(application,annotationTemplateId);
        mDimensionValueRepository = new DimensionValueRepository(application,operationId);
        mOperationWithAllForeignRepository = new OperationWithAllForeignRepository(application);
        mOperationWithAllForeignRepository.initByOperationId(operationId);
        mAllDimension = mDimensionRepository.getAllDimensions();
        mAllDimensionValue = mDimensionValueRepository.getAllDimensionsValues();
        mOperationWithAllForeign = mOperationWithAllForeignRepository.get();
        mLastUsedDimension = mDimensionRepository.getLastUsedDimensions();
    }
    public LiveData<List<Dimension>> getAllDimension() {return mAllDimension;}
    public LiveData<List<DimensionValue>> getAllDimensionValue() {return mAllDimensionValue;}
    public LiveData<OperationWithAllForeign> getOperationLiveData(){
        return mOperationWithAllForeign;
    }

    public LiveData<List<Dimension>> getLastUsedDimension() {
        return mLastUsedDimension;
    }

    public void insert(Dimension dimension) {
        mDimensionRepository.insert(dimension);
    }


}
