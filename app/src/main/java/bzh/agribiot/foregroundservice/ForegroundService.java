/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.foregroundservice;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Arrays;

import bzh.agribiot.AgribiotApplication;
import bzh.agribiot.MainActivity;
import bzh.agribiot.R;
import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationDao;
import bzh.agribiot.database.SensorObject;
import bzh.agribiot.database.SensorObjectDao;
import bzh.agribiot.logger.LoggerUtil;
import bzh.agribiot.rfid.AgribiotKeyEventCallback;
import bzh.agribiot.rfid.RFIDEventListener;
import bzh.agribiot.rfid.RFIDHandler;
import bzh.agribiot.rfid.RfidException;
import bzh.agribiot.rfid.RfidWriteCounter;
import bzh.agribiot.rfid.TagInfo;
import bzh.agribiot.ui.OperationMainFragment;
import bzh.agribiot.ui.rfidmanagement.RfidManagementFragment;

public class ForegroundService extends Service implements  RFIDEventListener, AgribiotKeyEventCallback {


    public static final String LOG_TAG = "ForegroundService";
    public static final String CHANNEL_ID = "ForegroundServiceChannel";



    public static String SWITCH_MODE = "SWITCH_MODE";

    public enum RfidHandheldMode {
        TASK_MODE,
        OBSERVATION_MODE,
        RFID_SETTINGS_MODE,
        RELEASED_KEY_MODE,
    };
    RfidHandheldMode mRfidHandheldMode;
    RfidManagementFragment.RfidManagementMessageType mRfidManagementMessageType;
    AnnotationTemplate mAnnotationTemplate = null;
    AgribiotDatabase agDb;
    AgribiotApplication agribiotApplication ;
    boolean rfidHandlerRunning = false;
    ForegroundService mForegroundService = this;
    RFIDHandler mRfidHandler = null;
    private Location mLastLocation = null;
    @Override
    public void onCreate() {
        super.onCreate();
        LocalBroadcastManager.getInstance(this).registerReceiver(mServiceBroadcastReceiver, new IntentFilter(ForegroundService.SWITCH_MODE));
    }
    @Override
    @SuppressLint("MissingPermission")
    public int onStartCommand(Intent intent, int flags, int startId) {
        LoggerUtil.debug(LOG_TAG,"start foreground service");
        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Foreground Service")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_menu_manage)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        LocationListener listener;
        LocationManager locationManager;
        agribiotApplication = (AgribiotApplication) this.getApplication();
        agDb = AgribiotDatabase.getDatabase(this.getApplicationContext());

        mRfidHandler = agribiotApplication.getRfidHandler();
        mRfidHandheldMode = RfidHandheldMode.RELEASED_KEY_MODE;
        mRfidManagementMessageType = RfidManagementFragment.RfidManagementMessageType.READ_TAG;
        if(mRfidHandler == null){
            try {
                Thread.sleep(1000);
                mRfidHandler = agribiotApplication.getRfidHandler();
                if(mRfidHandler == null){
                    onError(new RfidException("Erreur de connexion RFID"));
                    return START_NOT_STICKY;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        mRfidHandler.setEventListener(this);
        mRfidHandler.setKeyEventCallback(this);

        listener = new LocationListener()
        {
            @Override
            public void onLocationChanged(Location location)
            {
                mLastLocation= location;
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s)
            {

            }
        };

        locationManager = (LocationManager) agribiotApplication.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, listener);
        return START_NOT_STICKY;
    }
    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mServiceBroadcastReceiver);
        super.onDestroy();
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }


    @Override
    public void onConnect() {

    }

    @Override
    public void onDisconnect() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
        rfidHandlerRunning = false;
        stopSelf();

    }

    @Override
    public void onError(RfidException e) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ForegroundService.this.getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        rfidHandlerRunning = false;
    }

    @Override
    public void onReadMemoryComplete(TagInfo tag) {
        LoggerUtil.debug(LOG_TAG,"onReadMemoryComplete " + tag.toString());

        switch(mRfidHandheldMode) {
            case TASK_MODE:
            case OBSERVATION_MODE:
                if(tag != null) {
                    try {
                        RfidWriteCounter writeCounter = new RfidWriteCounter(this, mAnnotationTemplate.getAnnotationTemplateId(), mRfidHandler,tag);
                        writeCounter.execute();
                    } catch (RfidException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case RFID_SETTINGS_MODE:
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this.getApplicationContext());
                Intent intent = new Intent(RfidManagementFragment.RFID_MANAGEMENT_MESSAGE);
                intent.putExtra("tag",tag);
                lbm.sendBroadcast(intent);
                rfidHandlerRunning = false;
                break;
        }
    }

    @Override
    public void onWriteComplete(Operation operation,TagInfo tag) {
        OperationDao operationDao = agDb.operationDao();
        SensorObjectDao sensorObjectDao = agDb.sensorObjectDao();
        switch(mRfidHandheldMode) {
            case TASK_MODE:
            case OBSERVATION_MODE:
                if(mLastLocation != null){
                    operation.setOpLon(mLastLocation.getLongitude());
                    operation.setOpLat(mLastLocation.getLatitude());
                    operation.setOpAccuracy(mLastLocation.getAccuracy());
                }
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this.getApplicationContext());
                Intent intent = new Intent(OperationMainFragment.OPERATION_MESSAGE);
                intent.putExtra("operation",operation);
                lbm.sendBroadcast(intent);
                agDb.databaseWriteExecutor.execute(() -> {
                    SensorObject sensorObject= sensorObjectDao.findSensorObject(LoggerUtil.BytesToHex(Arrays.copyOfRange(tag.getTIDMemData(),0,12)));
                    if(sensorObject == null){
                        sensorObject = new SensorObject();
                        sensorObject.setDescription("");
                        sensorObject.setLat(operation.getOpLat());
                        sensorObject.setLon(operation.getOpLon());
                        sensorObject.setType("tag");
                        sensorObject.setUuid(LoggerUtil.BytesToHex(Arrays.copyOfRange(tag.getTIDMemData(),0,12)));
                        sensorObject.setSensorObjectId((int) sensorObjectDao.insert(sensorObject));
                    }
                    operation.setSensorObjectId(sensorObject.getSensorObjectId());
                    long id = operationDao.insert(operation);
                    operation.setOperationId((int)id);

                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                    rfidHandlerRunning = false;
                });

                break;
        }
    }


    private BroadcastReceiver mServiceBroadcastReceiver= new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int mode = intent.getIntExtra("mode",RfidHandheldMode.RELEASED_KEY_MODE.ordinal());
            mRfidHandheldMode = RfidHandheldMode.values()[mode];
            switch(mRfidHandheldMode){
                case TASK_MODE:
                case OBSERVATION_MODE:
                    mAnnotationTemplate = (AnnotationTemplate) intent.getSerializableExtra("AnnotationTemplate");
                    mRfidHandler.setEventListener(mForegroundService);
                    mRfidHandler.setKeyEventCallback(mForegroundService);
                    /*
                    If value is pass'd, user as click on the observation's screen button write tag manually
                     */
                    if(intent.hasExtra("write")){
                        LoggerUtil.debug(LOG_TAG,"Have switch to "+  mRfidHandheldMode + " mode with data : " + mAnnotationTemplate.toString());
                        if(rfidHandlerRunning == false) {
                            rfidHandlerRunning = true;
                            TagInfo closest = mRfidHandler.readClosestTag();
                            if(closest == null){
                                onError(new RfidException("Aucun tag detecté"));
                                return;
                            }
                            onReadMemoryComplete(closest);
                        }
                    }else{
                        LoggerUtil.debug(LOG_TAG,"Have switch to "+  mRfidHandheldMode + " mode with data : " + mAnnotationTemplate.toString());
                    }
                    break;
                case RFID_SETTINGS_MODE:
                    mRfidHandler.setEventListener(mForegroundService);
                    mRfidHandler.setKeyEventCallback(mForegroundService);
                    LoggerUtil.debug(LOG_TAG,"Have switch to "+  mRfidHandheldMode + " mode" );
                    if(intent.hasExtra("action")){
                        int action = intent.getIntExtra("action", RfidManagementFragment.RfidManagementMessageType.READ_TAG.ordinal());
                        mRfidManagementMessageType = RfidManagementFragment.RfidManagementMessageType.values()[action];
                        switch(mRfidManagementMessageType){
                            case READ_TAG:
                                if(rfidHandlerRunning == false){
                                    rfidHandlerRunning = true;
                                    TagInfo closest = mRfidHandler.readClosestTag();
                                    if(closest == null){
                                        onError(new RfidException("Aucun tag detecté"));
                                        return;
                                    }
                                    onReadMemoryComplete(closest);
                                }
                                break;
                            case ENABLE_TAG:
                                if(rfidHandlerRunning == false) {
                                    rfidHandlerRunning = true;
                                    if (!intent.hasExtra("tag")) {
                                        return;
                                    }
                                    TagInfo tagToEnable = (TagInfo) intent.getSerializableExtra("tag");
                                    try {
                                        if(initMemory(tagToEnable) == 0){
                                            onError(new RfidException("Aucun tag detecté à l'écriture"));
                                            return;
                                        }
                                    } catch (Exception e) {
                                        onError((RfidException) e);
                                    }
                                    TagInfo closest = mRfidHandler.readClosestTag();
                                    if(closest == null){
                                        onError(new RfidException("Aucun tag detecté à la vérification"));
                                        return;
                                    }
                                    onReadMemoryComplete(closest);
                                    rfidHandlerRunning = false;
                                }
                                break;
                            case DISABLE_TAG:
                                if(rfidHandlerRunning == false) {
                                    rfidHandlerRunning = true;
                                    if (!intent.hasExtra("tag")) {
                                        return;
                                    }
                                    TagInfo tagToDisable = (TagInfo) intent.getSerializableExtra("tag");
                                    try {
                                        if(disableMemory(tagToDisable) == 0){
                                            onError(new RfidException("Aucun tag detecté à l'écriture"));
                                            return;
                                        }
                                    } catch (Exception e) {
                                        onError((RfidException) e);
                                    }
                                    TagInfo closest = mRfidHandler.readClosestTag();
                                    if(closest == null){
                                        onError(new RfidException("Aucun tag detecté à la vérification"));
                                        return;
                                    }
                                    onReadMemoryComplete(closest);
                                    rfidHandlerRunning = false;
                                }
                                 break;
                         }

                    }
                    break;
                case RELEASED_KEY_MODE:
                    LoggerUtil.debug(LOG_TAG,"Set RELEASED_KEY_MODE");
                    break;
            }
        }
    };

    @Override
    public void KeyEvent() {
        LoggerUtil.debug(LOG_TAG,"KeyEvent  mode " + mRfidHandheldMode + " handler busy : " + rfidHandlerRunning  );
        switch(mRfidHandheldMode){
            case TASK_MODE:
            case OBSERVATION_MODE:
            case RFID_SETTINGS_MODE:
                if(rfidHandlerRunning == false) {
                    rfidHandlerRunning = true;
                    TagInfo closest = mRfidHandler.readClosestTag();
                    if(closest == null){
                        onError(new RfidException("Aucun tag detecté"));
                        return;
                    }
                    onReadMemoryComplete(closest);
                }
                break;
            case RELEASED_KEY_MODE:
            break;
        }
    }

    /*
     * Init Memory map
     */

    public int initMemory(TagInfo tag) throws Exception {
        byte initBytes[] = {TagInfo.MAGIC_NUMBER[0],TagInfo.MAGIC_NUMBER[1],TagInfo.MAGIC_NUMBER[2],TagInfo.MEMORY_MAPPING_VERSION,TagInfo.MEMORY_USED_BYTE,0x0,0x0,0x0,0x0,0x1};
        LoggerUtil.debug(LOG_TAG,"Init memory");
        return this.mRfidHandler.writeUserData(tag, 0, initBytes);
    }
    public int disableMemory(TagInfo tag) throws Exception {
        byte initBytes[] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};
        LoggerUtil.debug(LOG_TAG,"Disable memory");
        return this.mRfidHandler.writeUserData(tag, 0, initBytes);
    }
}