package bzh.agribiot.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.DimensionValue;
import bzh.agribiot.database.DimensionValueDao;

public class DimensionValueRepository {



    private DimensionValueDao mDimensionValueDao;

    private LiveData<List<DimensionValue>> mAllDimensionsValues;

    public DimensionValueRepository(Application application, int operationId) {
        AgribiotDatabase db;
        db = AgribiotDatabase.getDatabase(application);
        mDimensionValueDao = db.dimensionValueDao();
        mAllDimensionsValues = mDimensionValueDao.findByOperation(operationId);
    }
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<DimensionValue>> getAllDimensionsValues() {
        return mAllDimensionsValues;
    }


    public void insert(DimensionValue dimensionValue) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mDimensionValueDao.insert(dimensionValue);
        });
    }
    public void update(DimensionValue dimensionValue) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mDimensionValueDao.update(dimensionValue);
        });
    }

}
