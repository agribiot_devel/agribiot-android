/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.repository;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;

import androidx.lifecycle.LiveData;

import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.AnnotationTemplate;
import bzh.agribiot.database.AnnotationTemplateDao;
import bzh.agribiot.database.AnnotationWithOperations;

import java.util.List;

public class AnnotationTemplateRepository {
    private AnnotationTemplateDao mAnnotationDao;

    private LiveData<List<AnnotationTemplate>> mAllAnnotations;
    private LiveData<List<AnnotationWithOperations>> mAllAnnotationWithObservation;
    private LiveData<AnnotationTemplate> mAnnotationTemplateLiveData;

    public AnnotationTemplateRepository(Application application) {
        AgribiotDatabase db;
        db = AgribiotDatabase.getDatabase(application);
        mAnnotationDao = db.annotationTemplateDao();
        mAllAnnotations = mAnnotationDao.getAll();
        mAllAnnotationWithObservation = mAnnotationDao.getAllLiveAnnotationWithOperations();
    }
    public AnnotationTemplateRepository(Application application, int annotationTemplateId) {
        AgribiotDatabase db;
        db = AgribiotDatabase.getDatabase(application);
        mAnnotationDao = db.annotationTemplateDao();
        mAnnotationTemplateLiveData= mAnnotationDao.getLive(annotationTemplateId);
    }
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<AnnotationTemplate>> getAllAnnotations() {
        return mAllAnnotations;
    }

    public LiveData<List<AnnotationWithOperations>> getAllAnnotationWithOperations() {
        return mAllAnnotationWithObservation;
    }
    public LiveData<AnnotationTemplate> get() {
        return mAnnotationTemplateLiveData;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public long insert(AnnotationTemplate annotationTemplate) throws SQLiteConstraintException {
        return mAnnotationDao.insertAnnotationTemplate(annotationTemplate);
    }
    public void update(AnnotationTemplate annotationTemplate) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mAnnotationDao.updateAnnotationTemplate(annotationTemplate);
        });
    }

    public void deleteOldEntry(int []postGreID){
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mAnnotationDao.deleteAllByNotPostGreIds(postGreID);
        });
    }
}
