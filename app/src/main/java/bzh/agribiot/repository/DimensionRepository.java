package bzh.agribiot.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.Dimension;
import bzh.agribiot.database.DimensionDao;

public class DimensionRepository {
    private DimensionDao mDimensionDao;

    private LiveData<List<Dimension>> mAllDimensions;
    private LiveData<List<Dimension>> mUsedDimensions;
    public DimensionRepository(Application application,int annotationTemplateId) {
        AgribiotDatabase db;
        db = AgribiotDatabase.getDatabase(application);
        mDimensionDao = db.dimensionDao();
        mAllDimensions = mDimensionDao.getAll();
        mUsedDimensions = mDimensionDao.getUsedDimensionForAnnotationId(annotationTemplateId);
    }
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<Dimension>> getAllDimensions() {
        return mAllDimensions;
    }

    public LiveData<List<Dimension>> getLastUsedDimensions() {
        return mUsedDimensions;
    }

    public void insert(Dimension dimension) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mDimensionDao.insert(dimension);
        });
    }
    public void update(Dimension dimension) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mDimensionDao.update(dimension);
        });
    }

}
