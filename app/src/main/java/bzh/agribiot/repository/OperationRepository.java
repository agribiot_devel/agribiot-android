/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationDao;

public class OperationRepository {
    private OperationDao mOperationDao;
    private LiveData<List<Operation>> mAllOperation;
    private LiveData<Operation> mOperationLiveData;

    public OperationRepository(Application application) {
        AgribiotDatabase db;
        db = AgribiotDatabase.getDatabase(application);
        mOperationDao = db.operationDao();
    }
    public void initByAnnotationTemplateId(int taskId){
        mAllOperation = mOperationDao.findByTask(taskId);
    }
    public void initWithAllOperation(){
        mAllOperation = mOperationDao.getAll();
    }
    public void initByOperationId(int operationId){
        mOperationLiveData = mOperationDao.getLive(operationId);
    }


    public LiveData<Operation> get(){
        return mOperationLiveData;
    }
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<Operation>> getAllOperation() {
        return mAllOperation;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public void insert(Operation operation) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mOperationDao.insert(operation);
        });
    }
    public void remove(int operationId){
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mOperationDao.delete(operationId);
        });
    }
    public void insertAll(ArrayList<Operation> operationList) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mOperationDao.insertAll(operationList);
        });
    }
    public void update(Operation operation) {
        AgribiotDatabase.databaseWriteExecutor.execute(() -> {
            mOperationDao.updateOperation(operation);
        });
    }
}
