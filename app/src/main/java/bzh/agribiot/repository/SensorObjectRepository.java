/*
Projet AgriBIoT - Centre de Recherche INRIA Rennes
INRIA Startup Studio - 2020-2021
Copyright : AgriBIoT (c) 2020-2021
Module Name :
AgriBIoT-Android
Author : Tropée Simon (simon.tropee@gmail.com)
Oscar Roberto Bastos (roberto@bastos-reseach.fr)
Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
*/
package bzh.agribiot.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.SensorObject;
import bzh.agribiot.database.SensorObjectDao;


public class SensorObjectRepository {

    private SensorObjectDao mSensorObjectDao;

    private LiveData<List<SensorObject>> mAllSensorObjects;

    // Note that in order to unit test the WordRepository, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    public SensorObjectRepository(Application application) {
        AgribiotDatabase db;
        db = AgribiotDatabase.getDatabase(application);
        mSensorObjectDao = db.sensorObjectDao();
        mAllSensorObjects = mSensorObjectDao.getAll();
    }
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<SensorObject>> getmAllSensorObjects() {
        return mAllSensorObjects;
    }

}
