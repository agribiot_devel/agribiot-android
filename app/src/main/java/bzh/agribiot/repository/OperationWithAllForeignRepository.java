package bzh.agribiot.repository;


import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import bzh.agribiot.database.AgribiotDatabase;
import bzh.agribiot.database.Operation;
import bzh.agribiot.database.OperationWithAllForeign;
import bzh.agribiot.database.OperationWithAllForeignDao;

public class OperationWithAllForeignRepository {
    private OperationWithAllForeignDao mOperationWithallForeignDao;
    private LiveData<List<OperationWithAllForeign>> mAllOperation;
    private LiveData<OperationWithAllForeign> mOperationLiveData;
    public OperationWithAllForeignRepository(Application application) {
        AgribiotDatabase db;
        db = AgribiotDatabase.getDatabase(application);
        mOperationWithallForeignDao = db.operationWithAllForeignDao();
    }

    public void initByAnnotationTemplateId(int taskId){
        mAllOperation = mOperationWithallForeignDao.findAllByAnnotationTemplateId(taskId);
    }
    public void initWithAllOperation(){
        mAllOperation = mOperationWithallForeignDao.findAll();
    }
    public void initByOperationId(int operationId){
        mOperationLiveData = mOperationWithallForeignDao.get(operationId);
    }

    public LiveData<OperationWithAllForeign> get(){
        return mOperationLiveData;
    }
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<OperationWithAllForeign>> getAllOperation() {
        return mAllOperation;
    }
}
